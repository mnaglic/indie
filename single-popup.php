<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package iBE_2016
 */
?>

<?php
get_header();
$popup = rwmb_meta( 'ibe_popup' );
if (isset($popup['ibe_popup_image'])) {
    $image_id = $popup['ibe_popup_image'];
    $image = wp_get_attachment_image_src($image_id[0], 'large' );
    $image = $image[0];
}


if (isset($popup['ibe_popup_url'])) {
    $url = $popup['ibe_popup_url'];
} else {
    $url = null;
}
 echo '<div class="modal-popup cookie-modal invisible">';
 echo '  <div class="modal-bg"></div>';
 echo '  <div class="modal-popup-content" style="background-image: url('.$image.');">';
 echo '      <span class="remove">&times;</span>';
 if ( $url !== null ) {
     echo '      <a href="'.$url.'">';
 }
 echo '          <div class="inner">';
 echo                $popup['ibe_popup_content'];
 echo '          </div>';
 if ( $url !== null ) {
     echo '      </a>';
 }
 echo '  </div>';
 echo '</div>';
get_footer();
 ?>
