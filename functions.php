<?php
/**
 * iBE_2016 functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package iBE_2016
 */

require get_template_directory() . '/functions/enqueue.php';
require get_template_directory() . '/functions/template-tags.php';
require get_template_directory() . '/functions/extras.php';
require get_template_directory() . '/functions/customizer.php';
require get_template_directory() . '/functions/theme-support.php';
require get_template_directory() . '/functions/widgets.php';
require get_template_directory() . '/functions/admin.php';
require get_template_directory() . '/functions/meta-box.php';
require get_template_directory() . '/functions/post-types/remove-post-type-slug.php';
require get_template_directory() . '/functions/MailChimp.php';
require get_template_directory() . '/functions/registration.php';
?>
