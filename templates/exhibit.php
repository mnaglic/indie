<?php
/**
 *
 *
 * @file           about.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 * Template Name: Exhibit
 */
 ?>
<?php
    get_header();
    $loop = new WP_Query( 'post_type=page&p='.$post->ID );
    while ( $loop->have_posts() ) : $loop->the_post();
?>

 <div id="exhibit" class="page">
     <div class="container">
     		<?php if ( have_posts() ) : ?>
                <?php //include( locate_template( 'templates/partials/header-exhibit.php', false, false ) );  ?>
                <?php include( locate_template( 'templates/partials/section-exhibit-show-list.php', false, false ) );  ?>
                <?php include( locate_template( 'templates/partials/section-testimonials.php', false, false ) );  ?>
                <?php include( locate_template( 'templates/partials/section-carousel.php', false, false ) );  ?>
                <?php include( locate_template( 'templates/partials/section-media-assets.php', false, false ) );  ?>
                <?php include( locate_template( 'templates/partials/section-videos.php', false, false ) );  ?>
             <?php endif; ?>
     </div> <!-- /container -->
 </div><!-- /ww -->

 <?php endwhile; ?>
 <?php get_footer(); ?>
