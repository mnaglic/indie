<?php
/**
 *
 *
 * @file           about.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 * Template Name: Attend
 */
 ?>
<?php
    get_header();
    $loop = new WP_Query( 'post_type=page&p='.$post->ID );
    while ( $loop->have_posts() ) : $loop->the_post();
?>

 <div id="attend" class="page">
     <div class="container">
     		<?php if ( have_posts() ) : ?>
                <?php include( locate_template( 'templates/partials/section-show-list.php', false, false ) );  ?>
                <main class="attend-main">
                    <?php include( locate_template( 'templates/partials/section-show-day-list.php', false, false ) );  ?>
                    <?php include( locate_template( 'templates/partials/section-attend-videos.php', false, false ) );  ?>
                    <?php include( locate_template( 'templates/partials/section-attend-past-shows.php', false, false ) );  ?>
                </main>
                <aside id="secondary" class="sidebar" role="complementary">
                    <?php include( locate_template( 'templates/partials/section-attend-sidebar.php', false, false ) );  ?>
                </aside><!-- #secondary -->

             <?php endif; ?>
     </div> <!-- /container -->
 </div><!-- /ww -->

 <?php endwhile; ?>
 <?php get_footer(); ?>
