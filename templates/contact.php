<?php
/**
 *
 *
 * @file           policy.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 * Template Name: Contact
 */
?>
<?php

get_header();
$loop = new WP_Query( 'post_type=page&p='.$post->ID );
while ( $loop->have_posts() ) : $loop->the_post();

$headline= rwmb_meta( 'ibe_page_headline' );
$contacts = rwmb_meta( 'ibe_contact' );
if ( has_post_thumbnail( $post->ID ) ) {
    $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'retina' );
    $style = 'background-image: url('.$src[0].')';
} else {
    $style = '';
}
?>

<div id="contact" class="page" style="<?php echo $style; ?>">
    <div class="container">
            <?php include( locate_template( 'templates/partials/header-general.php', false, false ) );  ?>
    		<?php if ( have_posts() ) : ?>
                <?php include( locate_template( 'templates/partials/section-contact.php', false, false ) );  ?>
            <?php endif; ?>
    </div> <!-- /container -->
</div><!-- /ww -->

<?php endwhile; ?>
<?php get_footer(); ?>
