<?php
/**
 *
 *
 * @file           policy.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 * Template Name: Policies
 */
?>
<?php

get_header();
$loop = new WP_Query( 'post_type=page&p='.$post->ID );
while ( $loop->have_posts() ) : $loop->the_post(); ?>

<div id="policy" class="page">
    <div class="container">

        <?php
        echo "<h1>";
        the_title();
        echo "</h1>";
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
            echo "<article>";
            the_content();
            echo "</article>";
			endwhile;
		endif;
        ?>
    </div> <!-- /container -->
</div><!-- /ww -->

<?php endwhile; ?>
<?php get_footer(); ?>
