<?php
    echo '<section class="day-list">';
    echo '<div class="extra-details"><h3>WHO, WHAT, WHERE</h3></div>';
    $default_show = rwmb_meta( 'ibe_default_show' );
    $shows = rwmb_meta( 'ibe_showlist_selection' );

    foreach ($shows as &$show) {
        $show_id = $show['ibe_show_item'];
        $show_enabled = rwmb_meta( 'ibe_show_details_enabled', array(), $show_id );
        if ($show_enabled === '1') {
            echo '<div class="day venue">';
            $venue_image = 'ibe_venue_image';
            $venue_location = 'ibe_venue_location';
            $venue_address1 = 'ibe_venue_address1';
            $venue_address2 = 'ibe_venue_address2';
            $venue_address3 = 'ibe_venue_address3';
            $venue_map = 'ibe_venue_map';
            $venue_website = 'ibe_venue_website';
            $venue_description = 'ibe_venue_map';

            echo '<div class="day-header">';
            if (  isset($day[$day_image][0]) ) {
                $day_image = wp_get_attachment_image_src($day[$day_image][0], 'large' )[0];
                echo '<img class="day-image" src="'.$day_image.'" />';
            }
            if ( isset($day[$day_map]) ) {
                $map_open_tag = '<a href="'.$day[$day_map].'" target="_blank">';
                $map_close_tag = '</a>';
            }  else {
                $map_open_tag = '';
                $map_close_tag = '';
            }
            if ( isset($venue_location) ) {
                echo '<h3>'.$venue_location.'</h3>';
            }
            if ( isset($day[$day_times]) ) {
                echo '<h4 class="times">'.$day[$day_times].'</h4>';
            }
            if ( isset($day[$day_location]) ) {
                echo '<h4 class="location">'.$map_open_tag.$day[$day_location].$map_close_tag.'</h4>';
            }
            if ( isset($day[$day_address1]) ) {
                echo '<h4 class="address">'.$map_open_tag.$day[$day_address1].$map_close_tag.'</h4>';
            }
            if ( isset($day[$day_address2]) ) {
                echo '<h4 class="address">'.$map_open_tag.$day[$day_address2].$map_close_tag.'</h4>';
            }
            if ( isset($day[$day_address3]) ) {
                echo '<h4 class="address">'.$map_open_tag.$day[$day_address3].$map_close_tag.'</h4>';
                $spacer = '';
            }
    		echo $spacer;

            if ( isset($day[$day_description]) ) {
                echo '<article class="description">'.$day[$day_description].'</article>';
            }
            if ( isset($day[$day_tickets_url]) && isset($day[$day_tickets_text]) && $dayCount !== 3 ) {
                echo '<a class="button" href="'.$day[$day_tickets_url].'">'.$day[$day_tickets_text].'</a>';
            }

            echo '</div>';
            echo '</div>';
        }
    }
    echo '</section>';
?>
