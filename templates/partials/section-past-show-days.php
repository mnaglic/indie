<?php
    echo '<section class="day-list">';
        $show_enabled = rwmb_meta( 'ibe_show_details_enabled', array(), $show->ID );
        $day1 = rwmb_meta( 'ibe_show_day_1', array(), $show->ID );
        $day2 = rwmb_meta( 'ibe_show_day_2', array(), $show->ID );
        $day3 = rwmb_meta( 'ibe_show_day_3', array(), $show->ID );
        $days = array($day1, $day2, $day3);

        $pastshows_page = get_page_by_title( 'Past Shows' );

        $dayCount = 0;
        $tbaCount = 0;
        foreach ($days as &$day) {
            $dayCount++;
            if ( $show_enabled === '1' && $dayCount !== 1 ) {
                echo '<div class="day day-'.$dayCount.'">';
                $day_title = 'ibe_day_'.$dayCount.'_title';
                $day_date = 'ibe_day_'.$dayCount.'_date';
                $day_audience = 'ibe_day_'.$dayCount.'_audience';
                $day_location = 'ibe_day_'.$dayCount.'_location';
                $day_address1 = 'ibe_day_'.$dayCount.'_address1';
                $day_address2 = 'ibe_day_'.$dayCount.'_address2';
                $day_address3 = 'ibe_day_'.$dayCount.'_address3';
                $day_map = 'ibe_day_'.$dayCount.'_map';
                $day_times = 'ibe_day_'.$dayCount.'_times';
                $day_description = 'ibe_day_'.$dayCount.'_copy';
                $day_fineprint = 'ibe_day_'.$dayCount.'_fineprint';
                $day_image = 'ibe_day_'.$dayCount.'_image';
                $day_tickets_text = 'ibe_day_'.$dayCount.'_tickets_text';
                $day_tickets_url = 'ibe_day_'.$dayCount.'_tickets_url';
                $day_schedule_text = 'ibe_day_'.$dayCount.'_schedule_text';
                $day_schedule_url = 'ibe_day_'.$dayCount.'_schedule_url';
                $day_enabled = 'ibe_day_'.$dayCount.'_enabled';
				$spacer = '<h4 class="spacer">spacer</h4>';

                if ( $dayCount == 2 ) {
                    echo '<span class="day-count">Day 1</span>';
                    echo '<h3>Shop Indie</h3>';
                }
                if ( $dayCount == 3 ) {
                    echo '<span class="day-count">Day 2</span>';
                    echo '<h3>Trade Indie</h3>';
                }
                if ( isset($day[$day_date]) ) {
                    $day_name = explode(',',$day[$day_date]);
                    echo '<h4 class="date">'.$day_name[0].'</h4>';
                    if ( isset($day[$day_times]) ) {
                        echo '<h4 class="times">'.$day_name[1].', '.strtolower($day[$day_times]).'</h4>';
                    }
                }
                $day_image = wp_get_attachment_image_src($day[$day_image][0], 'large' )[0];
                echo '<img class="day-image" src="'.$day_image.'" />';
                if ( $dayCount == 2 ) {
                    echo '<h4 class="day-title">'.rwmb_meta( 'ibe_past_show_consumer_title','', $pastshows_page->ID ).'</h4>';
                    echo '<h5 class="day-description">'.rwmb_meta( 'ibe_past_show_consumer_description','', $pastshows_page->ID ).'</h5>';
                }
                if ( $dayCount == 3 ) {
                    echo '<h4 class="day-title">'.rwmb_meta( 'ibe_past_show_trade_title','', $pastshows_page->ID ).'</h4>';
                    echo '<h5 class="day-description">'.rwmb_meta( 'ibe_past_show_trade_description','', $pastshows_page->ID ).'</h5>';
                }

                echo '</div>';
            }
        }
    echo '</section>';


?>
