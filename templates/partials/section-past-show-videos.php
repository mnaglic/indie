<?php
    echo '<section class="media video footer" id="footer-videos">';
        $show->ID = $show->ID;
        $show_enabled = rwmb_meta( 'ibe_show_details_enabled', array(), $show->ID );
        $footer_video_oembed        = rwmb_meta( 'ibe_footer_youtube_oembed', array(), $show->ID );
        $footer_video_tagline       = rwmb_meta( 'ibe_footer_video_tagline', array(), $show->ID );
        $footer_video_description   = rwmb_meta( 'ibe_footer_video_description', array(), $show->ID );
        $footer_anth_image          = rwmb_meta( 'ibe_footer_anthology_image', array(), $show->ID );
        $footer_anth_link           = rwmb_meta( 'ibe_footer_anthology_link', array(), $show->ID );
        $footer_anth_tagline        = rwmb_meta( 'ibe_footer_anthology_tagline', array(), $show->ID );
        $footer_anth_description    = rwmb_meta( 'ibe_footer_anthology_description', array(), $show->ID );

        if ($show_enabled === '1') {
                echo '<div data-id="'.$show->ID.'" class="show-footer is-selected">';

            $pos = strpos($footer_video_oembed,'embed/');
            if($pos === false) {
                $youtube_video = explode('://youtu.be/', $footer_video_oembed);
                $youtube_video = $youtube_video[0]; // replaced $youtube_video = $youtube_video[1];
            }
            else {
                $youtube_video = explode('embed/', $footer_video_oembed);
                $youtube_video = explode('?', $youtube_video[1]);
                $youtube_video = $youtube_video[0];
            }

            // video
            echo '<div class="media-object">';
            if ( isset($youtube_video ) && $youtube_video !== 'Embed HTML not available.' ) {
                echo '<div class="video-container overlay-active">';
                if ( isset($video['ibe_video_title']) ) {
                    echo '<a class="video-overlay"><span class="active">'.$video['ibe_video_title'].'</span></a>';
                } else {
                    echo '<div class="video-overlay"></div>';
                }
                echo '<iframe id="frame" width="100%" height="100%" src="https://www.youtube.com/embed/'.$youtube_video.'?modestbranding=1&showinfo=0&autoplay=0&controls=1&playsinline=1&loop=0&playlist='.$youtube_video.'" frameborder="0" allowfullscreen=""></iframe>';
                echo '</div>';
            }
            echo '<section class="media-header">';
            if ( isset($footer_video_tagline) ) {
                echo '<h3>'.$footer_video_tagline.'</h3>';
            }
            if ( isset($footer_video_description) ) {
                echo '<article>'.$footer_video_description.'</article>';
            }
            echo '</section>';
            echo '</div>';

            // anthology
            echo '<div class="media-object">';
            if ( isset($footer_anth_image ) ) {
                $footer_anth_image_id = key($footer_anth_image);
                $footer_anth_image = wp_get_attachment_image_src($footer_anth_image_id, 'large' )[0];
                echo '<div class="video-container overlay-active" style="background-image: url('.$footer_anth_image.')">';
                echo '<a class="video-overlay anthology" href="'.$footer_anth_link.'"></a>';
                echo '</div>';
            }
            echo '<section class="media-header">';
            if ( isset($footer_anth_tagline) ) {
                echo '<h3>'.$footer_anth_tagline.'</h3>';
            }
            if ( isset($footer_anth_description) ) {
                echo '<article>'.$footer_anth_description.'</article>';
            }
            echo '</section>';
            echo '</div>';

            echo '</div>'; // end show-footer
        }
    echo '</section>';

?>
