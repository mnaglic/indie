<?php



$active_popup = rwmb_meta( 'ibe_default_popup', '', '133' );
$popup = rwmb_meta( 'ibe_popup', '', $active_popup );
$image_id = $popup['ibe_popup_image'];
$image = wp_get_attachment_image_src($image_id[0], 'large' );
$image = $image[0];
if (isset($url)) {
    $url = $popup['ibe_popup_url'];
} else {
    $url = null;
}

if ( !isset($_COOKIE["modal_popup"]) && !is_404() ) { //if ( !isset($_COOKIE["modal_popup"]) && !is_404() ) {
    $classes = 'modal-popup cookie-modal invisible pop';
} else {
    $classes = 'modal-popup cookie-modal invisible';
}

    echo '<div class="'.$classes.'">';
    echo '  <div class="modal-bg"></div>';
    echo '  <div class="modal-popup-content">'; //  style="background-image: url('.$image.');"
    echo '      <span class="remove">&times;</span>';
    if ( $url !== null ) {
        echo '      <a href="'.$url.'">';
    }
    echo '          <div class="inner">';
    echo                $popup['ibe_popup_content'];
    echo '          </div>';
    if ( $url !== null ) {
        echo '      </a>';
    }
    echo '  </div>';
    echo '</div>';


?>
