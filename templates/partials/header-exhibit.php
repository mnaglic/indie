<?php
    // Headline
    $showlist_headline = rwmb_meta( 'ibe_exhibit_headline' );
    $showlist_background = rwmb_meta( 'ibe_exhibit_background', 'size=retina' );
    if ( isset($showlist_background) ) {
        reset($showlist_background);
        $first_key = key($showlist_background);
        $showlist_background = wp_get_attachment_image_src($first_key, 'retina');
        $showlist_background = 'style="background-image: url('.$showlist_background[0].');"';
    } else {
        $showlist_background = '';
    }

    echo '<section class="show-list" '.$showlist_background.'>';
    echo '<div class="header-wrapper">';
    if ( isset($showlist_headline) ) {
        echo '<h1>'.$showlist_headline.'</h1>';
    }
    echo '<div class="shows-container">';
    $default_show = rwmb_meta( 'ibe_default_show' );
    $shows = rwmb_meta( 'ibe_showlist_selection' );
    foreach ($shows as &$show) {
        $show_id = $show['ibe_show_item'];
        if ( $show_id === $default_show ) {
            echo '<div class="show is-selected" data-id="'.$show_id.'">';
        }   else {
            echo '<div class="show" data-id="'.$show_id.'">';
        }
        $show_title = rwmb_meta( 'ibe_show_details_city', array(), $show_id );
        $show_dates = rwmb_meta( 'ibe_show_details_dates', array(), $show_id );

        //echo '<a class="" href="#" data-id="'.$show_id.'">';
        echo '<h3>'.$show_title.'</h3>';
        echo '<h4>'.$show_dates.'</h4>';
        //echo '</a>';
        echo '</div>';
    }
    echo '</div>';
    echo '</div>';
    echo '</section>';

    echo '<section class="category-list">';

        $exhibitCategories = rwmb_meta( 'ibe_exhibit_categories' );
        $categoryCount = 0;

        foreach ($exhibitCategories as &$category) {
                echo '<div class="category">';
                if ( isset($category['ibe_exhibit_category_background']) ) {
                    $category_background = $category['ibe_exhibit_category_background'];
                    $category_background = wp_get_attachment_image_src($category_background[0], 'large' )[0];
                    echo '<img class="cat-image" src="'.$category_background.'" />';
                }

                $categoryCount++;

                echo '<section class="category-header">';
                if ( isset($category['ibe_exhibit_category_title']) ) {
                    echo '<h3>'.$category['ibe_exhibit_category_title'].'</h3>';
                }
                if ( isset($category['ibe_exhibit_category_description']) ) {
                    echo '<article class="description">'.$category['ibe_exhibit_category_description'].'</article>';
                }
                echo '</section>';
                echo '</div>';
        }

    echo '</section>';

    $exhibitTagline = rwmb_meta( 'ibe_exhibit_tagline' );
    $exhibitButtonText = rwmb_meta( 'ibe_exhibit_button_text' );
    $exhibitButtonUrl = rwmb_meta( 'ibe_exhibit_button_url' );

    echo '<section class="exhibit-cta">';
    if ( isset($exhibitTagline) ) {
        echo '<h2>'.$exhibitTagline.'</h2>';
    }
    if ( isset($exhibitButtonText) && isset($exhibitButtonUrl) ) {
        echo '<a class="button" href="'.$exhibitButtonUrl.'">'.$exhibitButtonText.'</a>';
    }
    echo '</section>';
?>
