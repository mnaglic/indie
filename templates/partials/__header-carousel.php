<?php
    // ////////////////////////////////////////////////////////////////////////////
    // Carousel
    // ////////////////////////////////////////////////////////////////////////////

    echo '<header id="landing" class="carousel">';

    $carousel_images = rwmb_meta( 'ibe_home_carousel_images' );
    if ( $carousel_images === '' ) {
        $landing_id = rwmb_meta( 'ibe_default_landing', '', '133' );
        $carousel_images = rwmb_meta( 'ibe_home_carousel_images', '', $landing_id );
    }

    foreach ($carousel_images as &$carousel_image) {
        if ( $carousel_image['ibe_home_carousel_slide_link'] !== 'none' ) {
            if ( isset($carousel_image['ibe_home_carousel_link_page']) ) {
                $slide_link = get_permalink($carousel_image['ibe_home_carousel_link_page']);
            } elseif ( isset($carousel_image['ibe_home_carousel_link_news']) ) {
                $slide_link = get_permalink($carousel_image['ibe_home_carousel_link_news']);
            } elseif ( isset($carousel_image['ibe_home_carousel_link_article']) ) {
                $slide_link = get_permalink($carousel_image['ibe_home_carousel_link_article']);
            } elseif ( isset($carousel_image['ibe_home_carousel_link_deck']) ) {
                $slide_link = get_permalink($carousel_image['ibe_home_carousel_link_deck']);
            } elseif ( isset($carousel_image['ibe_home_carousel_link_url']) ) {
                $slide_link = $carousel_image['ibe_home_carousel_link_url'];
            }
        } else {
            $slide_link = '#';
        }
        $img_sm_id = $carousel_image['ibe_home_carousel_img_mobile'];
        $slideImage_sm = wp_get_attachment_image_src($img_sm_id[0], 'retina' );
        $img_lg_id = $carousel_image['ibe_home_carousel_img_desktop'];
        $slideImage_lg = wp_get_attachment_image_src($img_lg_id[0], 'retina' );

        $bgpos = $carousel_image['ibe_home_carousel_bgpos'];
        if ( $bgpos !== null ) {
            $position = ' background-position: '. $bgpos;
        } else {
            $position = '';
        }
        if ( $carousel_image !== null ) {
            echo '<div class="carousel-cell">';
            if ( $slideImage_sm !== null ) {
                echo '<div class="carousel-image sm" style="background-image: url('.$slideImage_sm[0].');'.$position.'"></div>';
            }
            if ( $slideImage_lg !== null ) {
                echo '<div class="carousel-image lg" style="background-image: url('.$slideImage_lg[0].');'.$position.'"></div>';
            }
            echo '  <a href="'.$slide_link.'">';
            echo '      <article>';
            if (isset($carousel_image['ibe_home_carousel_content'])) {
                echo $carousel_image['ibe_home_carousel_content'];
            }
            echo '      </article>';
            echo '  </a>';
            echo '</div>';
        }
    }
    echo '</header>';
?>
