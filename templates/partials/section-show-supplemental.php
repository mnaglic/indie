<?php

//$exhibitors_enabled = rwmb_meta( 'ibe_exhibitors_enabled', array(), $show_id );
//$exhibitors_placeholder_image = rwmb_meta( 'ibe_exhibitor_comingsoon_image', array(), $attend->ID );

if ($show_enabled === '1') {
    echo '<div class="extra-details"><h3>WHO, WHAT, WHERE</h3></div>';
}

echo '<section class="details">';

// Venue

/*
$venue_enabled = rwmb_meta( 'ibe_venue_enabled', array(), $show_id );
$venue_placeholder_image = rwmb_meta( 'ibe_venue_comingsoon_image', array(), $show_id );
$exhibitors_enabled = rwmb_meta( 'ibe_exhibitors_enabled', array(), $show_id );
$exhibitors_placeholder_image = rwmb_meta( 'ibe_exhibitor_comingsoon_image', array(), $show_id );
*/

echo '<div class="day venue">';
$venue_image = rwmb_meta( 'ibe_venue_image', array(), $show_id );
$venue_location = rwmb_meta( 'ibe_venue_location', array(), $show_id );
$venue_address1 = rwmb_meta( 'ibe_venue_address1', array(), $show_id );
$venue_address2 = rwmb_meta( 'ibe_venue_address2', array(), $show_id );
$venue_address3 = rwmb_meta( 'ibe_venue_address3', array(), $show_id );
$venue_map = rwmb_meta( 'ibe_venue_map', array(), $show_id );
$venue_website = rwmb_meta( 'ibe_venue_website', array(), $show_id );

echo '<div class="day-header">';
if ($venue_enabled === '1') {
    reset($venue_image);
    $first_key = key($venue_image);
    if (  isset($venue_image) ) {
        $venue_image = wp_get_attachment_image_src($first_key, 'large' )[0];
        echo '<img class="day-image" src="'.$venue_image.'" />';
    }
} else {
    reset($venue_placeholder_image);
    $first_key = key($venue_placeholder_image);
    if (  isset($venue_placeholder_image) ) {
        $venue_placeholder_image = wp_get_attachment_image_src($first_key, 'large' )[0];
        echo '<img class="day-image" src="'.$venue_placeholder_image.'" />';
    }
}


echo '<div class="detail-wrapper">';
    if ($venue_enabled === '1') {

        if ( isset($venue_location) ) {
            echo '<div class="location-name">'.$venue_location.'</div>';
        }
        echo '<article class="description detail">';
        if ( isset($venue_address1) ) {
            echo '<address class="address">'.$venue_address1.'</address>';
        }
        if ( isset($venue_address2) ) {
            echo '<address class="address">'.$venue_address2.'</address>';
        }
        if ( isset($venue_address3) ) {
            echo '<address class="address">'.$venue_address3.'</address>';
        }

        echo '</article>';
    }
    echo '<div class="button-wrapper">';
        if ($venue_enabled === '1') {
            if ( isset($venue_website) ) {
                echo '<a class="button venue" href="'.$venue_website.'" target="_blank">See Venue</a>';
            }
            if ( isset($venue_map) ) {
                echo '<a class="button venue" href="'.$venue_map.'" target="_blank">View Map</a>';
            }
        } else {
            echo '<a class="button coming-soon" href="#">Coming Soon</a>';
        }
    echo '</div>';
echo '</div>';

echo '</div>';
echo '</div>';

// Exhibitors
//if ($show_enabled === '1') {
echo '<div class="day exhibitors">';
$exhibitor_image = rwmb_meta( 'ibe_exhibitor_image', array(), $show_id );
$exhibitor_headline = rwmb_meta( 'ibe_exhibitor_location', array(), $show_id );
$exhibitor_blurb = rwmb_meta( 'ibe_exhibitors_blurb', array(), $show_id );
$exhibitor_link = rwmb_meta( 'ibe_exhibitors_url', array(), $show_id );

echo '<div class="day-header">';
if ($exhibitors_enabled === '1') {
    reset($exhibitor_image);
    $first_key = key($exhibitor_image);
    if (  isset($exhibitor_image) ) {
        $exhibitor_image = wp_get_attachment_image_src($first_key, 'large' )[0];
        echo '<img class="day-image" src="'.$exhibitor_image.'" />';
    }
} else {
    reset($exhibitors_placeholder_image);
    $first_key = key($exhibitors_placeholder_image);
    if (  isset($exhibitors_placeholder_image) ) {
        $exhibitors_placeholder_image = wp_get_attachment_image_src($first_key, 'large' )[0];
        echo '<img class="day-image" src="'.$exhibitors_placeholder_image.'" />';
    }
}

echo '<div class="detail-wrapper">';
    if ($exhibitors_enabled === '1') {
        if ( isset($exhibitor_headline) ) {
            echo '<div class="location-name">'.$exhibitor_headline.'</div>';
        }
        if ( isset($exhibitor_blurb) ) {
            echo '<article class="description detail">';
            echo    $exhibitor_blurb;
            echo '</article>';
        }
    }
    echo '<div class="button-wrapper">';
    if ($exhibitors_enabled === '1') {
        if ( isset($exhibitor_link) ) {
            echo '<a class="button exhibitor-link" href="'.$exhibitor_link.'">View Lineup</a>';
        }
    } else {
        echo '<a class="button coming-soon" href="#">Coming Soon</a>';
    }

    echo '</div>';
echo '</div>';

echo '</div>';
echo '</div>';
//}

echo '</section>';
