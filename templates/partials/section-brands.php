<?php
    // Brand
    $brands = rwmb_meta( 'bs_exclusive_brands' );
    echo '<section class="brands">';
    $siteRoot = get_option("siteurl");
    foreach ($brands as &$brand) {
        if ( is_user_logged_in() ) {
            $link = '<a href="'.$brand['bs_brand_link'].'"style="background-image: url('.$brand['bs_brand_logo'].');" data-name="'.$brand['bs_brand_name'].'"><span>Shop '.$brand['bs_brand_name'].'</span></a>';
        } else {
            $link = '<a href="'.$siteRoot.'/signup?notice=true'.'"style="background-image: url('.$brand['bs_brand_logo'].');"></a>';
        }
        //var_dump($brand);
        if ( $brand['bs_brand_name'] !== '' ) {
            echo '<div class="brand">';
            echo        $link;
            echo '<aside>'.$brand['bs_brand_blurb'].'</aside>';

            echo '</div>';
        }
    }
    echo '</section>';
?>
