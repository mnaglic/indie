// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Exhibit Template
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Exhibit - Header
$meta_boxes[] = array(
    'title'  => __( 'Header', 'ibe' ),
    'post_types' => array('page'),
    'include' => array(
        'template' => array(
            'templates/exhibit.php',
        ),
    ),
    'fields' => array(
        array(
            'name' => __('Headline','ibe'),
            'tooltip' => __('The headline for this page','ibe'),
            'id' => $prefix . 'trade_headline',
            'type' => 'text',
            'size'  => '60',
        ),
        array(
            'id'          => $prefix . 'trade_background',
            'name'        => __( 'Background Image', 'ibe' ),
            'tooltip' => __('Image to be used as the background of the header section. Minimum dimensions: 1400 x 566 px.','ibe'),
            'type' => 'image_advanced',
            'max_file_uploads' => 1,
            'max_status' => false,
        ),
    ),
);

// Exhibit - Shows
$meta_boxes[] = array(
    'title'  => __( 'Show Days', 'ibe' ),
    'post_types' => array('page'),
    'include' => array(
        'template' => array( 'templates/exhibit.php' ),
    ),
    'fields' => array(
        array(
            'id'     => $prefix . 'days',
            'type'   => 'group',
            'clone'  => false,
            'sort_clone' => false,
            'fields' => array(
                array(
                    'name' => __('Day 1 Background','ibe'),
                    'tooltip' => __('The background for day 1.','ibe'),
                    'id' => $prefix . 'day_1_background',
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                    'max_status' => false,
                ),
                array(
                    'name' => __('Day 2 Background','ibe'),
                    'tooltip' => __('The background for day 2.','ibe'),
                    'id' => $prefix . 'day_2_background',
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                    'max_status' => false,
                ),
                array(
                    'name' => __('Day 3 Background','ibe'),
                    'tooltip' => __('The background for day 3.','ibe'),
                    'id' => $prefix . 'day_3_background',
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                    'max_status' => false,
                ),
            ),
        ),
    ),
);
