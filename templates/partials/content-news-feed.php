<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package iBE_2016
 */
global $authordata;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
	<header class="entry-header">

		<div class="entry-meta">
			<?php ibe__posted_on(); ?>
			|
			<?php ibe_news_categories(); ?>
		</div><!-- .entry-meta -->

		<?php
		if ( has_post_thumbnail( $page_id ) ) {
			$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
			$style = 'background-image: url('.$src[0].')';
		} else {
			$style = '';
		}

		?>
		<div class="featured-image post-bg" style="<?php echo $style; ?>"></div>

	</header><!-- .entry-header -->
<?php echo '<h1 class="entry-title">'.str_replace(' | ', ' ', get_the_title()).'</h1>'; ?>
	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'ibe_' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ibe_' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php ibe_news_entry_footer(); ?>
		<?php social_sharing_bar( $post->ID ); ?>
	</footer><!-- .entry-footer -->
	<?php
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) {
			comments_template();
		}
	?>
</article><!-- #post-## -->
