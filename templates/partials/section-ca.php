<?php

    echo '<main>';
    echo '  <ul class="ca">';
    foreach ( $ca_items as &$ca_item ) {
        echo '<li>';
        if ( isset($ca_item['ibe_contact_title']) && $ca_item['ibe_contact_title'] !== '' ) {
            echo '<h4>'.$ca_item['ibe_contact_title'].'</h4>';
            echo '<p>';
            echo    '<span>'.$ca_item['ibe_contact_name'].'</span>';
            echo '  <a href="mailto:'.$ca_item['ibe_contact_email'].'">'.$ca_item['ibe_contact_email'].'</a>';
            echo '</p>';
        }
        echo '</li>';
    }
    echo '  </ul>';
    echo '</main>';

?>
