<?php
if ( isset($day['ibe_day'.$dayCount.'_exhibitors_image']) ) {
    $exhibitor_image = $day['ibe_day'.$dayCount.'_exhibitors_image'];
}
if ( isset($day['ibe_day'.$dayCount.'_exhibitors_location']) ) {
    $exhibitor_headline = $day['ibe_day'.$dayCount.'_exhibitors_location'];
}
if ( isset($day['ibe_day'.$dayCount.'_exhibitors_blurb']) ) {
    $exhibitor_blurb = $day['ibe_day'.$dayCount.'_exhibitors_blurb'];
}
if ( isset($day['ibe_day'.$dayCount.'_exhibitors_url']) ) {
    $exhibitor_link = $day['ibe_day'.$dayCount.'_exhibitors_url'];
}
if ( isset($day['ibe_day'.$dayCount.'_exhibitors_buttontext']) ) {
    $exhibitor_text = $day['ibe_day'.$dayCount.'_exhibitors_buttontext'];
} else {
    $exhibitor_text = 'View Lineup';
}

if (isset($day['ibe_day'.$dayCount.'_exhibitors_enabled'])) {
    echo '<div class="day exhibitor-list-inline">';
        echo '<div class="day-header">';
            if ( isset($day['ibe_day'.$dayCount.'_exhibitors_image']) ) {
                $exhibitor_image = $exhibitor_image[0];
                $exhibitor_image = wp_get_attachment_image_src($exhibitor_image, 'large' )[0];
                echo '<img class="day-image" src="'.$exhibitor_image.'" />';
            } else {
                $exhibitors_placeholder_image = rwmb_meta( 'ibe_exhibitor_comingsoon_image', array(), $attend->ID );
                if (  isset($exhibitors_placeholder_image) ) {
                    reset($exhibitors_placeholder_image);
                    $first_key = key($exhibitors_placeholder_image);
                    if (  isset($exhibitors_placeholder_image) ) {
                        $exhibitors_placeholder_image = wp_get_attachment_image_src($first_key, 'large' )[0];
                        echo '<img class="day-image placeholder" src="'.$exhibitors_placeholder_image.'" />';
                    }
                }
            }
            echo '<div class="detail-wrapper">';
                if ( isset($exhibitor_headline) ) {
                    echo '<div class="location-name">'.$exhibitor_headline.'</div>';
                }
                if ( isset($exhibitor_blurb) ) {
                    echo '<article class="description detail">'.$exhibitor_blurb.'</article>';
                }
                if ( isset($exhibitor_link) ) {
                    echo '<div class="button-wrapper">';
                    echo '<a class="button exhibitor-link" href="'.$exhibitor_link.'">'.$exhibitor_text.'</a>';
                    echo '</div>';
                } else {
                    echo '<div class="button-wrapper">';
                    echo '<a class="button exhibitor-link coming-soon" href="#">Coming Soon</a>';
                    echo '</div>';
                }
            echo '</div>';
        echo '</div>';
    echo '</div>';
}
