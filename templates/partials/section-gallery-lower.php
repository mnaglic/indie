<?php

    echo '<section class="gallery">';
        $gallery = rwmb_meta( 'ibe_gallery_bottom' );
        foreach ($gallery as &$image) {
            if ( isset($image['ibe_gallery_image']) ) {
                $imageBackground = wp_get_attachment_image_src($image['ibe_gallery_image'][0], 'large' );
                $imageBackground = $imageBackground[0];

            } else {
                $imageBackground = null;
            }
            echo '<div class="media-object">';
                if ( $imageBackground !== null ) {
                    echo '<div class="image post-bg" style="background-image: url('.$imageBackground.');">';
                } else {
                    echo '<div class="image post-bg">';
                }
            echo '</div>';

            echo '</div>';
        }
    echo '</section>';

?>
