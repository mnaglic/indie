<?php
    // ////////////////////////////////////////////////////////////////////////////
    // Carousel
    // ////////////////////////////////////////////////////////////////////////////

    $header_type = rwmb_meta( 'ibe_home_header_type' );

    // header Type
    if ( isset($header_type) ) {
        if ( $header_type === 'none' || $header_type === '' ) {
            echo '<header></header>';
        } elseif ( $header_type === 'carousel' ) {
            echo '<header id="landing" class="carousel">';
            $carousel_slides = rwmb_meta( 'ibe_home_carousel_images' );
            //var_dump($carousel_slides);
            foreach ($carousel_slides as &$carousel_slide) {
                // Link Type
                if ( isset($carousel_slide['ibe_home_carousel_slide_link']) ) {
                    if ( $carousel_slide['ibe_home_carousel_slide_link'] !== 'none' ) {
                        if ( isset($carousel_slide['ibe_home_carousel_link_page']) ) {
                            $slide_link = get_permalink($carousel_slide['ibe_home_carousel_link_page']);
                        } elseif ( isset($carousel_slide['ibe_home_carousel_link_news']) ) {
                            $slide_link = get_permalink($carousel_slide['ibe_home_carousel_link_news']);
                        } elseif ( isset($carousel_slide['ibe_home_carousel_link_article']) ) {
                            $slide_link = get_permalink($carousel_slide['ibe_home_carousel_link_article']);
                        } elseif ( isset($carousel_slide['ibe_home_carousel_link_deck']) ) {
                            $slide_link = get_permalink($carousel_slide['ibe_home_carousel_link_deck']);
                        } elseif ( isset($carousel_slide['ibe_home_carousel_link_url']) ) {
                            $slide_link = $carousel_slide['ibe_home_carousel_link_url'];
                        }
                    } else {
                        $slide_link = '#';
                    }
                }
                // Image - Mobile
                if ( isset($carousel_slide['ibe_home_carousel_img_mobile']) ) {
                    $img_sm_id = $carousel_slide['ibe_home_carousel_img_mobile'];
                    $slideImage_sm = wp_get_attachment_image_src($img_sm_id[0], 'retina' );
                } else {
                    $slideImage_sm = null;
                }
                // Image - Desktop
                if ( isset($carousel_slide['ibe_home_carousel_img_desktop']) ) {
                    $img_lg_id = $carousel_slide['ibe_home_carousel_img_desktop'];
                    $slideImage_lg = wp_get_attachment_image_src($img_lg_id[0], 'retina' );
                } else {
                    $slideImage_lg = null;
                }
                // Background Position
                if ( isset($carousel_slide['ibe_home_carousel_bgpos']) ) {
                    $bgpos = $carousel_slide['ibe_home_carousel_bgpos'];
                    if ( $bgpos !== null ) {
                        $position = ' background-position: '. $bgpos;
                    }
                } else {
                    $position = '';
                }

                // Youtube
                if ( isset($carousel_slide['ibe_home_carousel_youtube']) ) {
                    $youtube_src = $carousel_slide['ibe_home_carousel_youtube'];
                    $youtube_src = explode("/",$youtube_src);
                    if ($youtube_src[2] === 'youtu.be') { // Youtube
                        $youtube_src = $youtube_src[3];
                    } else {
                        $youtube_error = 'Error: Unknown share code format. Please make sure to input a code that looks like this: https://youtu.be/SLR8e1i_3eQ';
                    }
                    if ( isset($carousel_slide['ibe_home_carousel_youtube_poster']) ) {
                        $youtube_poster = $carousel_slide['ibe_home_carousel_youtube_poster'];
                        $youtube_poster = wp_get_attachment_image_src($img_lg_id[0], 'retina' );
                    } else {
                        $youtube_poster = null;
                    }
                } else {
                    $youtube_src = null;
                }

                if ( isset($carousel_slide['ibe_home_slide_type']) ) {
                    if ( $carousel_slide['ibe_home_slide_type'] === 'video' ) {
                        $height_class = ' video-cell';
                        $calc_height = '';
                        $cell_height = ' style="height: calc(100vh - 13vw);"';
                        $video_height = ' style="min-height: calc(100vh - 13vw);"';
                    } else {
                        $height_class = ' fixed-height';
                        $calc_height = ' ';
                        $cell_height = '';
                    }
                }

                if ( isset ($carousel_slide)) {
                    echo '<div class="carousel-cell'.$height_class.'"'.$cell_height.'>';
                    if (  isset($slideImage_sm) ) {
                        echo '<div class="carousel-image sm" style="background-image: url('.$slideImage_sm[0].');'.$position.'"></div>';
                    }
                    if ( isset($slideImage_lg) ) {
                        echo '<div class="carousel-image lg" style="background-image: url('.$slideImage_lg[0].');'.$position.'"></div>';
                    }
                    if ( isset($carousel_slide['ibe_home_carousel_video_mp4'])  ) {
                        echo '<video autoplay poster="'.$video_poster.'" loop'.$muted.$video_height.'>';
                        echo '  <source src="'.$video_src_mp4.'" />';
                        echo '  <source src="'.$video_src_webm.'" />';
                        echo '  Sorry, your browser doesn\'t support HTML5 video.';
                        echo '</video>';
                    }
                    echo '  <a href="'.$slide_link.'">';

                    if (isset($carousel_slide['ibe_home_carousel_content'])) {
                        echo '<article>';
                        echo    $carousel_slide['ibe_home_carousel_content'];
                        echo '</article>';
                    }
                    echo '  </a>';
                    echo '</div>';
                }
            }
            echo '</header>'."\r\n";
        } elseif ( $header_type === 'youtube' ) {
            $header_youtube = rwmb_meta( 'ibe_home_carousel_youtube' );

            // Shortcode
            if ( isset($header_youtube['ibe_home_carousel_youtube_oembed']) ) {
                $shortcode = $header_youtube['ibe_home_carousel_youtube_oembed'];
                $pos = strpos($shortcode,'embed/');
                if($pos === false) {
                    $shortcode = explode('://youtu.be/', $shortcode);
                    $shortcode = $shortcode[1];
                }
                else {
                    $shortcode = explode('embed/', $shortcode);
                    $shortcode = explode('?', $shortcode[1]);
                    $shortcode = $shortcode[0];
                }
            } else {
                $shortcode = '';
            }

            // Mobile Fallback
            if ( isset($header_youtube['ibe_home_carousel_youtube_mobile']) ) {
                $img_id = $header_youtube['ibe_home_carousel_youtube_mobile'];
                $mobileImage = wp_get_attachment_image_src($img_id[0], 'retina' );
            } else {
                $mobileImage = null;
            }

            // Link Type
            if ( isset($header_youtube['ibe_home_carousel_youtube_link']) ) {
                if ( $header_youtube['ibe_home_carousel_youtube_link'] !== 'none' ) {
                    if ( isset($header_youtube['ibe_home_carousel_link_page']) ) {
                        $slide_link = get_permalink($header_youtube['ibe_home_carousel_link_page']);
                    } elseif ( isset($carousel_slide['ibe_home_carousel_link_news']) ) {
                        $slide_link = get_permalink($header_youtube['ibe_home_carousel_link_news']);
                    } elseif ( isset($carousel_slide['ibe_home_carousel_link_article']) ) {
                        $slide_link = get_permalink($header_youtube['ibe_home_carousel_link_article']);
                    } elseif ( isset($carousel_slide['ibe_home_carousel_link_deck']) ) {
                        $slide_link = get_permalink($header_youtube['ibe_home_carousel_link_deck']);
                    } elseif ( isset($header_youtube['ibe_home_carousel_link_url']) ) {
                        $slide_link = $header_youtube['ibe_home_carousel_link_url'];
                    }
                } else {
                    $slide_link = '#';
                }
            }
            if ( isset($header_youtube['ibe_home_carousel_youtube_playlist']) ) {
                $slide_playlist = '&list=' . $header_youtube['ibe_home_carousel_youtube_playlist'];
            } else {
                $slide_playlist = '';
            }
            echo '<header id="landing" class="youtube" style="background-image: url('.$mobileImage[0].'); background-position: center">';
            echo '  <a class="video-link" href="'.$slide_link.'"></a>';
            echo '  <div class="backdrop"></div>';
            https://youtubeloop.net/watch?v=ZfY1SiVyNZk
            echo '  <div class="video-container"> <iframe id="ytembed" width="100%" height="100%" src="https://www.youtube.com/embed/'.$shortcode.'?modestbranding=1&showinfo=0&autoplay=1&controls=0&playsinline=1&loop=1'.$slide_playlist.'&enablejsapi=1" frameborder="0" allowfullscreen=""></iframe></div>';
            echo '</header>';
        }
    } else {
        echo 'NO HEADER TYPE SELECTED';
    }

?>
