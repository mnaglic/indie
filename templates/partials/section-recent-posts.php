<?php

// ////////////////////////////////////////////////////////////////////////////
// Recent Posts
// ////////////////////////////////////////////////////////////////////////////


$the_query = new WP_Query( 'post_type=post&posts_per_page=6' );
$related_articles = $the_query->posts;
/*
$carousel_images = rwmb_meta( 'ibe_home_carousel_images' );
if ( $carousel_images === '' ) {
    $landing_id = rwmb_meta( 'ibe_default_landing', '', '133' );
    $carousel_images = rwmb_meta( 'ibe_home_carousel_images', '', $landing_id );
}
*/
echo '<section class="related-posts">';
echo '<h4>More from iBE</h4>';
foreach ($related_articles as &$article) {
    $item_id = $article->ID;
    $item_title = str_replace(' | ', ' ', $article->post_title);
    $item_link = get_permalink( $item_id );

    if ( has_post_thumbnail( $item_id ) ) {
        $image_array_lg = wp_get_attachment_image_src( get_post_thumbnail_id( $item_id ), 'large' );
        $image_large = $image_array_lg[0];
        $image_array_rt = wp_get_attachment_image_src( get_post_thumbnail_id( $item_id ), 'retina' );
        $image_retina = $image_array_rt[0];
        $has_image = true;
    } else {
        $has_image = null;
    }
    if ( $item_title !== '' ) {
        echo '<div class="related-post">';
        echo '  <div class="photo">';
            echo '<a href="'.$item_link.'">';
            if ( $has_image !== null ) {
                    echo '<div style="background-image: url('.$image_large.');"></div>';
            } else {
                echo '<div class="post-bg"></div>';
            }
            echo '  </a>';

        echo '    <h3><a href="'.$item_link.'" class="ha">'.$item_title.'</a></h3>';

        echo '  </div>';
        echo '</div>';
    }
}
echo '</section>';
wp_reset_postdata();
?>
