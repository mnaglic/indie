<?php
$contacts = rwmb_meta( 'ibe_contact' );

echo '<main>';
echo '  <ul class="ca">';
foreach ( $contacts as &$contact ) {
    echo '<li>';
    if ( isset($contact['ibe_contact_title']) && $contact['ibe_contact_title'] !== '' ) {
        if ( isset($contact['ibe_contact_subhead']) && $contact['ibe_contact_subhead'] !== '' ) {
            echo '<h4><a href="'.$contact['ibe_contact_subhead'].'">'.$contact['ibe_contact_title'].'</a></h4>';
        } else {
            echo '<h4>'.$contact['ibe_contact_title'].'</h4>';
        }
        echo '<p>';
        echo '  <a href="mailto:'.$contact['ibe_contact_email'].'">'.$contact['ibe_contact_email'].'</a>';
        echo '</p>';
    }
    echo '</li>';
}
echo '  </ul>';
echo '</main>';

?>
