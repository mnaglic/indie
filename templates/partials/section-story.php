<?php

        $storyTitle = rwmb_meta( 'ibe_story_title' );
        $story = rwmb_meta( 'ibe_story_sections' );

        echo '<section class="story">';
        echo    '<h3>'.$storyTitle.'</h3>';
        echo    '<ul class="ca">';

            foreach ($story as &$section) {

                echo '<li>';
                if ( isset($section['ibe_section_title']) ) {
                    echo '<h4>'.$section['ibe_section_title'].'</h4>';
                }
                if ( isset($section['ibe_section_description']) ) {
                    echo '<p>'.$section['ibe_section_description'].'</p>';
                }
                echo '</li>';
            }

        echo    '</ul>';
        echo '</section>';


?>
