<?php
    // Headline
    $header_headline = rwmb_meta( 'ibe_header_headline' );
    $header_paragraph = rwmb_meta( 'ibe_header_paragraph' );
    $header_background = rwmb_meta( 'ibe_header_background', 'size=retina' );

    if ( isset($header_background) ) {
        reset($header_background);
        $first_key = key($header_background);
        $header_background = wp_get_attachment_image_src($first_key, 'retina');
        $header_background = 'style="background-image: url('.$header_background[0].');"';
    } else {
        $header_background = '';
    }

    echo '<header class="title" '.$header_background.'>';
    if ( isset($header_headline) ) {
        echo '<h1>'.$header_headline.'</h1>';
        echo '<p>'.$header_paragraph.'</p>';
    }
    echo '</header>';
?>
