<?php

$attend_page = get_page_by_path('attend', OBJECT);
$sidebar_show_id = rwmb_meta( 'ibe_el_show' );

$sidebar_access_cta         = rwmb_meta( 'ibe_attend_sidebar_access_cta', array(), $attend_page->ID );
$sidebar_exhibit_cta        = rwmb_meta( 'ibe_attend_sidebar_exhibit_cta', array(), $attend_page->ID );
$sidebar_exhibit_url        = rwmb_meta( 'ibe_attend_sidebar_exhibit_url', array(), $attend_page->ID );

$sidebar_newsfeed_headline  = rwmb_meta( 'ibe_attend_sidebar_newsfeed_headline', array(), $attend_page->ID );
$sidebar_newsletter_cta     = rwmb_meta( 'ibe_attend_sidebar_newsletter_cta', array(), $attend_page->ID );

$sidebar_shows = rwmb_meta( 'ibe_showlist_selection', array(), $attend_page->ID );
echo '<div class="sidebar-header">';

    $sidebar_show_tag = rwmb_meta( 'ibe_show_details_news_tag', array(), $sidebar_show_id );
    $sidebar_show_city = rwmb_meta( 'ibe_show_details_city', array(), $sidebar_show_id );
    $sidebar_access_buyer = rwmb_meta( 'ibe_attend_sidebar_access_buyer', array(), $sidebar_show_id );
    $sidebar_access_press = rwmb_meta( 'ibe_attend_sidebar_access_press', array(), $sidebar_show_id );
    $access_links = '';

    if ( (isset($sidebar_access_buyer) && $sidebar_access_buyer !== '') && (isset($sidebar_access_press) && $sidebar_access_press !== '') ) {
        $access_links = 'accesspresent';
    }

    if ( isset($sidebar_access_buyer)  || isset($sidebar_access_press)  ) {
            echo '<div id="accesslinks-'.$sidebar_show_id.'" class="button-wrapper access '.$access_links.' is-selected" data-id="'.$sidebar_show_id.'">';
            if ($access_links !== '') {
                if ( isset($sidebar_access_cta) ) {
                    echo '    <div class="sidebar-msg access">';
                    echo $sidebar_access_cta;
                    echo '    </div>';
                }
                if ( isset($sidebar_access_buyer) ) {
                    echo '<a class="button sidebar-half" href="'.$sidebar_access_buyer.'">Buyers</a>';
                }
                if ( isset($sidebar_access_press) ) {
                    echo '<a class="button sidebar-half" href="'.$sidebar_access_press.'">Press</a>';
                }
            }
        echo '</div>';
    }


    if ( isset($sidebar_exhibit_cta) ) {
        echo '<div class="sidebar-msg exhibit">';
        echo $sidebar_exhibit_cta;
        echo '</div>';
    }

        if ( isset($sidebar_exhibit_url) ) {
            echo '<div class="button-wrapper">';
            echo '  <a class="button sidebar" href="'.$sidebar_exhibit_url.'">Inquire To Exhibit</a>';
            echo '</div>';
        }

echo '</div>';

echo '<div class="sidebar-main">';

    // Newsfeed
    echo '<div class="news-feeds">';
            $sidebar_show_tag = rwmb_meta( 'ibe_show_details_news_tag', array(), $sidebar_show_id );
            $sidebar_show_city = rwmb_meta( 'ibe_show_details_city', array(), $sidebar_show_id );

            echo '<div id="feed-'.$sidebar_show_id.'" class="show-newsfeed is-selected" data-id="'.$sidebar_show_id.'">';

            if ( isset($sidebar_newsfeed_headline) ) {
                echo '<h5 class="headline">';
                //echo $sidebar_newsfeed_headline . ' ' . $sidebar_show_city;
                echo $sidebar_show_city . ' News';
                echo '</h5>';
            }

            $sidebarloop = new WP_Query( 'post_type=post&posts_per_page=5&tag=' . $sidebar_show_tag );
            while ( $sidebarloop->have_posts() ) : $sidebarloop->the_post();
                echo '  <article id="post-'.get_the_ID().'">';
                echo '      <header class="entry-header">';
                if ( has_post_thumbnail( $post->ID ) ) {
                    $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
                    $style = 'background-image: url('.$src[0].')';
                } else {
                    $src = false;
                }
                if ($src) {
                    echo '          <a href="'.get_permalink($post->ID).'">';
                    echo '              <img class="featured-image post-bg" src="'.$src[0].'" />';
                    echo '          </a>';
                }
                echo '      </header>';
                echo '      <a class="article-title" href="'.get_permalink($post->ID).'"><h6 class="entry-title">'.str_replace(' | ', ' ', get_the_title()).'</h6></a>';
                echo '  </article>';

            endwhile;
            wp_reset_postdata();
            echo '<a class="view-more" href="/tag/'.$sidebar_show_tag.'">View More</a>';
            echo '</div>';
    echo '</div>';

    // Newsletter
    echo '<div class="newsletter">';
        if ( isset($sidebar_newsletter_cta) ) {
            echo '<div class="newsletter-cta">';
            echo    $sidebar_newsletter_cta;
            echo '</div>';
        }
        echo '<form action="//Indiebeautyexpo.us9.list-manage.com/subscribe/post-json?u=f4a3d85a1388923b3fe3a6cb9&amp;id=4ed1cffcc8" method="post" name="mc-embedded-subscribe-form" class="mc-embedded-subscribe-form validate" novalidate>';
        echo '  <input type="email" placeholder="Enter email" name="EMAIL" class="required mc-embedded-email">';
        echo '  <input type="submit" value="Sign up" name="subscribe" class="button mc-embedded-subscribe">';
        echo '</form>';
    echo '</div>';

echo '</div>';

?>
