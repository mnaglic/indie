<?php

    $show_city = rwmb_meta( 'ibe_show_details_city_abbreviation', array(), $show->ID );
    $show_dates = rwmb_meta( 'ibe_show_details_dates', array(), $show->ID );
    $show_type = rwmb_meta( 'ibe_show_header_type', array(), $show->ID );
    $show_archive_headline = rwmb_meta( 'ibe_show_archive_headline', array(), $show->ID );
    if ($show_archive_headline === '') {
        $show_archive_headline = 'Thanks for Attending';
    }

    $pastshows_page = get_page_by_title( 'Past Shows' );
    $past_shows_headertext = rwmb_meta( 'ibe_past_show_header_text','', $pastshows_page->ID );

    $show_header_type = rwmb_meta( 'ibe_show_header_type', array(), $show->ID );

    if ( $show_header_type === 'vimeo' ) {
        $show_video = rwmb_meta( 'ibe_show_header_video', array(), $show->ID );
        $show_image_id = $show_video['ibe_show_header_vimeo_mobile'][0];
        $show_image = wp_get_attachment_image_src($show_image_id, 'retina');
        $show_image = $show_image[0];
    } elseif ( $show_header_type === 'image' ) {
        $show_image_id = rwmb_meta( 'ibe_show_header_image', array(), $show->ID );
        $show_image_id = $show_image_id['ibe_show_background'][0];
        if ( isset($show_image_id) ) {
            $show_image = wp_get_attachment_image_src($show_image_id, 'retina');
            $show_image = $show_image[0];
        } else {
            $show_image = null;
        }
    } else {
        $show_image = null;
    }

    if ( $show_image ) {
        echo '<header class="title" data-id="'.$show->ID.'" style="background-image: url('.$show_image.');">';
    } else {
        echo '<header class="title placeholder" data-id="'.$show->ID.'">';
    }
    echo '  <h2>'.$show_archive_headline.'</h2>';
    echo '  <h3>IBE '.$show->post_title.'</h3>';
    echo '  <h3>'.$show_dates.'</h3>';
    echo '  <h4>'.rwmb_meta( 'ibe_venue_location', array(), $show->ID ).'</h4>';
    echo '  <a class="back-link" href="/past-shows/">View all past shows</a>';
    echo '</header>';
    echo '<section class="headline">';
    echo    $past_shows_headertext;
    echo '</section>';
?>
