<?php

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
        if (isset($emailSent) && $emailSent == true) {
            echo '<div class="alert alert-success"><p>Thanks, your email was sent successfully.</p></div>';
        } else {
            // Error Handling
            if (isset($hasError) || isset($captchaError)) {
                echo '<div class="alert alert-danger alert-dismissable">';
                echo '  <a class="close" data-dismiss="alert">×</a>';
                echo '  <h4 class="alert-heading">Sorry, an error occured.</h4>';
                echo '  <p class="error">Please try again!<p>';
                echo '</div>';
            }
            echo '<form action="<?php the_permalink(); ?>" id="contactForm" method="post">';
            echo '    <fieldset>';
            echo '        <div class="form-group">';
            echo '            <input class="form-control" type="text" name="contactName" id="contactName" value="" placeholder="Name" />';
            echo '            <span class="input-icon fui-check-inverted"></span>';
            echo '            <?php if(isset($nameError)) { ?>';
            echo '            <p><span class="error"><?=$nameError;?></span></p>';
            echo '            <?php } ?>';
            echo '             <br>';
            echo '        </div>';
            echo '        <div class="form-group">';
            echo '            <input class="form-control" type="text" name="email" id="email" value="" placeholder="Email" />';
            echo '            <span class="input-icon fui-check-inverted"></span>';
            echo '            <?php if(isset($emailError)) { ?>';
            echo '            <p><span class="error"><?=$emailError;?></span></p>';
            echo '            <?php } ?>';
            echo '             <br>';
            echo '        </div>';
            echo '        <div class="form-group">';
            echo '            <textarea class="form-control" name="comments" id="commentsText" rows="10" cols="20" placeholder="Comments"></textarea>';
            echo '            <?php if(isset($commentError)) { ?>';
            echo '            <p><span class="error"><?=$commentError;?></span></p>';
            echo '            <?php } ?>';
            echo '             <br>';
            echo '        </div>';
            echo '        <div class="form-actions">';
            echo '            <button type="submit" class="btn btn-success">SUBMIT</button>';
            echo '        </div>';
            echo '        <input type="hidden" name="submitted" id="submitted" value="true" />';
            echo '    </fieldset>';
            echo '</form>';
        }
	} // end while
} else {
    echo '<h4 class="title-404">Not Found</h4>';
}
?>
