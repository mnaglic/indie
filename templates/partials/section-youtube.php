<?php
$youtube_video = rwmb_meta( 'ibe_page_youtube_oembed' );
$youtube_autoplay = rwmb_meta( 'ibe_page_youtube_autoplay' );
$youtube_link = rwmb_meta( 'ibe_page_youtube_link' );
//var_dump($youtube_video);
if ( isset($youtube_video ) && $youtube_video !== 'Embed HTML not available.' ) {

    $pos = strpos($youtube_video,'embed/');
    if($pos === false) {
        $youtube_video = explode('://youtu.be/', $youtube_video);
        var_dump($youtube_video);
        $youtube_video = $youtube_video[1];
    }
    else {
        $youtube_video = explode('embed/', $youtube_video);
        $youtube_video = explode('?', $youtube_video[1]);
        $youtube_video = $youtube_video[0];
    }

    if ( isset($youtube_autoplay) && $youtube_autoplay !== '' ) {
        if ( $youtube_autoplay === 'on') {
            $youtube_autoplay = '&autoplay=1&enablejsapi=1';
        } else {
            $youtube_autoplay = '&autoplay=0';
        }
    } else {
        $youtube_autoplay = '&autoplay=0';
    }

    echo '<section class="youtube">';
    if ( isset($youtube_link) && $youtube_link !== '' ) {
        echo '  <a class="video-link" href="'.$youtube_link.'"></a>';
    }
    echo '  <div class="video-container"> <iframe id="ytembed" width="100%" height="100%" src="https://www.youtube.com/embed/'.$youtube_video.'?modestbranding=1&showinfo=0'.$youtube_autoplay.'&controls=0&playsinline=1&loop=1&playlist='.$youtube_video.'" frameborder="0" allowfullscreen=""></iframe></div>';
    echo '</section>';
}

?>
