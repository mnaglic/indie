<?php
    // Featured Team Member 1
    $team_members = rwmb_meta( 'bs_about_team' );
    foreach ($team_members as &$member) {
        if ( $member['bs_about_team_featured'] === 'true' ) {
            if ( $member['bs_about_team_name'] !== '' ) {
                echo '<section class="team_featured">';
                echo '  <div class="photo">';
                if ( $member['bs_about_team_img'] !== '' ) {
                    echo '<div style="background-image: url('.$member['bs_about_team_img'].');"></div>';
                }
                echo '    <h4>';
                echo        $member['bs_about_team_name'];
                echo '    </h4>';
                if ( $member['bs_about_team_title'] !== '' ) {
                    echo '     <p>';
                    echo          $member['bs_about_team_title'];
                    echo '    </p>';
                }
                echo '  </div>';
                if ( $member['bs_about_team_bio'] !== '' ) {
                    if ( $member['bs_about_team_singlecol'] === 'true' ) {
                        echo '<article class="one-col">';
                    } else {
                        echo '<article>';
                    }
                    echo    $member['bs_about_team_bio'];
                    echo '</article>';
                }
                echo '</section>';
            }
        }
    }
    echo '<section class="team_standard">';
    foreach ($team_members as &$member) {
        if ( $member['bs_about_team_featured'] === 'false' ) {
            if ( $member['bs_about_team_name'] !== '' ) {
                echo '<div class="team_member">';
                echo '  <div class="photo">';
                if ( $member['bs_about_team_img'] !== '' ) {
                    echo '<div style="background-image: url('.$member['bs_about_team_img'].');"></div>';
                }
                echo '  </div>';
                echo '    <h4>';
                echo        $member['bs_about_team_name'];
                echo '    </h4>';
                if ( $member['bs_about_team_title'] !== '' ) {
                    echo '     <p>';
                    echo          $member['bs_about_team_title'];
                    echo '    </p>';
                }
                if ( $member['bs_about_team_bio'] !== '' ) {
                    echo '<article>';
                    echo    $member['bs_about_team_bio'];
                    echo '</article>';
                }
                echo '</div>';
            }
        }
    }
    echo '</section>';
?>
