<?php
// Exhibitors

if ($show_enabled === '1') {
    echo '<div class="extra-details"><h3>Exhibitor Lineups</h3></div>';
}
echo '<div class="day exhibitors">';
$exhibitor_image = rwmb_meta( 'ibe_exhibitor_image', array(), $show_id );
$exhibitor_headline = rwmb_meta( 'ibe_exhibitor_location', array(), $show_id );
$exhibitor_blurb = rwmb_meta( 'ibe_exhibitors_blurb', array(), $show_id );
$exhibitor_link = rwmb_meta( 'ibe_exhibitors_url', array(), $show_id );

echo '<div class="day-header">';
if ($exhibitors_enabled === '1') {
    reset($exhibitor_image);
    $first_key = key($exhibitor_image);
    if (  isset($exhibitor_image) ) {
        $exhibitor_image = wp_get_attachment_image_src($first_key, 'large' )[0];
        echo '<img class="day-image" src="'.$exhibitor_image.'" />';
    }
} else {
    reset($exhibitors_placeholder_image);
    $first_key = key($exhibitors_placeholder_image);
    if (  isset($exhibitors_placeholder_image) ) {
        $exhibitors_placeholder_image = wp_get_attachment_image_src($first_key, 'large' )[0];
        echo '<img class="day-image" src="'.$exhibitors_placeholder_image.'" />';
    }
}

echo '<div class="detail-wrapper">';
    if ($exhibitors_enabled === '1') {
        if ( isset($exhibitor_headline) ) {
            echo '<div class="location-name">'.$exhibitor_headline.'</div>';
        }
        if ( isset($exhibitor_blurb) ) {
            echo '<article class="description detail">';
            echo    $exhibitor_blurb;
            echo '</article>';
        }
    }
    echo '<div class="button-wrapper">';
    if ($exhibitors_enabled === '1') {
        if ( isset($exhibitor_link) ) {
            echo '<a class="button exhibitor-link" href="'.$exhibitor_link.'">View Lineup</a>';
        }
    } else {
        echo '<a class="button coming-soon" href="#">Coming Soon</a>';
    }

    echo '</div>';
echo '</div>';

echo '</div>';
echo '</div>';
// End Exhibitors
