<?php

        $sponsorTitle = rwmb_meta( 'ibe_sponsor_title' );
        $sponsorSections = rwmb_meta( 'ibe_sponsor_sections' );



        echo '<section class="sponsor">';
        echo    '<h2>'.$sponsorTitle.'</h2>';
        echo    '<ul>';

            $sponsorCount = 0;
            foreach ($sponsorSections as &$section) {
                if ( isset($section['ibe_section_image']) ) {
                    $sectionBackground = wp_get_attachment_image_src($section['ibe_section_image'][0], 'large' );
                    $sectionBackground = $sectionBackground[0];
                } else {
                    $sectionBackground = null;
                }
                $sponsorCount++;

                echo '<li>';
                if ( $sectionBackground !== '' && $sectionBackground !== NULL ) {
                    $sectionBackground = ' style="background-image: url('.$sectionBackground.')"';
                } else {
                    $sectionBackground = '';
                }
                echo '<div class="section-image"'.$sectionBackground.'></div>';
                echo '<div class="section-info">';
                    if ( isset($section['ibe_section_title']) ) {
                        echo '<h3>'.$section['ibe_section_title'].'</h3>';
                    }
                    if ( isset($section['ibe_section_attends']) ) {
                        echo '<h4>Who Attends</h4>';
                        echo '<p>'.$section['ibe_section_attends'].'</p>';
                    }
                    if ( isset($section['ibe_section_exhibits']) ) {
                        echo '<h4>Who Exhibits</h4>';
                        echo '<p>'.$section['ibe_section_exhibits'].'</p>';
                    }
                    if ( isset($section['ibe_section_video_link']) ) {
                        echo '<a class="button" href="'.$section['ibe_section_video_link'].'" target="_blank">Watch '.$section['ibe_section_title'].' recap</a>';
                    }
                echo '</div>';
                echo '</li>';
            }

        echo    '</ul>';
        echo '</section>';


?>
