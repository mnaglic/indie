<?php

// ////////////////////////////////////////////////////////////////////////////
// Featured Content
// ////////////////////////////////////////////////////////////////////////////


$featured_items = rwmb_meta( 'ibe_home_featured_items' );
$feature_count = 0;
echo '<section class="featured-items">';
//var_dump($featured_items);
if ( isset($featured_items) && $featured_items !== '' ) {
    foreach ($featured_items as &$featured_item) {
        switch ($featured_item['ibe_home_featured_item_type']) {
            case 'page':
                $item_id = $featured_item['ibe_home_featured_item_page'];
                $featured_post = new WP_Query( "post_type=page&p=$item_id" );
                $postdata = $featured_post->post;
                $item_title = str_replace(' | ', '<br />', $postdata->post_title);
                $item_link = get_permalink( $item_id );
                if ($postdata->post_excerpt !== '') {
                    $item_text = get_the_excerpt( $item_id );
                } else {
                    $item_text = wp_trim_words( $postdata->post_content, 25, '...' );
                    $item_text_xl = wp_trim_words( $postdata->post_content, 54, '...' );
                }
                $page_id = $featured_item["ibe_home_featured_item_page"];
                if ( has_post_thumbnail( $page_id ) ) {
                    $image_array_lg = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'large' );
                    $image_large = $image_array_lg[0];
                    $image_array_rt = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'retina' );
                    $image_retina = $image_array_rt[0];
                    $has_image = true;
                } else {
                    $has_image = null;
                }
                break;
            case 'news':
                $item_id = $featured_item['ibe_home_featured_item_news'];
                $featured_post = new WP_Query( "post_type=post&p=$item_id" );
                $news_data = $featured_post->post;
                $item_title = str_replace(' | ', '<br />', $news_data->post_title);
                $item_link = get_permalink( $item_id );
                if ($news_data->post_excerpt !== '') {
                    $item_text = get_the_excerpt( $item_id );
                } else {
                    $item_text = wp_trim_words( $news_data->post_content, 25, '...' );
                    $item_text_xl = wp_trim_words( $news_data->post_content, 54, '...' );
                }
                $page_id = $featured_item["ibe_home_featured_item_news"];
                if ( has_post_thumbnail( $page_id ) ) {
                    $image_array_lg = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'large' );
                    $image_large = $image_array_lg[0];
                    $image_array_rt = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'retina' );
                    $image_retina = $image_array_rt[0];
                    $has_image = true;
                } else {
                    $has_image = null;
                }
                break;
            case 'article':
                $item_id = $featured_item['ibe_home_featured_item_article'];
                $featured_post = new WP_Query( "post_type=article&p=$item_id" );
                $postdata = $featured_post->post;
                $item_title = str_replace(' | ', '<br />', $postdata->post_title);
                $item_link = get_permalink( $item_id );
                if ($postdata->post_excerpt !== '') {
                    $item_text = get_the_excerpt( $item_id );
                } else {
                    $item_text = wp_trim_words( $news_data->post_content, 25, '...' );
                    $item_text_xl = wp_trim_words( $news_data->post_content, 54, '...' );
                }
                $page_id = $featured_item["ibe_home_featured_item_article"];
                if ( has_post_thumbnail( $page_id ) ) {
                    $image_array_lg = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'large' );
                    $image_large = $image_array_lg[0];
                    $image_array_rt = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'retina' );
                    $image_retina = $image_array_rt[0];
                    $has_image = true;
                } else {
                    $has_image = null;
                }
                break;
            case 'custom':
                $item_title = $featured_item['ibe_home_featured_item_custom_title'];
                $item_link = $featured_item['ibe_home_featured_item_custom_url'];
                //$item_text = wp_trim_words( esc_attr( $featured_item['ibe_home_featured_item_custom_summary'] ), 25, '...' );
                //$item_text_xl = wp_trim_words( esc_attr( $featured_item['ibe_home_featured_item_custom_summary'] ), 54, '...' );
                $item_text =  $featured_item['ibe_home_featured_item_custom_summary'];
                $item_text_xl = $item_text;
                $image_id = $featured_item['ibe_home_featured_item_custom_image'];
                $image_array_lg = wp_get_attachment_image_src($image_id[0], 'large' );
                $image_large = $image_array_lg[0];
                $image_array_rt = wp_get_attachment_image_src($image_id[0], 'retina' );
                $image_retina = $image_array_rt[0];
                $has_image = true;
                break;
            default:
                echo 'Error accessing featured item data';
        }
        if ( isset($featured_item['ibe_home_featured_item_bgpos']) ) {
            $bgpos = $featured_item['ibe_home_featured_item_bgpos'];
            $position = ' background-position: '. $bgpos;
        } else {
            $position = '';
        }
        if ( $item_title !== '' ) {
            if ( $featured_item['ibe_home_featured_item_style'] === 'spotlight' ) {
                $class = 'spotlight-item';
                $item_text = $item_text_xl;
                //$width_override = 'padding-left: 0; padding-right: 0;';
                $width_override = '';
            } else {
                $feature_count++;
                if ($feature_count % 2 == 0) {
                    $class = 'featured-item last';
                } else {
                    $class = 'featured-item';
                }
                $width_override = '';
            }

            //                $word_count = echo str_word_count($postdata->post_content);

            echo '<div class="'.$class.'">';
            echo '  <div class="photo">';
                echo '<a href="'.$item_link.'">';
                if ( $has_image !== null ) {
                    if (  $featured_item['ibe_home_featured_item_style'] === 'spotlight' ) {
                        echo '<div style="background-image: url('.$image_retina.');'.$position.'"></div>';
                    } else {
                        echo '<div style="background-image: url('.$image_large.');'.$position.'"></div>';
                    }
                } else {
                    echo '<div class="post-bg"></div>';
                }
                echo '  </a>';

            echo '    <h3><a class="ha" href="'.$item_link.'">'.$item_title.'</a></h3>';
            if ( $item_text !== '' ) {
                echo '    <article style="'.$width_override.'">'.$item_text.'</article>';
            }
            echo '  </div>';
            echo '</div>';
        }
    }
}
echo '</section>';

?>
