<?php

// Venue
if ($show_enabled === '1') {
    echo '<div class="extra-details"><h3>THE VENUE</h3></div>';
}
echo '<section class="details">';

echo '<div class="day venue full-width">';
$venue_image = rwmb_meta( 'ibe_venue_image', array(), $show_id );
$venue_image = rwmb_meta( 'ibe_venue_image_wide', array(), $show_id );
$venue_location = rwmb_meta( 'ibe_venue_location', array(), $show_id );
$venue_address1 = rwmb_meta( 'ibe_venue_address1', array(), $show_id );
$venue_address2 = rwmb_meta( 'ibe_venue_address2', array(), $show_id );
$venue_address3 = rwmb_meta( 'ibe_venue_address3', array(), $show_id );
$venue_map = rwmb_meta( 'ibe_venue_map', array(), $show_id );
$venue_website = rwmb_meta( 'ibe_venue_website', array(), $show_id );

echo '<div class="day-header">';
if ($venue_enabled === '1') {
    reset($venue_image);
    $first_key = key($venue_image);
    if (  isset($venue_image) ) {
        $venue_image = wp_get_attachment_image_src($first_key, 'large' )[0];
        echo '<img class="day-image" src="'.$venue_image.'" />';
    }
} else {
    reset($venue_placeholder_image_wide);
    $first_key = key($venue_placeholder_image_wide);
    if (  isset($venue_placeholder_image_wide) ) {
        $venue_placeholder_image_wide = wp_get_attachment_image_src($first_key, 'large' )[0];
        echo '<img class="day-image" src="'.$venue_placeholder_image_wide.'" />';
    }
}

echo '<div class="detail-wrapper">';
if ($venue_enabled === '1') {

    if ( isset($venue_location) ) {
        echo '<div class="location-name">'.$venue_location.'</div>';
    }
    echo '<article class="description detail">';
    if ( isset($venue_address1) ) {
        echo '<address class="address">'.$venue_address1.'</address>';
    }
    if ( isset($venue_address2) ) {
        echo '<address class="address">'.$venue_address2.'</address>';
    }
    if ( isset($venue_address3) ) {
        echo '<address class="address">'.$venue_address3.'</address>';
    }

    echo '</article>';
}
echo '<div class="button-wrapper">';
    if ($venue_enabled === '1') {
        if ( isset($venue_website) ) {
            echo '<a class="button venue" href="'.$venue_website.'" target="_blank">See Venue</a>';
        }
        if ( isset($venue_map) ) {
            echo '<a class="button venue" href="'.$venue_map.'" target="_blank">View Map</a>';
        }
    } else {
        echo '<a class="button coming-soon" href="#">Coming Soon</a>';
    }
echo '</div>';
echo '</div>';

echo '</div>';
echo '</div>';
// End Venue

echo '</section>';
