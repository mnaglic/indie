<section class="photobanner-container">
    <div class="photobanner">
        <?php
            $stream_photos = rwmb_meta( 'ibe_show_gallery_images', array(), $show->ID );
            if ( $stream_photos !== '' ) {
                $stream_photos = $stream_photos["ibe_show_gallery_image"];
                for( $i= 0 ; $i <= count($stream_photos) - 1 ; $i++ ) {
                    $gallery_image_id = $stream_photos[$i][0];
                    if ( isset($gallery_image_id) ) {
                        $gallery_image = wp_get_attachment_image_src($gallery_image_id, 'retina');
                        $gallery_image = $gallery_image[0];
                        echo '<div class="carousel-img" style="background-image: url('.$gallery_image.');"></div>';
                    }
                }
            }
        ?>
    </div>
</section>
<script>
/*
jQuery('.photobanner').slick({
  centerMode: true,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 2000,
  speed: 1000,
  slidesToShow: 4,
  centerMode: true,
  variableWidth: true
});
*/
    jQuery(document).ready( function() {
    	var $showGallery = jQuery('.photobanner').flickity({
            "wrapAround": true,
            "autoPlay": 4000,
            "prevNextButtons": false
        });
    });
</script>
