<?php

// ////////////////////////////////////////////////////////////////////////////
// Featured Content
// ////////////////////////////////////////////////////////////////////////////


$text_header = rwmb_meta( 'ibe_home_text_header' );

if ( $text_header['ibe_home_text_header_type'] !== 'none' ) {
    if ( $text_header['ibe_home_text_header_type'] === 'bi' ) {
        $text = $text_header['ibe_home_text_header_bi_text'];
        echo '<section class="text-header bi">';
        echo '  <div class="bi-logo"></div>';
        if ( $text !== '' ) {
            echo '  <article>'.$text.'</article>';
        }
        echo '</section>';
    } else {
        $text = $text_header['ibe_home_text_header_custom_text'];
        echo '<section class="text-header">';
        if ( $text !== '' ) {
            echo '  <article>'.$text.'</article>';
        }
        echo '</section>';
    }
}
?>
