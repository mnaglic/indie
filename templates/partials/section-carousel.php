<?php
    // ////////////////////////////////////////////////////////////////////////////
    // Carousel
    // ////////////////////////////////////////////////////////////////////////////

    $carousel_slides = rwmb_meta( 'ibe_page_carousel_images' );
    if ( isset($carousel_slides) ) {
        // Transition Type
        $transition_type = rwmb_meta( 'ibe_page_transition_type' );
        if ( !isset($transition_type) ) {
            $transition_type = '';
        }
        echo '<section class="carousel '.$transition_type.'">';
        foreach ($carousel_slides as &$carousel_slide) {
            //var_dump($carousel_slide);
            // Link Type
            if ( isset($carousel_slide['ibe_page_carousel_slide_link']) ) {
                if ( $carousel_slide['ibe_page_carousel_slide_link'] !== 'none' ) {
                    if ( isset($carousel_slide['ibe_page_carousel_link_page']) ) {
                        $slide_link = get_permalink($carousel_slide['ibe_page_carousel_link_page']);
                        $slide_link = 'href="'.$slide_link.'"';
                    } elseif ( isset($carousel_slide['ibe_page_carousel_link_news']) ) {
                        $slide_link = get_permalink($carousel_slide['ibe_page_carousel_link_news']);
                        $slide_link = 'href="'.$slide_link.'"';
                    } elseif ( isset($carousel_slide['ibe_page_carousel_link_article']) ) {
                        $slide_link = get_permalink($carousel_slide['ibe_page_carousel_link_article']);
                        $slide_link = 'href="'.$slide_link.'"';
                    } elseif ( isset($carousel_slide['ibe_page_carousel_link_deck']) ) {
                        $slide_link = get_permalink($carousel_slide['ibe_page_carousel_link_deck']);
                        $slide_link = 'href="'.$slide_link.'"';
                    } elseif ( isset($carousel_slide['ibe_page_carousel_link_url']) ) {
                        $slide_link = $carousel_slide['ibe_page_carousel_link_url'];
                        $slide_link = 'href="'.$slide_link.'"';
                    }
                } else {
                    $slide_link = '';
                }
            }
            // Image - Mobile
            if ( isset($carousel_slide['ibe_page_carousel_img_mobile']) ) {
                $img_sm_id = $carousel_slide['ibe_page_carousel_img_mobile'];
                $slideImage_sm = wp_get_attachment_image_src($img_sm_id[0], 'retina' );
            } else {
                $slideImage_sm = null;
            }
            // Image - Desktop
            if ( isset($carousel_slide['ibe_page_carousel_img_desktop']) ) {
                $img_lg_id = $carousel_slide['ibe_page_carousel_img_desktop'];
                $slideImage_lg = wp_get_attachment_image_src($img_lg_id[0], 'retina' );
            } else {
                $slideImage_lg = null;
            }
            // Background Position
            if ( isset($carousel_slide['ibe_page_carousel_bgpos']) ) {
                $bgpos = $carousel_slide['ibe_page_carousel_bgpos'];
                if ( $bgpos !== null ) {
                    $position = ' background-position: '. $bgpos;
                }
            } else {
                $position = '';
            }

            // Quote
            if ( isset($carousel_slide['ibe_page_carousel_quote_text']) ) {
                $quote = $carousel_slide['ibe_page_carousel_quote_text'];
                if ( isset($carousel_slide['ibe_page_carousel_quote_source']) ) {
                    $source = $carousel_slide['ibe_page_carousel_quote_source'];
                }
                if ( isset($carousel_slide['ibe_page_quote_alignment']) ) {
                    $quote_alignment = $carousel_slide['ibe_page_quote_alignment'] . ' !important';
                } else {
                    $quote_alignment = '';
                }
                if ( isset($carousel_slide['ibe_page_source_alignment']) ) {
                    $source_alignment = $carousel_slide['ibe_page_source_alignment'] . ' !important';
                } else {
                    $source_alignment = '';
                }
                if ( isset($carousel_slide['ibe_page_quote_textcolor']) ) {
                    $textcolor = $carousel_slide['ibe_page_quote_textcolor'];
                } else {
                    $textcolor = 'color__white';
                }
            } else {
                $quote = null;
            }

            // height Class
            if ( isset($carousel_slide['ibe_page_slide_type']) ) {
                $height_class = ' fixed-height';
                $cell_height = '';
            }

            if ( isset ($carousel_slide)) {
                echo '<div class="carousel-cell'.$height_class.'">';
                if (  isset($slideImage_sm) ) {
                    echo '<div class="carousel-image sm" style="background-image: url('.$slideImage_sm[0].');'.$position.'"></div>';
                }
                if ( isset($slideImage_lg) ) {
                    echo '<div class="carousel-image lg" style="background-image: url('.$slideImage_lg[0].');'.$position.'"></div>';
                }
                echo '  <a>';

                if (isset($carousel_slide['ibe_page_carousel_content'])) {
                    echo '<article>';
                    echo    $carousel_slide['ibe_page_carousel_content'];
                    echo '</article>';
                }
                if (isset($quote)) {
                    echo '<article class="quote">';
                    echo '  <main class="'.$textcolor.'" style="text-align: '.$quote_alignment.'">&ldquo;'.$quote.'&rdquo;</main>';
                    if (isset($source)) {
                        echo '  <aside class="'.$textcolor.'" style="text-align: '.$source_alignment.'">&mdash;'.$source.'</aside>';
                    }
                    echo '</article>';
                }
                echo '  </a>';
                echo '</div>';
            }
        }
        echo '</section>'."\r\n";
    }

?>
