<?php

    $show_id = rwmb_meta( 'ibe_el_show' );
    $show_override_image = rwmb_meta( 'ibe_el_header_image');
    $show_code = rwmb_meta( 'ibe_el_shortcode' );
    $show_intro_text = rwmb_meta( 'ibe_el_intro_text' );
    $show_footer_text = rwmb_meta( 'ibe_el_footer_text' );
    $show_map_image = rwmb_meta( 'ibe_el_map_image');
    $show_map_pdf = rwmb_meta( 'ibe_el_map_pdf');


    $show_city = rwmb_meta( 'ibe_show_details_city', array(), $show_id );
    $show_dates = rwmb_meta( 'ibe_show_details_dates', array(), $show_id );
    $show_type = rwmb_meta( 'ibe_show_header_type', array(), $show_id );

    $show_city_abbreviation = rwmb_meta( 'ibe_show_details_city_abbreviation', array(), $show_id );
    $show_year = rwmb_meta( 'ibe_show_details_dates', array(), $show_id );
    $show_year = explode(', ',$show_year);
    $show_year = end($show_year);

    if (!empty($show_intro_text)) {
        echo '<section class="exhibitor-list-intro">';
        echo    $show_intro_text;
        echo '</section>';
    }

    if (!empty($show_map_image)) {
        echo '<section class="exhibitor-list-map">';
        echo '<h2 class="el-section-header">IBE '.$show_city_abbreviation.' '.$show_year.' Exhibitor Map</h2>';


        if ( isset( $show_map_pdf ) ) {
            reset($show_map_pdf);
            $first_key = key($show_map_pdf);
            $show_map_pdf = $show_map_pdf[$first_key];

            echo '<div class="button-wrapper" style="padding-bottom: 0;">';
            echo '  <a class="button" href="'.$show_map_pdf['path'].'" target="_blank">Download the Map</a>';
            echo '</div>';
        }

        reset($show_map_image);
        $show_image_id = key($show_map_image);
            $show_image = wp_get_attachment_image_src($show_image_id, 'retina');
        echo '<img class="exhibitor-map" src="'.$show_image[0].'");" />';

        echo '</section>';
    }

    echo '<section class="exhibitor-list-vendors">';
    echo '  <h2 class="el-section-header">Meet The Exhibitors</h2>';
    echo '  <div class="vendor-wrapper">';
    echo        do_shortcode( $show_code );
    echo '  </div>';
    echo '</section>';

    if (!empty($show_footer_text)) {
        echo '<section class="exhibitor-list-footer">';
        echo    $show_footer_text;
        echo '</section>';
    }

?>
