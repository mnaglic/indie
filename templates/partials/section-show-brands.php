<?php
// Exhibitors

    echo '<section class="exhibitors">';
    $exhibitor_image = rwmb_meta( 'ibe_exhibitor_image', array(), $show->ID );
    $exhibitor_headline = rwmb_meta( 'ibe_exhibitor_location', array(), $show->ID );
    $exhibitor_blurb = rwmb_meta( 'ibe_exhibitors_blurb', array(), $show->ID );
    $exhibitor_link = rwmb_meta( 'ibe_exhibitors_url', array(), $show->ID );
    if ( $exhibitor_link !== '') {
        echo '<div class="brands-wrapper">';
        echo '<h4 class="brands-title">The Brands</h4>';
                echo '<article class="description detail">';
                echo 'Beauty Tools, Color Cosmetics, Dental/Oral Care, Fragrace, Hair Care, Ingestible/Nutrition, Nail Care, Skin Care & More!';
                echo '</article>';
            echo '<div class="button-wrapper">';
            echo '<a class="button exhibitor-link" href="'.$exhibitor_link.'">View the list of brands that exhibited</a>';
            echo '</div>';
        echo '</div>';
    }
echo '</section>';

?>
