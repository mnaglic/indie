<?php
    // Headline
    $showlist_headline = rwmb_meta( 'ibe_showlist_headline' );
    echo '<script src="https://player.vimeo.com/api/player.js"></script>';
    echo '<section class="show-list">';
    echo '<div class="header-wrapper">';

    echo '<div class="shows-image-container">';
    $show_id = rwmb_meta( 'ibe_el_show' );
    $show_override_image = rwmb_meta( 'ibe_el_header_image');

    $show_city = rwmb_meta( 'ibe_show_details_city', array(), $show_id );
    $show_dates = rwmb_meta( 'ibe_show_details_dates', array(), $show_id );
    $show_type = rwmb_meta( 'ibe_show_header_type', array(), $show_id );

    echo '<div class="show ' . $show_type . '" data-id="'.$show_id.'">';

    if (!empty($show_override_image)) {
        reset($show_override_image);
        $show_image_id = key($show_override_image);
            $show_image = wp_get_attachment_image_src($show_image_id, 'retina');
        echo '<div class="show-background" data-id="'.$show_id.'" style="background-image: url('.$show_image[0].');"></div>';
    } else {
        if ($show_type == 'image') {
            $show_image_id = rwmb_meta( 'ibe_show_header_image', array(), $show_id );
            $show_image_id = $show_image_id['ibe_show_background'][0];
            if ( isset($show_image_id) ) {
                $show_image = wp_get_attachment_image_src($show_image_id, 'retina');
            } else {
                $show_image = '';
            }
            echo '<div class="show-background" data-id="'.$show_id.'" style="background-image: url('.$show_image[0].');"></div>';

        } elseif ($show_type == 'vimeo') {
            $show_video = rwmb_meta( 'ibe_show_header_video', array(), $show_id );
            $show_oembed = $show_video['ibe_show_header_vimeo_oembed'];
            $show_video_id = explode('vimeo.com/', $show_oembed);
            $show_video_id = $show_video_id[1];
            $show_image_id = $show_video['ibe_show_header_vimeo_mobile'][0];
            if ( isset($show_image_id) ) {
                $show_image = wp_get_attachment_image_src($show_image_id, 'retina');
                echo '<div class="show-background fallback" data-id="'.$show_id.'" style="background-image: url('.$show_image[0].');"></div>';
            }
            echo '<div class="video">';
            //echo '<iframe id="vimeo-embed-'.$show_id.'" width="100%" height="100%" src="https://player.vimeo.com/video/'.$show_video_id.'?autoplay=1&loop=1&title=0&byline=0&portrait=0&muted=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            echo '<iframe id="vimeo-player-'.$show_id.'" frameborder="0" width="640" height="360" src="https://player.vimeo.com/video/'.$show_video_id.'?title=0&amp;background=1&amp;quality=720p&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;loop=1&amp;dnt=1"></iframe>';
            echo '</div>';
        } else {
            echo '<div class="show-header-overlay"></div>';
        }
    }



    echo'<div class="heading-wrapper">';
        echo '<h1><span class="show-city">Exhibitor Lineup</span></h1>';
        if (isset($show_city)) {
            echo '<h3 class="show-city">'.$show_city.'</h3>';
        }
        if (isset($show_dates)) {
            echo '<h3 class="show-dates">'.$show_dates.'</h3>';
        }
    echo '</div>';

    echo '</div>';
    if ($show_type == 'vimeo') {
        echo '<script>
        var iframe = jQuery("#vimeo-player-'.$show_id.'");
        var player_'.$show_id.' = new Vimeo.Player(iframe);

        player_'.$show_id.'.on("play", function() {
            console.log("played the video!");
        });

        player_'.$show_id.'.getVideoTitle().then(function(title) {
            console.log("title:", title);
        });

        player_'.$show_id.'.play().then(function() {
            console.log("played: player_'.$show_id.'");
        });
        </script>';
    }

    echo '</div>'; //.shows-image-container
    echo '</div>';
    echo '</section>';
?>
