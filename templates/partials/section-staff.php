<?php
$staff = rwmb_meta( 'ibe_staff_member' );

echo '<section class="staff">';
echo '<h3>MEET THE FOUNDERS</h3>';
echo '      <ul class="bios">';
$member_id = 1;
foreach ( $staff as &$member ) {
    if ( isset($member['ibe_staff_member_photo']) && $member['ibe_staff_member_photo'] !== '' ) {
        $img_id = $member['ibe_staff_member_photo'];
        $image_array_lg = wp_get_attachment_image_src( $img_id[0], 'large' );
        $photo = $image_array_lg[0];
    } else {
        $photo = null;
    }

    echo '<li class="founder" id="staff-'.$member_id.'">';
    if ( isset($member['ibe_staff_member_name']) && $member['ibe_staff_member_name'] !== '' ) {
        if ( $photo !== null ) {
            echo    '<div class="photo" style="background-image: url('.$photo.');"></div>';
        } else {
            echo    '<div class="photo post-bg"></div>';
        }
        echo    '<h4>'.$member['ibe_staff_member_name'].'</h4>';
        echo    '<p>'.$member['ibe_staff_member_bio'].'</p>';
    }
    echo '</li>';
    $member_id++;
}
echo '      </ul>';



echo '</section>'
?>
