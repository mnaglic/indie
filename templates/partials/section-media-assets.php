<?php
/*
    echo '<section class="media-assets">';
    $mediaAssetsTitle = rwmb_meta( 'ibe_media_assets_title' );
    $mediaAssets = rwmb_meta( 'ibe_media_assets' );
    echo '<h2>'.$mediaAssetsTitle.'</h2>';

    foreach ($mediaAssets as &$asset) {
        if ( isset($asset['ibe_media_asset_background']) ) {
            $assetBackground = wp_get_attachment_image_src($asset['ibe_media_asset_background'][0], 'large' );
            $assetBackground = $assetBackground[0];

        } else {
            $assetBackground = null;
        }
        if ( $asset['ibe_media_asset_title'] !== '' ) {
            echo '<div class="media-asset">';
            echo '  <div class="image">';
                echo '<a href="'.$asset['ibe_media_asset_link_deck'].'">';
                if ( $assetBackground !== null ) {
                        echo '<div style="background-image: url('.$assetBackground.');"><span>'.$asset['ibe_media_asset_title'].'</span></div>';
                } else {
                    echo '<div class="post-bg"><span>'.$asset['ibe_media_asset_title'].'</span></div>';
                }
                echo '  </a>';

            echo '    <h3><a href="'.$asset['ibe_media_asset_link_deck'].'" class="ha">'.$asset['ibe_media_asset_tagline'].'</a></h3>';
            echo '    <p><a href="'.$asset['ibe_media_asset_link_deck'].'" class="ha">'.$asset['ibe_media_asset_description'].'</a></p>';

            echo '  </div>';
            echo '</div>';
        }
    }
    echo '</section>';
*/
$mediaAssetsTitle = rwmb_meta( 'ibe_media_assets_title' );
$mediaAssetsLayout = rwmb_meta( 'ibe_media_layout' );
$mediaAssets = rwmb_meta( 'ibe_media_assets' );
    echo '<section class="media '.$mediaAssetsLayout.'">';

        echo '<h2>'.$mediaAssetsTitle.'</h2>';

        $mediaTitleCount = 0;
        foreach ($mediaAssets as &$asset) {
            if ( isset($asset['ibe_media_asset_background']) ) {
                $assetBackground = wp_get_attachment_image_src($asset['ibe_media_asset_background'][0], 'large' );
                $assetBackground = $assetBackground[0];

            } else {
                $assetBackground = null;
            }

            $mediaTitleCount++;
            echo '<div class="media-object">';
            if ( $assetBackground !== null ) {
                echo '<div class="image post-bg" style="background-image: url('.$assetBackground.');">';
            } else {
                echo '<div class="image post-bg">';
            }
            if ( isset($asset['ibe_media_asset_link_deck']) ) {
                echo '<a href="'.get_permalink( $asset['ibe_media_asset_link_deck'] ).'" class="download">';
            }
            if ( isset($asset['ibe_media_asset_title']) ) {
                echo '<span>'.$asset['ibe_media_asset_title'].'</span>';
            }
            if ( isset($asset['ibe_media_asset_link_deck']) ) {
                echo '</a>';
            }
            echo '</div>';
            echo '<section class="media-header">';
            if ( isset($asset['ibe_media_asset_tagline']) ) {
                echo '<h3>'.$asset['ibe_media_asset_tagline'].'</h3>';
            }
            if ( isset($asset['ibe_media_asset_description']) ) {
                echo '<article>'.$asset['ibe_media_asset_description'].'</article>';
            }
            echo '</section>';
            echo '</div>';
        }
    echo '</section>';

?>
