<?php


    if (isset($_POST['persist']) && $_POST['persist'] === 'true') {
        $hide_class = '';
    } else {
        $hide_class = 'invisible';
    }
    // Register for comments
    echo '<div class="modal-popup subscribe-form '.$hide_class.'">';
    echo '  <div class="modal-bg"></div>';
    echo '  <div class="modal-popup-content">';
    echo '      <span class="remove">&times;</span>';
    echo '          <div class="inner">';
    register_subscriber();
    echo '          </div>';
    echo '  </div>';
    echo '</div>';

    // Register for newsletter
    echo '<div class="modal-popup newsletter-form '.$hide_class.'">';
    echo '  <div class="modal-bg"></div>';
    echo '  <div class="modal-popup-content">';
    echo '      <span class="remove">&times;</span>';
    echo '          <div class="inner">';
    echo '
                        <div id="mc_embed_signup">
                            <form action="https://Indiebeautyexpo.us9.list-manage.com/subscribe/post-json?u=f4a3d85a1388923b3fe3a6cb9&amp;id=4ed1cffcc8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll">
                                <p style="color: black; text-transform: uppercase; text-align: center;">Join our newsletter &amp; receive exclusive event invites, brand stories + exciting news about all things Indie Beauty.</p>
                                <hr />
                                    <div class="mc-field-group">
                                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
                                    </div>
                                    <div class="mc-field-group input-group">
                                        <strong>How would you describe yourself?</strong>
                                        <ul>
                                            <li><input type="radio" value="1" name="group[28229]" id="mce-group[28229]-28229-0"><label for="mce-group[28229]-28229-0">Brand Founder/Entrepreneur</label></li>
                                            <li><input type="radio" value="2" name="group[28229]" id="mce-group[28229]-28229-1"><label for="mce-group[28229]-28229-1">Buyer/Retailer</label></li>
                                            <li><input type="radio" value="4" name="group[28229]" id="mce-group[28229]-28229-2"><label for="mce-group[28229]-28229-2">Press</label></li>
                                            <li><input type="radio" value="8" name="group[28229]" id="mce-group[28229]-28229-3"><label for="mce-group[28229]-28229-3">A Beauty Lover</label></li>
                                            <li><input type="radio" value="34359738368" name="group[28229]" id="mce-group[28229]-28229-4"><label for="mce-group[28229]-28229-4">Beauty Industry Professional</label></li>
                                            <li><input type="radio" value="68719476736" name="group[28229]" id="mce-group[28229]-28229-5"><label for="mce-group[28229]-28229-5">Other Trade Professional</label></li>
                                        </ul>
                                    </div>
                                    <div class="mc-field-group input-group" style="display:none;">
                                        <ul>
                                            <li><input type="radio" value="html" name="EMAILTYPE" id="mce-EMAILTYPE-0" checked="checked"><label for="mce-EMAILTYPE-0">html</label></li>
                                            <li><input type="radio" value="text" name="EMAILTYPE" id="mce-EMAILTYPE-1"><label for="mce-EMAILTYPE-1">text</label></li>
                                        </ul>
                                    </div>
                                    <div id="mce-responses" class="clear">
                                        <div class="response" id="mce-error-response" style="display:none"></div>
                                        <div class="response" id="mce-success-response" style="display:none"></div>
                                    </div>
                                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_f4a3d85a1388923b3fe3a6cb9_4ed1cffcc8" tabindex="-1" value=""></div>
                                    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-newsletter" class="button"></div>
                                </div>
                            </form>
                        </div>
	';
    echo '          </div>';
    echo '  </div>';
    echo '</div>';

?>
