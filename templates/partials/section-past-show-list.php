<?php
    echo '<section class="show-list">';
    $attend_page = get_page_by_title( 'Attend' );

    $live_shows = rwmb_meta( 'ibe_showlist_selection', array(), $attend_page->ID );
    $all_show_ids = array();
    $live_show_ids = array();
    $archived_shows = $all_shows;

    $count = 0;
    foreach ($all_shows as &$show) {
        //array_push($all_show_ids, $show->ID);
        $key = current($show);
        foreach ($live_shows as &$live_show) {
            if ($show->ID === intval($live_show['ibe_show_item']) ) {
                unset($archived_shows[$count]);
            }
        }
        $count++;
    }

    foreach ($archived_shows as &$show) {
        echo '<div id="show-'.$show->ID.'" class="show">';
        $show_header_type = rwmb_meta( 'ibe_show_header_type', array(), $show->ID );

                if ( $show_header_type === 'vimeo' ) {
                    $show_video = rwmb_meta( 'ibe_show_header_video', array(), $show->ID );
                    $show_image_id = $show_video['ibe_show_header_vimeo_mobile'][0];
                    $show_image = wp_get_attachment_image_src($show_image_id, 'retina');
                    $show_image = $show_image[0];
                } elseif ( $show_header_type === 'image' ) {
                    $show_image_id = rwmb_meta( 'ibe_show_header_image', array(), $show->ID );
                    $show_image_id = $show_image_id['ibe_show_background'][0];
                    if ( isset($show_image_id) ) {
                        $show_image = wp_get_attachment_image_src($show_image_id, 'retina');
                        $show_image = $show_image[0];
                    } else {
                        $show_image = null;
                    }
                } else {
                    $show_image = null;
                }

                if ( $show_image ) {
                    echo '<div class="show-image" data-id="'.$show->ID.'" style="background-image: url('.$show_image.');">';
                    echo '  <img src="' . get_stylesheet_directory_uri() . '/images/past-shows-ar.png"  />';
                    echo '</div>';
                } else {
                    echo '<div class="show-image placeholder" data-id="'.$show->ID.'">';
                    echo '  <img src="' . get_stylesheet_directory_uri() . '/images/past-shows-ar.png"  />';
                    echo '</div>';
                }
                echo '<div class="detail-wrapper">';
                    if ( isset($show->post_title) ) {
                        echo '<h4 class="location-name">IBE '.$show->post_title.'</h4>';
                    }
                    echo '<article class="description detail">';
                    if ( rwmb_meta( 'ibe_show_archive_blurb', array(), $show->ID ) !== null && rwmb_meta( 'ibe_show_archive_blurb', array(), $show->ID ) !== '' ) {
                        echo '<div class="description">'.rwmb_meta( 'ibe_show_archive_blurb', array(), $show->ID ).'</div>';
                    }
                    echo '</article>';

                    echo '<div class="button-wrapper">';
                    echo '  <a class="button venue" href="'.$show->guid.'">View Show</a>';
                    echo '</div>';
                echo '</div>';
    echo '</div>';
    }
    echo '</section>';
?>
