<?php
    // Headline
    $past_shows = get_page_by_title( 'Past Shows' );
    $header_background = rwmb_meta( 'ibe_past_shows_header_image', 'size=retina', $past_shows->ID );

    if ( isset($header_background) ) {
        reset($header_background);
        $first_key = key($header_background);
        $header_background = wp_get_attachment_image_src($first_key, 'retina');
        $header_background = 'style="background-image: url('.$header_background[0].');"';
    } else {
        $header_background = '';
    }

    echo '<header class="title" '.$header_background.'>';
    echo '  <h1>Past Shows</h1>';
    echo '</header>';
?>
