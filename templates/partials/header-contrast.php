<?php
    // Header Contrast
    $headers = rwmb_meta( 'bs_header_contrast' );
    foreach ($headers as &$header) {
        if ( $header['bs_header_contrast_title'] !== '' ) {
            echo '    <h3>';
            echo        $header['bs_header_contrast_title'];
            echo '    </h3>';
        }
    }
?>
