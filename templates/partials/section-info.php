<?php
$headline = rwmb_meta( 'ibe_page_headline' );
$description = rwmb_meta( 'ibe_page_paragraph' );
$mission = rwmb_meta( 'ibe_page_mission' );
    echo '<section class="info">';
    echo '  <h3>';
    echo        $headline;
    echo '  </h3>';
    echo '<article>';
    echo    $description;
    echo '</article>';
    echo '<aside>';
    echo    '<h3>'.$mission.'</h3>';
    echo '</aside>';
    echo '</section>';
?>
