<?php
    // Section - 3 Column
    $sections = rwmb_meta( 'bs_section_3col' );

    foreach ($sections as &$section) {
        if ( $section['bs_section_3col_title'] !== '' ) {
            if ( $section['bs_section_3col_highlighted'] === 'alternate' ) {
                echo '<section class="highlight">';
            } else {
                echo '<section>';
            }
            echo '  <div class="subhead">';
            echo '    <h2>';
            echo        $section['bs_section_3col_title'];
            echo '    </h2>';
            echo '  </div>';
            if ( $section['bs_section_3col_copy'] !== '' ) {
                echo '    <article>';
                echo        $section['bs_section_3col_copy'];
                echo '    </article>';
            }
            echo '</section>';
        }
    }
?>
