<?php
    // Headline
    $past_shows_enabled = rwmb_meta( 'ibe_pastshows_enabled' );
    $past_shows_headline = rwmb_meta( 'ibe_attend_past_headline' );
    $past_shows_background = rwmb_meta( 'ibe_attend_past_background', 'size=retina' );

    if ( $past_shows_enabled ) {
        if ( isset($past_shows_background) ) {
            reset($past_shows_background);
            $first_key = key($past_shows_background);
            $past_shows_background = wp_get_attachment_image_src($first_key, 'retina');
            $past_shows_background = 'style="background-image: url('.$past_shows_background[0].');"';
        } else {
            $past_shows_background = '';
        }

        echo '<section class="past-shows" '.$past_shows_background.'>';
            echo '<div class="header-wrapper">';
            if ( isset($past_shows_headline) ) {
                echo '<h3>'.$past_shows_headline.'</h3>';
            }
                echo '<div class="button-wrapper">';
                echo '  <a class="button venue" href="/past-shows/">View Past Shows</a>';
                echo '</div>';
            echo '</div>';
        echo '</section>';
    }
?>
