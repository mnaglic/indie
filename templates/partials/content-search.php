<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package iBE_2016
 */

?>

<?php
if ( has_post_thumbnail( $post->ID ) ) {
	$image_array_lg = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
	$image_large = $image_array_lg[0];
	$image_array_rt = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'retina' );
	$image_retina = $image_array_rt[0];
	$has_image = true;
} else {
	$has_image = null;
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		if ( $has_image !== null ) {
			$imageClass = 'class="entry-header" style="background-image: url('.$image_large.');"';
		} else {
			$imageClass = 'class="entry-header post-bg"';
		}
	?>
	<header <?php echo $imageClass; ?>>
		<?php echo sprintf( '<a class="search-image" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>'; ?>
	</header>

	<div class="entry-summary">
		<?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
		<?php echo strip_tags(get_the_excerpt()); ?>
		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php ibe__posted_on(); ?>
			<?php ibe_news_entry_footer(); ?>
		</div>
		<?php endif; ?>
	</div>

</article><!-- #post-## -->
