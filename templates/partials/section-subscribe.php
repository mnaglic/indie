<?php



global $wp;
$current_url = home_url( add_query_arg( array(), $wp->request ) );


    if (isset($_POST['persist']) && $_POST['persist'] === 'true') {
        $hide_class = '';
    } else {
        $hide_class = 'invisible';
    }
    // Register for comments
    echo '<div class="modal-popup subscribe-form '.$hide_class.'">';
    echo '  <div class="modal-bg"></div>';
    echo '  <div class="modal-popup-content">';
    echo '      <span class="remove">&times;</span>';
    echo '          <div class="inner">';
    register_subscriber();
    echo '          </div>';
    echo '  </div>';
    echo '</div>';

    // Register for newsletter
    echo '<div class="modal-popup newsletter-form '.$hide_class.'">';
    echo '  <div class="modal-bg"></div>';
    echo '  <div class="modal-popup-content">';
    echo '      <span class="remove">&times;</span>';
    echo '          <div class="inner">';
    echo '
    <div id="mc_embed_signup">
    <form action="https://Indiebeautyexpo.us9.list-manage.com/subscribe/post-json?u=f4a3d85a1388923b3fe3a6cb9&amp;id=4ed1cffcc8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <p style="font-weight: 500;">We can&#39;t wait to share the latest updates, breaking news, exclusive offers, exhibitor sneak peeks and so much more.</p>
        <div class="mc-field-wrapper">
            <div class="mc-field-group">
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                <label for="mce-EMAIL">Email Address <span class="asterisk">*</span></label>
            </div>
            <div class="mc-field-group half">
                <input type="text" value="" name="MMERGE1" class="" id="mce-MMERGE1">
                <label for="mce-MMERGE1">First Name </label>

            </div>
            <div class="mc-field-group half">
            <input type="text" value="" name="MMERGE2" class="" id="mce-MMERGE2">
                <label for="mce-MMERGE2">Last Name </label>
            </div>
        </div>
        <div class="mc-field-group input-group">
            <p style="margin-top: 1.5rem;">Select a category that best describes you below, and we&#39;ll send you updates that are the most relevant to you.</p>
            <ul>
                <li><input type="radio" value="1" name="group[28229]" id="mce-group[28229]-28229-0"><label for="mce-group[28229]-28229-0">Brand Founder/Entrepreneur</label></li><br />
                <li><input type="radio" value="2" name="group[28229]" id="mce-group[28229]-28229-1"><label for="mce-group[28229]-28229-1">Buyer/Retailer</label></li><br />
                <li><input type="radio" value="4" name="group[28229]" id="mce-group[28229]-28229-2"><label for="mce-group[28229]-28229-2">Press/Influencer</label></li><br />
                <li><input type="radio" value="36028797018963968" name="group[28229]" id="mce-group[28229]-28229-3"><label for="mce-group[28229]-28229-3">Solution Provider</label></li><br />
                <li><input type="radio" value="4294967296" name="group[28229]" id="mce-group[28229]-28229-4"><label for="mce-group[28229]-28229-4">Other Trade Professional</label></li><br />
                <li><input type="radio" value="2147483648" name="group[28229]" id="mce-group[28229]-28229-5"><label for="mce-group[28229]-28229-5">Beauty Industry Professional</label></li><br />
                <li><input type="radio" value="8" name="group[28229]" id="mce-group[28229]-28229-6"><label for="mce-group[28229]-28229-6">A Beauty Lover</label></li>
            </ul>
        </div>
        <input type="hidden" value="'.$current_url.'" name="SOURCE_URL" class="" id="SOURCE_URL">
        <input type="hidden" value="Indie Beauty Expo" name="SOURCE_PRO" class="" id="SOURCE_PRO">        
        <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
        </div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_f4a3d85a1388923b3fe3a6cb9_4ed1cffcc8" tabindex="-1" value=""></div>
        <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </form><!--End mc_embed_signup-->
    </div>
	';
    echo '          </div>';
    echo '  </div>';
    echo '</div>';

?>
