<?php
$sidebar_access_cta         = rwmb_meta( 'ibe_attend_sidebar_access_cta','',359 );
$sidebar_exhibit_cta        = rwmb_meta( 'ibe_attend_sidebar_exhibit_cta','',359 );
$sidebar_exhibit_url        = rwmb_meta( 'ibe_attend_sidebar_exhibit_url','',359 );

$sidebar_newsfeed_headline  = rwmb_meta( 'ibe_attend_sidebar_newsfeed_headline','',359 );
$sidebar_newsletter_cta     = rwmb_meta( 'ibe_attend_sidebar_newsletter_cta','',359 );

$default_sidebar_show = rwmb_meta( 'ibe_default_show','',359 );
$sidebar_shows = rwmb_meta( 'ibe_showlist_selection','',359 );

echo '<div class="sidebar-main">';

    // Newsfeed
    echo '<div class="news-feeds">';
        foreach ($sidebar_shows as &$sidebar_show) {
            $sidebar_show_id = $sidebar_show['ibe_show_item'];
            $sidebar_show_tag = rwmb_meta( 'ibe_show_details_news_tag', array(), $sidebar_show_id );
            $sidebar_show_city = rwmb_meta( 'ibe_show_details_city', array(), $sidebar_show_id );

            if ( $sidebar_show_id === $default_sidebar_show ) {
                echo '<div id="feed-'.$sidebar_show_id.'" class="show-newsfeed is-selected" data-id="'.$sidebar_show_id.'">';
            }   else {
                echo '<div id="feed-'.$sidebar_show_id.'" class="show-newsfeed" data-id="'.$sidebar_show_id.'" style="display:none;">';
            }

            if ( isset($sidebar_newsfeed_headline) ) {
                echo '<h5 class="headline">';
                //echo $sidebar_newsfeed_headline . ' ' . $sidebar_show_city;
                echo $sidebar_show_city . ' News';
                echo '</h5>';
            }

            $sidebarloop = new WP_Query( 'post_type=post&posts_per_page=5&tag=' . $sidebar_show_tag );
            while ( $sidebarloop->have_posts() ) : $sidebarloop->the_post();
                //var_dump($post);
                echo '  <article id="post-'.get_the_ID().'">';
                echo '      <header class="entry-header">';
                if ( has_post_thumbnail( $post->ID ) ) {
                    $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
                    $style = 'background-image: url('.$src[0].')';
                } else {
                    $src = false;
                }
                if ($src) {
                    echo '          <a href="'.get_permalink($post->ID).'">';
                    echo '              <img class="featured-image post-bg" src="'.$src[0].'" />';
                    echo '          </a>';
                }
                echo '      </header>';
                echo '      <a class="article-title" href="'.get_permalink($post->ID).'"><h6 class="entry-title">'.str_replace(' | ', ' ', get_the_title()).'</h6></a>';
                echo '  </article>';

            endwhile;
            wp_reset_postdata();
            echo '<a class="view-more" href="/tag/'.$sidebar_show_tag.'">View More</a>';
            echo '</div>';
        }
    echo '</div>';

    // Newsletter
    echo '<div class="newsletter">';
        if ( isset($sidebar_newsletter_cta) ) {
            echo '<div class="newsletter-cta">';
            echo    $sidebar_newsletter_cta;
            echo '</div>';
        }
        echo '<form action="//Indiebeautyexpo.us9.list-manage.com/subscribe/post-json?u=f4a3d85a1388923b3fe3a6cb9&amp;id=4ed1cffcc8" method="post" name="mc-embedded-subscribe-form" class="mc-embedded-subscribe-form validate" novalidate>';
        echo '  <input type="email" placeholder="Enter email" name="EMAIL" class="required mc-embedded-email">';
        echo '  <input type="submit" value="Sign up" name="subscribe" class="button mc-embedded-subscribe">';
        echo '</form>';
    echo '</div>';
    ?></div>
    <?php $front_id = get_option('page_on_front');
    if($front_id == get_the_ID()):?>
    <div class="slick-wrapper">
        <div class="new-slider">
            <div class="slide">
                <p>
                    "The quality of the buyers, beauty editors, influencers and distributors was exceptional.
                    We came away with several new wholesale customers, a new distributor and several mentions in 
                    premier beauty press."
                </p>
                <h2>
                    Adesse New York
                </h2>
            </div>
            <div class="slide">
                <p>
                    "The quality of the buyers, beauty editors, influencers and distributors was exceptional.
                    We came away with several new wholesale customers, a new distributor and several mentions in 
                    premier beauty press."
                </p>
                <h2>
                    Adesse New York
                </h2>
            </div>
            <div class="slide">
                <p>
                    "The quality of the buyers, beauty editors, influencers and distributors was exceptional.
                    We came away with several new wholesale customers, a new distributor and several mentions in 
                    premier beauty press."
                </p>
                <h2>
                    Adesse New York
                </h2>
            </div>
        </div>
    </div>

    <?php
    endif;
//get_sidebar();