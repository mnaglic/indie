<?php
$testimonialsTitle = rwmb_meta( 'ibe_testimonials_title' );
$testimonialsLayout = rwmb_meta( 'ibe_testimonials_layout' );
$testimonials = rwmb_meta( 'ibe_testimonials' );
$count = 0;
    echo '<section class="media video '.$testimonialsLayout.'">';

        echo '<h2>'.$testimonialsTitle.'</h2>';

        foreach ($testimonials as &$testimonial) {
            $count++;
            $youtube_video = $testimonial['ibe_testimonial_youtube_oembed'];

            $pos = strpos($youtube_video,'embed/');
            if($pos === false) {
                $youtube_video = explode('://youtu.be/', $youtube_video);
                $youtube_video = $youtube_video[1];
            }
            else {
                $youtube_video = explode('embed/', $youtube_video);
                $youtube_video = explode('?', $youtube_video[1]);
                $youtube_video = $youtube_video[0];
            }

            echo '<div class="media-object">';
            if ( isset($youtube_video ) && $youtube_video !== 'Embed HTML not available.' ) {
                echo '<div class="video-container overlay-active">';
                if ( isset($testimonial['ibe_testimonial_title']) ) {
                    echo '<a class="video-overlay"><span class="active">'.$testimonial['ibe_testimonial_title'].'</span></a>';
                } else {
                    echo '<div class="video-overlay"></div>';
                }
                echo '<iframe id="frame'.$count.'" width="100%" height="100%" src="https://www.youtube.com/embed/'.$youtube_video.'?modestbranding=1&showinfo=0&autoplay=0&controls=1&playsinline=1&loop=0&playlist='.$youtube_video.'" frameborder="0" allowfullscreen=""></iframe>';
                echo '</div>';
            }
            echo '<section class="media-header">';
            if ( isset($testimonial['ibe_testimonial_tagline']) ) {
                echo '<h3>'.$testimonial['ibe_testimonial_tagline'].'</h3>';
            }
            if ( isset($testimonial['ibe_testimonial_description']) ) {
                echo '<article>'.$testimonial['ibe_testimonial_description'].'</article>';
            }
            echo '</section>';
            echo '</div>';
        }
    echo '</section>';
?>
