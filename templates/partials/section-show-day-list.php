<?php

    $preview_enabled = false;
    if (isset($_GET["preview"])) {
        if ($_GET["preview"] === "true") {
            $preview_enabled = true;
        }
    }

    echo '<section class="day-list">';
    $default_show = rwmb_meta( 'ibe_default_show' );
    $shows = rwmb_meta( 'ibe_showlist_selection' );

    // Check for Query string to override default show
    if (isset($_GET['sh'])) {
        $query = $_GET['sh'];
        foreach ($shows as &$show) {
            if ( is_numeric($query) ) {
                if ($query === $show['ibe_show_item']) {
                    $default_show = intval($query);
                    $default_show = strval($default_show);
                }
            }
        }
    }



    foreach ($shows as &$show) {
        $show_id = $show['ibe_show_item'];
        $show_enabled = rwmb_meta( 'ibe_show_details_enabled', array(), $show_id );
        $show_newlayout = rwmb_meta( 'ibe_show_newlayout_enabled', array(), $show_id );


        $attend = get_page_by_title( 'Attend' );
        $venue_enabled = rwmb_meta( 'ibe_venue_enabled', array(), $show_id );
        $venue_placeholder_image = rwmb_meta( 'ibe_venue_comingsoon_image', array(), $attend->ID );
        $venue_placeholder_image_wide = rwmb_meta( 'ibe_venue_comingsoon_image_wide', array(), $attend->ID );
        $exhibitors_enabled = rwmb_meta( 'ibe_exhibitors_enabled', array(), $show_id );
        $exhibitors_placeholder_image = rwmb_meta( 'ibe_exhibitor_comingsoon_image', array(), $attend->ID );

        $day1 = rwmb_meta( 'ibe_show_day_1', array(), $show_id );
        $day2 = rwmb_meta( 'ibe_show_day_2', array(), $show_id );
        $day3 = rwmb_meta( 'ibe_show_day_3', array(), $show_id );
        $days = array($day1, $day2, $day3);
        //$days = array($day2, $day3);
        if ( $show_id === $default_show ) {
            echo '<div id="show-'.$show_id.'" class="show is-selected">';
        }   else {
            echo '<div id="show-'.$show_id.'" class="show">';
        }

        $show_startdate = rwmb_meta( 'ibe_show_details_startdate', array(), $show_id );
        $show_startdate = date_create($show_startdate);
        $current_date = date('Y-m-d');
        $current_date = date_create($current_date);
        $diff = date_diff($show_startdate,$current_date);

        if ($diff->days != 0 ) {
            date_default_timezone_set('America/New_York');


            $show_hashtag = rwmb_meta( 'ibe_show_details_hashtag', array(), $show_id );
            if (!isset($show_hashtag) || $show_hashtag == '') {
                $show_city = rwmb_meta( 'ibe_show_details_city', array(), $show_id );
                $show_hashtag = 'IBE ' . $show_city;
            }

            $show_type = rwmb_meta( 'ibe_countdown_background_type', array(), $show_id );

            if ( $show_id === $default_show ) {
                $classes = 'show-countdown ' . $show_type;
            }   else {
                $classes = 'show-countdown ' . $show_type;
            }

            echo '<section class="'.$classes.'">';

            if ($show_type == 'image') {
                $show_image_id = rwmb_meta( 'ibe_countdown_background_image', array(), $show_id );
                $show_image_id = $show_image_id['ibe_show_background'][0];
                if ( isset($show_image_id) ) {
                    $show_image = wp_get_attachment_image_src($show_image_id, 'retina');
                } else {
                    $show_image = '';
                }
                echo '<div class="countdown-background" data-id="'.$show_id.'" style="background-image: url('.$show_image[0].');"></div>';

            } elseif ($show_type == 'vimeo') {
                $show_video = rwmb_meta( 'ibe_countdown_background_video', array(), $show_id );
                $show_oembed = $show_video['ibe_countdown_background_vimeo_oembed'];
                $show_video_id = explode('vimeo.com/', $show_oembed);
                $show_video_id = $show_video_id[1];
                $show_image_id = $show_video['ibe_countdown_background_vimeo_mobile'][0];
                if ( isset($show_image_id) ) {
                    $show_image = wp_get_attachment_image_src($show_image_id, 'retina');
                    echo '<div class="countdown-background fallback" data-id="'.$show_id.'" style="background-image: url('.$show_image[0].');"></div>';
                }
                echo '<div class="video">';
                //echo '<iframe id="vimeo-embed-'.$show_id.'" width="100%" height="100%" src="https://player.vimeo.com/video/'.$show_video_id.'?autoplay=1&loop=1&title=0&byline=0&portrait=0&muted=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                //echo '<iframe id="vimeo-player-'.$show_id.'" frameborder="0" width="640" height="360" muted="muted" src="https://player.vimeo.com/video/'.$show_video_id.'?title=0&amp;background=1&amp;quality=720p&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;loop=1&amp;dnt=1"></iframe>';
                echo '<iframe id="vimeo-player-'.$show_id.'" src="https://player.vimeo.com/video/'.$show_video_id.'?api=1&player_id=vimeo_player&autoplay=1&loop=1&muted=1" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="autoplay"></iframe>';
                echo '</div>';
            } else {
                echo '<div class="show-header-overlay"></div>';
            }

            if ($show_type == 'vimeo') {
                echo '<script>
                var iframe = jQuery("#vimeo-player-'.$show_id.'");
                var player_'.$show_id.' = new Vimeo.Player(iframe);

                player_'.$show_id.'.on("play", function() {
                    console.log("played the video!");
                });

                player_'.$show_id.'.getVideoTitle().then(function(title) {
                    console.log("title:", title);
                });

                player_'.$show_id.'.play().then(function() {
                    console.log("played: player_'.$show_id.'");
                });
                </script>';
            }

            echo '<span class="countdown-days">'.$diff->days.'</span>';
            echo '<span class="countdown-message">DAYS UNTIL '.$show_hashtag.'</span>';

            echo '</section>';
        }
        if ( ($show_enabled === '1' || $preview_enabled) && $show_newlayout ) {
            // show_banner_image
            $show_banner_id = rwmb_meta( 'ibe_show_banner', array(), $show_id );
            if ($show_banner_id) {
                $show_banner_id = $show_banner_id['ibe_show_banner_image'][0];
                if ( isset($show_banner_id) && $show_banner_id !== '' ) {
                    echo '<section class="show-banner">';
                        $show_banner_image = wp_get_attachment_image_src($show_banner_id, 'retina');
                        echo '<img class="show-banner-image" src="'.$show_banner_image[0].'" />';
                    echo '</section>';
                }
            }
        }


        echo '<div class="days">';

        $dayCount = 0;
        $blockCount = 0;
        $tbaCount = 0;
        foreach ($days as &$day) {
            if ($show_enabled === '1' || $preview_enabled) {
                $dayCount++;
                echo '<div class="day day-'.$dayCount.'">';
                $day_title = 'ibe_day_'.$dayCount.'_title';
                $day_date = 'ibe_day_'.$dayCount.'_date';
                $day_date2 = 'ibe_day_'.$dayCount.'_date2';
                $day_audience = 'ibe_day_'.$dayCount.'_audience';
                $day_location = 'ibe_day_'.$dayCount.'_location';
                $day_address1 = 'ibe_day_'.$dayCount.'_address1';
                $day_address2 = 'ibe_day_'.$dayCount.'_address2';
                $day_address3 = 'ibe_day_'.$dayCount.'_address3';
                $day_map = 'ibe_day_'.$dayCount.'_map';
                $day_times = 'ibe_day_'.$dayCount.'_times';
                $day_times2 = 'ibe_day_'.$dayCount.'_times2';
                $day_description = 'ibe_day_'.$dayCount.'_copy';
                $day_fineprint = 'ibe_day_'.$dayCount.'_fineprint';
                $day_image = 'ibe_day_'.$dayCount.'_image';
                $day_tickets_text = 'ibe_day_'.$dayCount.'_tickets_text';
                $day_tickets_url = 'ibe_day_'.$dayCount.'_tickets_url';
                $day_schedule_text = 'ibe_day_'.$dayCount.'_schedule_text';
                $day_schedule_url = 'ibe_day_'.$dayCount.'_schedule_url';
                $day_learnmore_url = 'ibe_day_'.$dayCount.'_learnmore_url';
                $day_enabled = 'ibe_day_'.$dayCount.'_enabled';
				$spacer = '<h4 class="spacer">spacer</h4>';

                echo '<section class="day-header">';
                if (  isset($day[$day_image][0]) ) {
                    echo '<div class="image-wrapper">';
                    /*
                    if ( isset($day[$day_title]) ) {
                        $cleaned_title = strtolower(str_replace('INDIE', '', $day[$day_title]));
                        echo '<h3>'.$cleaned_title.'</h3>';
                    }
                    */
                    $day_image = wp_get_attachment_image_src($day[$day_image][0], 'large' )[0];
                    echo '<img class="day-image" src="'.$day_image.'" />';
                    echo '</div>';
                }
                if ( isset($day[$day_title]) ) {
                    $cleaned_title = strtolower($day[$day_title]);
                    echo '<h3 class="day-title">'.$cleaned_title.'</h3>';
                }
                if ( isset($day[$day_map]) ) {
                    $map_open_tag = '<a href="'.$day[$day_map].'" target="_blank">';
                    $map_close_tag = '</a>';
                }  else {
                    $map_open_tag = '';
                    $map_close_tag = '';
                }
                if ( isset($day[$day_audience]) ) {
                    echo '<h4 class="audience">'.$day[$day_audience].'</h4>';
                }
                if ( isset($day[$day_date]) ) {
                    echo '<h4 class="date">'.$day[$day_date].'</h4>';
                }
                if ( isset($day[$day_times]) ) {
                    //echo '<h4 class="times">'.strtolower($day[$day_times]).'</h4>';
                    echo '<h4 class="times">'.$day[$day_times].'</h4>';
                }
                if ( isset($day[$day_date2]) ) {
                    echo '<h4 class="date">'.$day[$day_date2].'</h4>';
                }
                if ( isset($day[$day_times2]) ) {
                    //echo '<h4 class="times">'.strtolower($day[$day_times]).'</h4>';
                    echo '<h4 class="times">'.$day[$day_times2].'</h4>';
                }
                if ( isset($day[$day_description]) ) {
                    echo '<article class="description">'.$day[$day_description].'</article>';
                }
                if ( isset($day[$day_fineprint]) ) {
                    echo '<span class="fineprint">'.$day[$day_fineprint].'</span>';
                }
                if ( isset($day[$day_tickets_url]) && isset($day[$day_tickets_text]) && $dayCount !== 3 ) {
                    echo '<div class="button-wrapper" style="padding-bottom: 0;">';
                    echo '<a class="button" href="'.$day[$day_tickets_url].'" target="_blank">'.$day[$day_tickets_text].'</a>';
                    echo '</div>';
                }

                echo '</section>';
                if ( isset($day[$day_enabled]) ) {
                    echo '<div class="button-wrapper">';
                    if ( $dayCount === 3 ) {
                        if ( isset($day[$day_tickets_url]) && isset($day[$day_tickets_text]) ) {
                            echo '<a class="button" href="'.$day[$day_tickets_url].'" target="_blank">'.$day[$day_tickets_text].'</a>';
                        } else {
                            echo '<button class="ticketing-options dummy" href="#">Ticketing Options</button>';
                        }

                        if ( isset($day[$day_schedule_url]) && isset($day[$day_schedule_text]) ) {
                            echo '<a class="button" href="'.$day[$day_schedule_url].'" target="_blank">'.$day[$day_schedule_text].'</a>';
                        }

                        if ( isset($day[$day_learnmore_url]) && $day[$day_learnmore_url] != '' ) {
                            echo '<a class="button uplink-learn" href="'.$day[$day_learnmore_url].'" target="_blank">Learn More</a>';
                        } else {
                            echo '<button class="learn learn-more" href="#">Learn More</button>';
                        }
                    } else {
                        if ( isset($day[$day_schedule_url]) && isset($day[$day_schedule_text]) ) {
                            echo '<a class="button" href="'.$day[$day_schedule_url].'" target="_blank">'.$day[$day_schedule_text].'</a>';
                        }
                        if ( isset($day[$day_learnmore_url]) && $day[$day_learnmore_url] != '' ) {
                            echo '<a class="button uplink-learn" href="'.$day[$day_learnmore_url].'" target="_blank">Learn More</a>';
                        } else {
                            echo '<button class="learn learn-more" href="#">Learn More</button>';
                        }
                        //echo '<a class="button" style="margin-top: 0; opacity: 0; pointer-events: none;" href="http://indiebeautyexpo.com/wp-content/uploads/2016/12/IBELA2017_TradeIndieRunOfShowV5.pdf">View Schedule</a>';

                    }
                    echo '</div>';
                } else {
                    echo '<button class="disabled" href="#">Coming soon</button>';
                }

                // Blocks
                echo '<section class="blocks">';
                $blockGroup = 'ibe_day_'.$dayCount.'_blocks';
                if ( isset($days[$blockCount][$blockGroup]) ) {
                    foreach ($days[$blockCount][$blockGroup] as &$block) {
                        //$section_ticket_price = $block['ibe_day_'.$dayCount.'_section_ticketprice'];
                        //$section_url =          $block['ibe_day_'.$dayCount.'_section_url'];

                        // Paragraph
                        if ( isset($block['ibe_day_'.$dayCount.'_section_type']) && $block['ibe_day_'.$dayCount.'_section_type'] === 'section_type_paragraph') {
                            echo '<article class="paragraph">';
                            if ( isset($block['ibe_day_'.$dayCount.'_section_heading'])) {
                                echo '<h5>'.$block['ibe_day_'.$dayCount.'_section_heading'].'</h5>';
                            }
                            if ( isset($block['ibe_day_'.$dayCount.'_section_copy'])) {
                                echo '<article>'.$block['ibe_day_'.$dayCount.'_section_copy'].'</article>';
                            }
                            echo '</article>';
                        }
                        // Ticket
                        if ( isset($block['ibe_day_'.$dayCount.'_section_type']) && $block['ibe_day_'.$dayCount.'_section_type'] === 'section_type_ticket') {
                            echo '<article class="ticket">';
                            if ( isset($block['ibe_day_'.$dayCount.'_section_heading'])) {
                                echo '<h5>'.$block['ibe_day_'.$dayCount.'_section_heading'].'</h5>';
                            }
                            if ( isset($block['ibe_day_'.$dayCount.'_section_ticketprice'])) {
                                echo '<h6>'.$block['ibe_day_'.$dayCount.'_section_ticketprice'].'</h6>';
                            }
                            if ( isset($block['ibe_day_'.$dayCount.'_section_copy'])) {
                                echo '<article>'.$block['ibe_day_'.$dayCount.'_section_copy'].'</article>';
                            }
                            echo '</article>';
                        }
                        // Button
                        if ( isset($block['ibe_day_'.$dayCount.'_section_type']) && $block['ibe_day_'.$dayCount.'_section_type'] === 'section_type_button') {
                            echo '<article class="button">';
                            if ( isset($block['ibe_day_'.$dayCount.'_section_url'])) {
                                echo '<a class="button" href="'.$block['ibe_day_'.$dayCount.'_section_url'].'" target="_blank">';
                                if ( isset($block['ibe_day_'.$dayCount.'_section_heading'])) {
                                    echo $block['ibe_day_'.$dayCount.'_section_heading'].'</a>';
                                }
                            }
                            echo '</article>';
                        }
                        // Divider
                        if ( isset($block['ibe_day_'.$dayCount.'_section_type']) && $block['ibe_day_'.$dayCount.'_section_type'] === 'section_type_divider') {
                            echo '<article class="divider">';
                            if ( isset($block['ibe_day_'.$dayCount.'_divider'])) {
                                echo '<hr>';
                            }
                            echo '</article>';
                        }
                        // Callout
                        if ( isset($block['ibe_day_'.$dayCount.'_section_type']) && $block['ibe_day_'.$dayCount.'_section_type'] === 'section_type_callout') {
                            echo '<article class="callout">';
                            if ( isset($block['ibe_day_'.$dayCount.'_section_heading'])) {
                                echo '<h5>'.$block['ibe_day_'.$dayCount.'_section_heading'].'</h5>';
                            }
                            if ( isset($block['ibe_day_'.$dayCount.'_section_copy'])) {
                                echo '<article>'.$block['ibe_day_'.$dayCount.'_section_copy'].'</article>';
                            }
                            echo '</article>';
                        }
                    }
                }
                echo '<article class="location">';

                echo '</article>';
                echo '</section>';

                if ($show_newlayout) {
                    include( locate_template( 'templates/partials/section-show-exhibitor-list.php', false, false ) );
                }

                echo '</div>';
                $blockCount++;
            } else {
                if ($tbaCount <= 0) {
                    echo '<div class="day tba">';
                    echo '<section>';
                    echo '<h3>Information for this event will be announced soon</h3>';
                    echo '<p>Join the IBE newsletter for updates</p>';
                    echo '<a class="button newsletter">Sign Up</a>';
                    echo '</section>';
                    echo '</div>';
                    $tbaCount++;
                }
            }

        }
            echo '</div>';

            // Supplemental (Venue / Exhibitors)
            if ($show_newlayout) {
                include( locate_template( 'templates/partials/section-show-venue.php', false, false ) );
            } else {
                include( locate_template( 'templates/partials/section-show-supplemental.php', false, false ) );
            }

        echo '</div>';
    }
    //echo '</div>';
    echo '</section>';


?>
