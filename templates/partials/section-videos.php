<?php

    echo '<section class="media video footer" id="footer-videos">';
        $count = 0;
        $videos = rwmb_meta( 'ibe_videos' );
        foreach ($videos as &$video) {
            $count++;
            $youtube_video = $video['ibe_video_youtube_oembed'];

            $pos = strpos($youtube_video,'embed/');
            if($pos === false) {
                $youtube_video = explode('://youtu.be/', $youtube_video);
                $youtube_video = $youtube_video[1];
            }
            else {
                $youtube_video = explode('embed/', $youtube_video);
                $youtube_video = explode('?', $youtube_video[1]);
                $youtube_video = $youtube_video[0];
            }

            echo '<div class="media-object">';
            if ( isset($youtube_video ) && $youtube_video !== 'Embed HTML not available.' ) {
                echo '<div class="video-container overlay-active">';
                if ( isset($video['ibe_video_title']) ) {
                    echo '<a class="video-overlay"><span class="active">'.$video['ibe_video_title'].'</span></a>';
                } else {
                    echo '<div class="video-overlay"></div>';
                }
                echo '<iframe id="frame'.$count.'" width="100%" height="100%" src="https://www.youtube.com/embed/'.$youtube_video.'?modestbranding=1&showinfo=0&autoplay=0&controls=1&playsinline=1&loop=0&playlist='.$youtube_video.'" frameborder="0" allowfullscreen=""></iframe>';
                echo '</div>';
            }
            echo '<section class="media-header">';
            if ( isset($video['ibe_video_tagline']) ) {
                echo '<h3>'.$video['ibe_video_tagline'].'</h3>';
            }
            if ( isset($video['ibe_video_description']) ) {
                echo '<article>'.$video['ibe_video_description'].'</article>';
            }
            echo '</section>';
            echo '</div>';
        }
    echo '</section>';
?>
