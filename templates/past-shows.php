<?php
/**
 *
 *
 * @file           past-shows.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 * Template Name: Past Shows
 */
 ?>

 <?php
     get_header();
     $shows = new WP_Query( 'post_type=show&post_status=publish&posts_per_page=-1' );
     $all_shows = $shows->posts;
 ?>
 <div id="past-shows" class="page">
     <div class="container">
        <?php include( locate_template( 'templates/partials/header-past-shows.php', false, false ) );  ?>

        <main class="past-shows-main">
            <?php
                //var_dump($loop->posts);
                include( locate_template( 'templates/partials/section-past-show-list.php', false, false ) );
            ?>
        </main>


     </div>
 </div>
 <?php get_footer(); ?>
