<?php
/**
 *
 *
 * @file           subscribed.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 * Template Name: Subscribed
 */
?>
<?php

get_header();
$loop = new WP_Query( 'post_type=page&p='.$post->ID );
while ( $loop->have_posts() ) : $loop->the_post(); ?>

<div id="subscribed" class="page">
    <div class="container">

        <?php
        echo "<h1>";
        the_title();
        echo "</h1>";
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
            echo '
            <div id="templateBody" class="bodyContent rounded6">
                <div id="subscribeFormWelcome">
                    '.get_the_content().'
                </div>
                <form action="https://Indiebeautyexpo.us9.list-manage.com/profile/post" method="POST" id="subprefs-form">
                	<input type="hidden" name="u" value="f4a3d85a1388923b3fe3a6cb9">
                	<input type="hidden" name="id" value="4ed1cffcc8">
                	<input type="hidden" name="e" value="*|UNIQID|*">
                	<input type="hidden" name="orig-lang" value="1">

                	<label for="MERGE0">Email Address <span class="req asterisk">*</span></label>
                	<div class="field-group">
                		<input type="email" autocapitalize="off" autocorrect="off" name="MERGE0" id="MERGE0" size="25" value="">
                	</div>
                	<label for="MERGE1">First Name</label>
                	<div class="field-group">
                		<input type="text" name="MERGE1" id="MERGE1" size="25" value="">
                	</div>
                	<label for="MERGE2">Last Name</label>
                	<div class="field-group">
                		<input type="text" name="MERGE2" id="MERGE2" size="25" value="">
                	</div>
                	<label for="MERGE3">Brand</label>
                	<div class="field-group">
                		<input type="text" name="MERGE3" id="MERGE3" size="25" value="">
                	</div>
                	<label for="MERGE5">Position</label>
                	<div class="field-group">
                		<input type="text" name="MERGE5" id="MERGE5" size="25" value="">
                	</div>
                	<label for="MERGE4">NOTES</label>
                	<div class="field-group">
                		<input type="text" name="MERGE4" id="MERGE4" size="25" value="">
                	</div>
                	<label for="MERGE6">Buyer Company</label>
                	<div class="field-group">
                		<input type="text" name="MERGE6" id="MERGE6" size="25" value="">
                	</div>
                	<label for="MERGE7">Type Of Retail</label>
                	<div class="field-group">
                		<input type="text" name="MERGE7" id="MERGE7" size="25" value="">
                	</div>
                	<div>
                		<label>How would you describe yourself?</label>
                		<div class="field-group groups">
                			<ul class="interestgroup_field radio-group">
                				<li class="!margin-bottom--lv2"> <label for="group_1" class="radio !margin--lv0"><input data-dojo-type="dijit/form/RadioButton" type="radio" id="group_1" name="group[28229]" value="1" class="av-radio required" required><span>Brand Founder/Entrepreneur</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label for="group_2" class="radio !margin--lv0"><input data-dojo-type="dijit/form/RadioButton" type="radio" id="group_2" name="group[28229]" value="2" class="av-radio"><span>Buyer/Retailer</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label for="group_4" class="radio !margin--lv0"><input data-dojo-type="dijit/form/RadioButton" type="radio" id="group_4" name="group[28229]" value="4" class="av-radio"><span>Press</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label for="group_8" class="radio !margin--lv0"><input data-dojo-type="dijit/form/RadioButton" type="radio" id="group_8" name="group[28229]" value="8" class="av-radio"><span>A Beauty Lover</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label for="group_34359738368" class="radio !margin--lv0"><input data-dojo-type="dijit/form/RadioButton" type="radio" id="group_34359738368" name="group[28229]" value="34359738368" class="av-radio"><span>Beauty Industry Professional</span> </label>					</li>
                				<li class="!margin-bottom--lv2"> <label for="group_68719476736" class="radio !margin--lv0"><input data-dojo-type="dijit/form/RadioButton" type="radio" id="group_68719476736" name="group[28229]" value="68719476736" class="av-radio"><span>Other Trade Professional</span> </label> </li>
                			</ul>
                		</div>
                	</div>
                	<div style="display:none;">
                		<label>IBE LA 2018</label>
                		<div class="field-group groups">
                			<ul class="interestgroup_field checkbox-group">
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_16"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_16" name="group[28277][16]" value="1" class="av-checkbox"><span>Exhibitors</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_32"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_32" name="group[28277][32]" value="1" class="av-checkbox"><span>Buyers</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_64"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_64" name="group[28277][64]" value="1" class="av-checkbox"><span>Press</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_128"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_128" name="group[28277][128]" value="1" class="av-checkbox"><span>Shop Attendees</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_256"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_256" name="group[28277][256]" value="1" class="av-checkbox"><span>Investors</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_549755813888"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_549755813888" name="group[28277][549755813888]" value="1" class="av-checkbox"><span>Trade Attendees</span> </label> </li>
                			</ul>

                		</div>
                	</div>
                	<div style="display:none;">
                		<label>IBE NY 2016</label>
                		<div class="field-group groups">
                			<ul class="interestgroup_field checkbox-group">
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_512"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_512" name="group[28301][512]" value="1" class="av-checkbox"><span>Exhibitors</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_1024"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_1024" name="group[28301][1024]" value="1" class="av-checkbox"><span>Attendees</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_2048"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_2048" name="group[28301][2048]" value="1" class="av-checkbox"><span>Buyers</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_4096"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_4096" name="group[28301][4096]" value="1" class="av-checkbox"><span>Press</span> </label> </li>
                			</ul>

                		</div>
                	</div>
                	<div style="display:none;">
                		<label>Team IBE</label>
                		<div class="field-group groups">
                			<ul class="interestgroup_field checkbox-group">
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_8192"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_8192" name="group[28305][8192]" value="1" class="av-checkbox"><span>Sales Team</span> </label> </li>
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_274877906944"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_274877906944" name="group[28305][274877906944]" value="1" class="av-checkbox"><span>For Press/Buyer Emails</span> </label> </li>
                			</ul>

                		</div>
                	</div>
                	<div style="display:none;">
                		<label>Previously Exhibited Brands</label>
                		<div class="field-group groups">
                			<ul class="interestgroup_field checkbox-group">
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_262144"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_262144" name="group[28309][262144]" value="1" class="av-checkbox"><span>IBE DL 2017</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_16777216"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_16777216" name="group[28309][16777216]" value="1" class="av-checkbox"><span>IBE NY 2017</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_16384"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_16384" name="group[28309][16384]" value="1" class="av-checkbox"><span>IBE NY 2016</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_65536"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_65536" name="group[28309][65536]" value="1" class="av-checkbox"><span>IBE NY 2015</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_2097152"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_2097152" name="group[28309][2097152]" value="1" class="av-checkbox"><span>MISC</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_17179869184"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_17179869184" name="group[28309][17179869184]" value="1" class="av-checkbox"><span>IBE LA 2017</span> </label> </li>
                			</ul>

                		</div>
                	</div>
                	<div style="display:none;">
                		<label>IBE NY 2015</label>
                		<div class="field-group groups">
                			<ul class="interestgroup_field checkbox-group">
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_32768"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_32768" name="group[28313][32768]" value="1" class="av-checkbox"><span>Exhibitors</span> </label> </li>
                			</ul>

                		</div>
                	</div>
                	<div style="display:none;">
                		<label>IBE DL 2017</label>
                		<div class="field-group groups">
                			<ul class="interestgroup_field checkbox-group">
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_131072"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_131072" name="group[28317][131072]" value="1" class="av-checkbox"><span>Exhibitors</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_524288"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_524288" name="group[28317][524288]" value="1" class="av-checkbox"><span>SHOP Attendees</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_1048576"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_1048576" name="group[28317][1048576]" value="1" class="av-checkbox"><span>Buyers</span> </label> </li>
                			</ul>

                		</div>
                	</div>
                	<div style="display:none;">
                		<label>IBE NY 2017</label>
                		<div class="field-group groups">
                			<ul class="interestgroup_field checkbox-group">
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_4194304"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_4194304" name="group[28325][4194304]" value="1" class="av-checkbox"><span>Exhibitors</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_8388608"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_8388608" name="group[28325][8388608]" value="1" class="av-checkbox"><span>Attendees</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_33554432"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_33554432" name="group[28325][33554432]" value="1" class="av-checkbox"><span>Press</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_67108864"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_67108864" name="group[28325][67108864]" value="1" class="av-checkbox"><span>Buyers</span> </label> </li>
                			</ul>

                		</div>
                	</div>
                	<div style="display:none;">
                		<label>Boxed Collection Purchasees</label>
                		<div class="field-group groups">
                			<ul class="interestgroup_field checkbox-group">
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_134217728"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_134217728" name="group[28329][134217728]" value="1" class="av-checkbox"><span>BIS 2016</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_268435456"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_268435456" name="group[28329][268435456]" value="1" class="av-checkbox"><span>Discover Dallas</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_536870912"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_536870912" name="group[28329][536870912]" value="1" class="av-checkbox"><span>BIS 2017</span> </label> </li>
                			</ul>

                		</div>
                	</div>
                	<div style="display:none;">
                		<label>IBE LA 2017</label>
                		<div class="field-group groups">
                			<ul class="interestgroup_field checkbox-group">
                				<li class="!margin-bottom--lv2"> <label class="checkbox" for="group_1073741824"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_1073741824" name="group[28333][1073741824]" value="1" class="av-checkbox"><span>Exhibitors</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_2147483648"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_2147483648" name="group[28333][2147483648]" value="1" class="av-checkbox"><span>Attendees</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_4294967296"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_4294967296" name="group[28333][4294967296]" value="1" class="av-checkbox"><span>Buyers</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_8589934592"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_8589934592" name="group[28333][8589934592]" value="1" class="av-checkbox"><span>Press</span> </label> </li>
                				<li class="!margin-bottom--lv2">
                				<label class="checkbox" for="group_137438953472"><input type="checkbox" data-dojo-type="dijit/form/CheckBox" id="group_137438953472" name="group[28333][137438953472]" value="1" class="av-checkbox"><span>Trade Attendees</span> </label> </li>
                			</ul>

                		</div>
                	</div>
                	<div class="mergeRow">
                		<label>Preferred format</label>
                		<div class="field-group groups">
                			<div class="interestgroup_field radio-group">
                				<label class="radio" for="EMAILTYPE_HTML"><input type="radio" name="EMAILTYPE" id="EMAILTYPE_HTML" value="html" checked=""><span>HTML</span></label>
                				<label class="radio" for="EMAILTYPE_TEXT"><input type="radio" name="EMAILTYPE" id="EMAILTYPE_TEXT" value="text"><span>Plain-text</span></label>
                			</div>
                		</div>
                	</div>
                	<br>
                	<input class="formEmailButton" type="submit" value="Update Profile" id="">
                	<span class="or">or <a href="/unsubscribe?u=f4a3d85a1388923b3fe3a6cb9&amp;id=4ed1cffcc8&amp;e=*|UNIQID|*">Unsubscribe</a></span>
                </form>
            </div>
            ';
			endwhile;
		endif;
        ?>
    </div> <!-- /container -->
</div><!-- /ww -->

<?php endwhile; ?>
<?php get_footer(); ?>
