<?php
/**
 *
 *
 * @file           exhibitor-list.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 * Template Name: Exhibitor List
 */
 ?>
<?php
    get_header();
    $loop = new WP_Query( 'post_type=page&p='.$post->ID );
    while ( $loop->have_posts() ) : $loop->the_post();
?>

 <div id="exhibitor-list" class="page">
     <div class="container">
     		<?php if ( have_posts() ) : ?>
                <?php include( locate_template( 'templates/partials/section-exhibitor-list-header.php', false, false ) );  ?>
                <main class="exhibitor-list">
                    <?php include( locate_template( 'templates/partials/section-exhibitor-list-main.php', false, false ) );  ?>
                </main>
                <aside id="secondary" class="sidebar" role="complementary">
                    <?php include( locate_template( 'templates/partials/section-exhibitor-list-sidebar.php', false, false ) );  ?>
                </aside><!-- #secondary -->

             <?php endif; ?>
     </div> <!-- /container -->
 </div><!-- /ww -->

 <?php endwhile; ?>
 <?php get_footer(); ?>
