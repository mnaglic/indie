<?php
/**
 *
 *
 * @file           home.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 * Template Name: Home
 */
?>
<?php

get_header();
if ( isset($_GET['v']) ) {
    $view = hexdec($_GET['v']);
} else {
    $view = rwmb_meta( 'ibe_default_landing' );
}

$loop = new WP_Query( 'post_type=landing&p='.$view );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<div id="home" class="page landing-<?php echo $view; ?>">
    <div class="container">
        <?php get_template_part( 'templates/partials/header-carousel', 'none' ); ?>
        <?php get_template_part( 'templates/partials/section-featured-content', 'none' ); ?>
        <!-- maybe next line dont need -->
        <?php get_template_part( 'templates/partials/section-show-day-list','none');  ?>
            <?php get_template_part( 'templates/partials/section-attend-sidebar-shows','none');  ?>

    </div>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
