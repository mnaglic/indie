<?php
/**
 *
 *
 * @file           about.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 * Template Name: Sponsors
 */
 ?>
 <?php

 get_header();
 $loop = new WP_Query( 'post_type=page&p='.$post->ID );
 while ( $loop->have_posts() ) : $loop->the_post();

 $contacts = rwmb_meta( 'ibe_contact' );
 ?>

 <div id="sponsors" class="page">
     <div class="container">
     		<?php if ( have_posts() ) : ?>
                <h1><?php the_title(); ?></h1>
                <?php include( locate_template( 'templates/partials/header-large-backgound.php', false, false ) );  ?>
                <?php include( locate_template( 'templates/partials/section-sponsors.php', false, false ) );  ?>
                <?php include( locate_template( 'templates/partials/section-carousel.php', false, false ) );  ?>
                <?php include( locate_template( 'templates/partials/section-sponsor-form.php', false, false ) );  ?>
            <?php endif; ?>

     </div> <!-- /container -->
 </div><!-- /ww -->

 <?php endwhile; ?>
 <?php get_footer(); ?>
