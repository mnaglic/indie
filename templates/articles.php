<?php
/**
 *
 *
 * @file           articles.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 * Template Name: Articles
 */

get_header();


?>
<div id="home" class="page landing-<?php echo $view; ?>">
    <div class="container">
        <?php

        wp_reset_postdata();
        if (is_paged()) {
            $post_num = 8;
        } else {
            $post_num = 7;
        }
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $query_args = array(
            'post_type' => 'article',
            'orderby' => 'date',
            'order' => 'DESC',
            'cat' => -7, // Exclude posts marked 'Private'
            'posts_per_page' => $post_num,
            'paged' => $paged
        );

        echo '<section class="featured-items">';
        $the_query = new WP_Query( $query_args );
        $post_count = 0;
        if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
            $post->ID = $post->ID;
            $item_title = str_replace(' | ', '<br />', get_the_title());
            $item_link = get_permalink( $post->ID );
            if ($post->post_excerpt !== '') {
                $item_text = get_the_excerpt( $post->ID );
            } else {
                $item_text = wp_trim_words( $post->post_content, 25, '...' );
            }
            if ( has_post_thumbnail( $post->ID ) ) {
                $image_array_lg = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                $image_large = $image_array_lg[0];
                $image_array_rt = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'retina' );
                $image_retina = $image_array_rt[0];
                $has_image = true;
            } else {
                $has_image = null;
            }
            if (!is_paged() ) {
                if ( $post_count < 1 ) {
                    $class = 'spotlight-item';
                    $post_count++;
                } else {
                    if ($post_count % 2 == 0) {
                        $class = 'featured-item last';
                    } else {
                        $class = 'featured-item';
                    }
                    $post_count++;
                }
            } else {
                if ($post_count % 2 == 0) {
                    $class = 'featured-item ';
                } else {
                    $class = 'featured-item last';
                }
                $post_count++;
            }


            if ( $item_title !== '' ) {
                echo '<div id="sort-'.$post_count.'" class="'.$class.'">';
                echo '  <div class="photo">';
                    echo '<a href="'.$item_link.'">';
                    if ( $has_image !== null ) {
                        if ( $class === 'spotlight-item' ) {
                            echo '<div style="background-image: url('.$image_retina.');"></div>';
                        } else {
                            echo '<div style="background-image: url('.$image_large.');"></div>';
                        }
                    } else {
                        echo '<div class="post-bg"></div>';
                    }
                    echo '  </a>';

                echo '    <h3><a href="'.$item_link.'" class="ha">'.$item_title.'</a></h3>';
                if ( $item_text !== '' ) {
                    echo '    <article>'.$item_text.'</article>';
                }
                echo '  </div>';
                echo '</div>';
            }
        endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif; ?>
        </section>

        <div class="pagination-wrapper">
            <?php
            if (  $the_query->max_num_pages > 1 ) : ?>
            <nav id="pagination">
                <?php numeric_posts_nav();
                ?>
            </nav>
            <?php  endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
