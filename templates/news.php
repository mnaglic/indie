<?php
/**
 *
 *
 * @file           news-feed.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 * Template Name: News
 */

get_header(); ?>

<div id="news" class="page run">
    <div class="container news-feed">

    <?php
        wp_reset_postdata();
        global $authordata;

        if (is_paged()) {
           $post_num = 6;
        } else {
           $post_num = 6;
        }
        $post_count = 0;

        // Query
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $query_args = array(
           'post_type' => 'post',
           'orderby' => 'date',
           'order' => 'DESC',
           //'cat' => -7, // Exclude posts marked 'Private'
           'posts_per_page' => $post_num,
           'paged' => $paged
        );
        $the_query = new WP_Query( $query_args );

        // Loop
        if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
           $post->ID = $post->ID;

           $item_title = str_replace(' | ', '<br />', get_the_title());
           $item_link = get_permalink( $post->ID );
           if ($post->post_excerpt !== '') {
               $item_text = strip_tags( get_the_excerpt( $post->ID ) );
               $item_text_long = $item_text = strip_tags( get_the_excerpt( $post->ID ) );
           } else {
               //$item_text = wp_trim_words( $post->post_content, 25, '... <a href="'.$item_link.'">read more</a>' );
               $excerpt_length = 250;
               $long_excerpt_length = 350;
               $content = apply_filters('the_content', strip_tags( get_the_content() ) );
               $item_text = truncate( $content, $excerpt_length, '... <a href="'.$item_link.'">read more</a>', false, true );
               $item_text_long = truncate( $content, $long_excerpt_length, '... ' . '<a href="'.$item_link.'">read more</a>', false, true );
               //var_dump($item_link);
               //$item_text_long = wp_trim_words( $post->post_content, 50,  );
           }
           if ( has_post_thumbnail( $post->ID ) ) {
               $image_array_lg = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
               $image_large = $image_array_lg[0];
               $image_array_rt = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'retina' );
               $image_retina = $image_array_rt[0];
               $has_image = true;
           } else {
               $has_image = null;
           }

           /*
            if (!is_paged() ) {
                if ( $post_count < 1 ) {
                    if ( has_post_thumbnail( $page_id ) ) {
            			$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
            			$style = 'background-image: url('.$src[0].')';
            		} else {
            			$style = '';
            		}
                    echo '<header>';
                    if ( has_post_thumbnail( $page_id ) ) {
                        $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                        $style = 'background-image: url('.$src[0].')';
                    } else {
                        $style = '';
                    }
                    echo '  <aside>';
                    echo '      <div class="entry-header">';
                    echo '          <div class="entry-meta">';
                    echo               ibe__posted_on();
                    echo               ibe_news_categories();
                    echo '          </div><!-- .entry-meta -->';
                    echo '      </div>';
                    echo '      <a href="'.$item_link.'">';
                    echo '          <div class="featured-image post-bg" style="'.$style.'"></div>';
                    echo '      </a>';
                    echo '  </aside>';
                    echo '  <article id="post-'.get_the_ID().'" data-sort-order="'.$post_count.'">';
                    echo '      <div class="news-wrapper">';
                    echo '          <a href="'.$item_link.'"><h2 class="entry-title">'.str_replace(' | ', ' ', get_the_title()).'</h2></a>';
                    echo '          <span class="by-line"><a href='.get_author_posts_url( $authordata->ID, $authordata->user_nicename ).'>By '.get_the_author()."</a></span>\r\n";
                    echo '          <div class="entry-content">'.$item_text_long.'</div>';
                    echo '      </div>';
                    echo '  </article>';
                    echo '  <footer class="entry-footer"><span></span></footer>';
                    echo '</header>';
                    echo '<main>';
                } else {
                    echo '  <article id="post-'.get_the_ID().'" data-sort-order="'.$post_count.'">';
                    echo '      <header class="entry-header">';
                    echo '          <div class="entry-meta">';
                    echo                ibe__posted_on();
                    echo                ibe_news_categories();
                    echo '          </div><!-- .entry-meta -->';
                    if ( has_post_thumbnail( $page_id ) ) {
                        $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                        $style = 'background-image: url('.$src[0].')';
                    } else {
                        $style = '';
                    }
                    echo '          <a href="'.$item_link.'">';
                    echo '              <div class="featured-image post-bg" style="'.$style.'"></div>';
                    echo '          </a>';
                    echo '      </header>';
                    echo '      <a href="'.$item_link.'"><h2 class="entry-title">'.str_replace(' | ', ' ', get_the_title()).'</h2></a>';
                    echo '      <span class="by-line"><a href='.get_author_posts_url( $authordata->ID, $authordata->user_nicename ).'>By '.get_the_author()."</a></span>\r\n";
                    echo '      <div class="entry-content">'.$item_text.'</div>';
                    echo '      <footer class="entry-footer"><span></span></footer>';
                    echo '  </article>';
                }
                $post_count++;
            } else {
            */
                if ( $post_count < 1 ) {
                    if ( has_post_thumbnail( $page_id ) ) {
            			$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
            			$style = 'background-image: url('.$src[0].')';
            		} else {
            			$style = '';
            		}
                    echo '<main>';
                    echo '  <article id="post-'.get_the_ID().'" data-sort-order="'.$post_count.'">';
                    echo '      <header class="entry-header">';
                    echo '          <div class="entry-meta">';
                    echo                ibe__posted_on();
                    echo                ibe_news_categories();
                    echo '          </div><!-- .entry-meta -->';
                    if ( has_post_thumbnail( $page_id ) ) {
                        $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                        $style = 'background-image: url('.$src[0].')';
                    } else {
                        $style = '';
                    }
                    echo '          <a href="'.$item_link.'">';
                    echo '              <div class="featured-image post-bg" style="'.$style.'"></div>';
                    echo '          </a>';
                    echo '      </header>';
                    echo '      <a href="'.$item_link.'"><h2 class="entry-title">'.str_replace(' | ', ' ', get_the_title()).'</h2></a>';
                    echo '      <span class="by-line"><a href='.get_author_posts_url( $authordata->ID, $authordata->user_nicename ).'>By '.get_the_author()."</a></span>\r\n";
                    echo '      <div class="entry-content">'.$item_text.'</div>';
                    echo '      <footer class="entry-footer"><span></span></footer>';
                    echo '  </article>';
                } else {
                    echo '  <article id="post-'.get_the_ID().'" data-sort-order="'.$post_count.'">';
                    echo '      <header class="entry-header">';
                    echo '          <div class="entry-meta">';
                    echo                ibe__posted_on();
                    echo                ibe_news_categories();
                    echo '          </div><!-- .entry-meta -->';
                    if ( has_post_thumbnail( $page_id ) ) {
                        $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                        $style = 'background-image: url('.$src[0].')';
                    } else {
                        $style = '';
                    }
                    echo '          <a href="'.$item_link.'">';
                    echo '              <div class="featured-image post-bg" style="'.$style.'"></div>';
                    echo '          </a>';
                    echo '      </header>';
                    echo '      <a href="'.$item_link.'"><h2 class="entry-title">'.str_replace(' | ', ' ', get_the_title()).'</h2></a>';
                    echo '      <span class="by-line"><a href='.get_author_posts_url( $authordata->ID, $authordata->user_nicename ).'>By '.get_the_author()."</a></span>\r\n";
                    echo '      <div class="entry-content">'.$item_text.'</div>';
                    echo '      <footer class="entry-footer"><span></span></footer>';
                    echo '  </article>';
                }
                $post_count++;
            //}
        ?>



<?php endwhile; else: ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

        </main>
        <?php get_sidebar(); ?>





   </div>
   <div class="pagination-wrapper">
       <?php if (  $the_query->max_num_pages > 1 ) : ?>
       <nav id="pagination">
           <?php numeric_posts_nav(); ?>
       </nav>
       <?php  endif; ?>
   </div>
</div>

<?php get_footer(); ?>
