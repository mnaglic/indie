<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package iBE_2016
 */

$deck = rwmb_meta( 'ibe_deck_url' );
header('Location: '. $deck );
