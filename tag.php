<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package iBE_2016
 */

get_header(); ?>

<div id="search-results" class="page">
    <div class="container">

		<?php
        //query_posts('cat=-3');
		if ( have_posts() ) : ?>
			<?php $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1; ?>
			<header class="page-header">
				<?php //the_archive_title( '<h2>', '</h2>' ); ?>
                <h2><?php echo str_replace( 'Category: ', '', get_the_archive_title() ); ?></h2>
			</header><!-- .page-header -->
			<section class="search-items">
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

			get_template_part( 'templates/partials/content', 'search' );

			endwhile; ?>

			<nav id="pagination">
				<?php //numeric_posts_nav(); ?>
				<?php the_posts_navigation(); ?>
			</nav>

		<?php else : ?>

		<?php 	get_template_part( 'templates/partials/content', 'none' );

		endif; ?>
		</section>
	</div>
</div>

<?php get_footer();
