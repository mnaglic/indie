jQuery(function (){
    // attend
    var content = '<li class="img-box">';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-dallas active"><div class="info"><p class="img-title">Ibe Dallas<br>2019</p></div></a>';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-new-york"><div class="info"><p class="img-title">Ibe New York<br>2019</p></div></a>';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-london"><div class="info"><p class="img-title">Ibe London<br>2019</p></div></a>';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-los-angeles"><div class="info"><p class="img-title">Ibe Los Angeles<br>2019</p></div></a>';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-berlin"><div class="info"><p class="img-title">Ibe Berlin<br>2019</p></div></a>';
    content += '</li>';
    jQuery('#menu-item-364 .sub-menu').append(content);

    // exibit
    var content = '<li class="img-box">';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-dallas active"><div class="info"><p class="img-title">Ibe Dallas<br>2019</p></div></a>';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-new-york"><div class="info"><p class="img-title">Ibe New York<br>2019</p></div></a>';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-london"><div class="info"><p class="img-title">Ibe London<br>2019</p></div></a>';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-los-angeles"><div class="info"><p class="img-title">Ibe Los Angeles<br>2019</p></div></a>';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-berlin"><div class="info"><p class="img-title">Ibe Berlin<br>2019</p></div></a>';
    content += '</li>';
    jQuery('#menu-item-365 .sub-menu').append(content);


    // news
    var content = '<li class="img-box">';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-dallas active"><div class="info"><p class="img-title">Ibe Dallas<br>2019</p></div></a>';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-berlin"><div class="info"><p class="img-title">Ibe Berlin<br>2019</p></div></a>';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-los-angeles"><div class="info"><p class="img-title">Ibe Los Angeles<br>2019</p></div></a>';
    content += '</li>';
    jQuery('#menu-item-366 .sub-menu').append(content);


    //  about
    var content = '<li class="img-box">';
    content += '<a href="https://indiebeautyexpo.com/attend/" class="inner bg-dallas active"><div class="info"><p class="img-title">Ibe Dallas<br>2019</p></div></a>';
    content += '</li>';
    jQuery('#menu-item-928 .sub-menu').append(content);

    jQuery('.menu-button').click(function(){
        jQuery('#menu-wrapper').toggleClass('show');
    });

    jQuery('.menu-item').mouseenter(function(){
        jQuery(this).children('.sub-menu').show();
    });

    jQuery('.menu-item').mouseleave(function(){
        jQuery(this).children('.sub-menu').hide();
    });

    jQuery('.menu-navigation-container ul.sub-menu').hide();
    // attend hover

    jQuery('#menu-item-364 .sub-menu .menu-item:nth-child(1)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-dallas').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-dallas').siblings().removeClass('active');
    });

    jQuery('#menu-item-364 .sub-menu .menu-item:nth-child(2)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-new-york').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-new-york').siblings().removeClass('active');
    });

    jQuery('#menu-item-364 .sub-menu .menu-item:nth-child(3)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-london').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-london').siblings().removeClass('active');
    });

    jQuery('#menu-item-364 .sub-menu .menu-item:nth-child(4)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-los-angeles').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-los-angeles').siblings().removeClass('active');
    });

    jQuery('#menu-item-364 .sub-menu .menu-item:nth-child(5)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-berlin').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-berlin').siblings().removeClass('active');
    });

    // exibit hover

    jQuery('#menu-item-365 .sub-menu .menu-item:nth-child(1)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-dallas').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-dallas').siblings().removeClass('active');
    });

    jQuery('#menu-item-365 .sub-menu .menu-item:nth-child(2)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-new-york').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-new-york').siblings().removeClass('active');
    });

    jQuery('#menu-item-365 .sub-menu .menu-item:nth-child(3)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-london').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-london').siblings().removeClass('active');
    });

    jQuery('#menu-item-365 .sub-menu .menu-item:nth-child(4)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-los-angeles').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-los-angeles').siblings().removeClass('active');
    });

    jQuery('#menu-item-365 .sub-menu .menu-item:nth-child(5)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-berlin').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-berlin').siblings().removeClass('active');
    });

    // news hover

    jQuery('#menu-item-366 .sub-menu .menu-item:nth-child(1)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-dallas').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-dallas').siblings().removeClass('active');
    });

    jQuery('#menu-item-366 .sub-menu .menu-item:nth-child(2)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-berlin').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-berlin').siblings().removeClass('active');
    });

    jQuery('#menu-item-366 .sub-menu .menu-item:nth-child(3)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-los-angeles').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-los-angeles').siblings().removeClass('active');
    });

    // news hover

    jQuery('#menu-item-928 .sub-menu .menu-item:nth-child(1)').mouseenter(function(){
        jQuery(this).siblings('.img-box').children('.bg-dallas').addClass('active');
        jQuery(this).siblings('.img-box').children('.bg-dallas').siblings().removeClass('active');
    });

    // shop indie
    jQuery('.day-2 .learn.learn-more').click(function (){
        console.log('shop indie');
        jQuery(this).hide().parent().next().slideDown();
    });

    // trade indie
    jQuery('.day-3 .button-wrapper button').click(function (){
        console.log('shop indie');
        jQuery(this).hide().siblings().hide().parent().next().slideDown();
    });

    jQuery('#menu-item-8094 a, #menu-item-8095 a').attr('target', '_blank');

    window.onresize = function() {
        updateBoxSizes();
    }

    setTimeout(function() {
        updateBoxSizes();
    }, 200);

});

function updateBoxSizes() {
    var dayElement = jQuery('#home .day-2');
    var newsElement = jQuery('#home .sidebar-main');

    var venueElement = jQuery('.day.venue');
    var exhibitorsElement = jQuery('.day exhibitors');
    var quoteElement = jQuery('.slick-wrapper')

    console.log('venueElement exhibitorsElement', venueElement.height(), exhibitorsElement.height())

    dayElement.removeAttr('style');
    newsElement.removeAttr('style');
    venueElement.removeAttr('style');
    exhibitorsElement.removeAttr('style');
    quoteElement.removeAttr('style');

    if (window.innerWidth < 1001) return;

    if (dayElement.height() > newsElement.height()) {
        newsElement.attr('style', 'height: ' + dayElement.height() + 'px');
    } else {
        dayElement.attr('style', 'height: ' + newsElement.height() + 'px');
    }

    if (venueElement.height() > quoteElement.height()) {
        console.log('venueElement');
        quoteElement.attr('style', 'height: ' + venueElement.height() + 'px');
    } else {
        console.log('quoteElement');
        venueElement.attr('style', 'height: ' + quoteElement.height() + 'px');
    }

    console.log('dayElement sidebar', dayElement.height(), newsElement.height());
}