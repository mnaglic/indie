// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Detect IE
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function detectIE() {
  var ua = window.navigator.userAgent;

  // Test values; Uncomment to check result …

  // IE 10
  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

  // IE 11
  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

  // Edge 12 (Spartan)
  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

  // Edge 13
  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}


// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Debouncing Resize Event
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

(function(jQuery,sr){

  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Detect mobile devices and set display mode
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	if ( Modernizr.touch ) { // your code}
	// if (Modernizr.mq('only all and (max-width: 480px)')) {}

var mobileMode;
var vp = {
    width: null,
    height: null,
	detach: function () {
    return this.height * 0.05;
  }
};
//var displayThreshhold = 768;
/*
function detectMobileMode() {
	if (device.desktop() || device.tablet()) {
		mobileMode = false;
	} else {
		mobileMode = true;
	}
	return mobileMode;
}
*/
function checkViewport() {
    var bodyStyles = window.getComputedStyle(document.body);
    var menuMobileBreakpoint = parseInt(bodyStyles.getPropertyValue('--breakpoint_min'), 10);
	vp.width = jQuery(window).width();
	vp.height = jQuery(window).height();
	vp.detach = vp.width * 0.05;
    //console.log(menuMobileBreakpoint);
    //body.style.setProperty('--mobile-break', newValue);
    if (jQuery(window).width() <= menuMobileBreakpoint) {
        if (!jQuery('nav').hasClass('mobile')) {
            jQuery('nav').addClass('mobile');
        }
    } else {
        if (jQuery('nav').hasClass('mobile')) {
            jQuery('nav').removeClass('mobile');
        }
    }
	return vp;
}

jQuery(document).ready( function() {
    checkViewport();
});
jQuery(window).smartresize( function() {
    checkViewport();
});

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Navigation Menu
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function Navigation() {
    var menuFixed = false,  //Menu fixed / unfixed
	menuQueued = false, // default menu state
    previousScroll = 0, // previous scroll position
    menuOffset = 5,   // height of menu (once scroll passed it, menu is hidden)
    detachPoint = 5,  // point of detach (after scroll passed it, menu is fixed)
    hideShowOffset = vp.detach; // scrolling value after which triggers hide/show menu

    this.show = showNav;
    this.hide = hideNav;
    this.queue = queueMenu;
    this.toggle = showHideNav;
    this.fixed = fixMenu;
	this.report = report;
	this.watchScroll = hideShowDetached;

	function report() {
		return 'menuFixed: ' + menuFixed;
	}

    function hideShowDetached() {
        // On scroll hide/show menu
        if (!jQuery('nav').hasClass('expanded')) {
            var currentScroll = jQuery(this).scrollTop(), // gets current scroll position
                scrollDifference = Math.abs(currentScroll - previousScroll); // calculates how fast user is scrolling
            // if scrolled past menu
            if (currentScroll > menuOffset) {
                // if scrolled past detach point add class to fix menu
                if (currentScroll > detachPoint) {
                    if (!jQuery('nav').hasClass('mobile')) {
                        if (!jQuery('nav').hasClass('detached')) {
                            jQuery('nav').addClass('detached');
                            jQuery('.site').addClass('detached');
                        }
                    }
                }
                // if scrolling faster than hideShowOffset hide/show menu
                if (currentScroll >= hideShowOffset) {
                    if (scrollDifference >= hideShowOffset) {
                        if (currentScroll > previousScroll) {
                            // scrolling down; hide menu
                            if (!jQuery('nav').hasClass('invisible')) {
                            }
                        } else {
                            // scrolling up; show menu
                            if (jQuery('nav').hasClass('invisible')) {
                                jQuery('nav').removeClass('invisible');
                            }
                            // if scrolled past point to fade Masthead
                        }
                    }
                }
            } else {
                // only remove “detached” class if user is at the top of document (menu jump fix)
                if (currentScroll <= 0) {
                    if (!detectIE()) {
                        jQuery('nav').removeClass('detached');
                        jQuery('.site').removeClass('detached');
                    }
                }
            }
            if (currentScroll < detachPoint ) {
                if (jQuery('nav').hasClass('invisible')) {
                    jQuery('nav').removeClass('invisible');
                }
            }
            // if user is at the bottom of document show menu
            if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                jQuery('nav').removeClass('invisible');
            }
            // replace previous scroll position with new one
            previousScroll = currentScroll;
        }
    }

    function animateOnScroll() {
        jQuery(window).scroll(hideShowDetached)
    }
    requestAnimationFrame(animateOnScroll);

    // shows/hides navigation’s popover if class "expanded"
    jQuery('.menu-button').on('click touchstart', function(event) {
        showHideNav();
        //event.preventDefault();
    });

    // clicking anywhere inside navigation or heading won’t close navigation’s popover
    jQuery('#navigation').on('click touchstart', function(event) {
        //event.stopPropagation();
    });

    function queueMenu() {
        setTimeout(function(){
            menuQueued = false;
        }, 500);
        return menuQueued;
    }

    function fixMenu(state) {
        menuFixed = state;
		if (menuFixed === true) {
			if (jQuery('nav').hasClass('fixed') === false) {
				jQuery('nav').addClass('fixed');
			}
		}
		if (menuFixed === false) {
			if (jQuery('nav').hasClass('fixed')) {
				jQuery('nav').removeClass('fixed');
				hideShowDetached();
			}
		}
        return menuFixed;
    }


    // checks if navigation’s popover is shown
    function showHideNav() {
        if (menuQueued === false) {
            if (jQuery('nav').hasClass('expanded')) {
                hideNav();
                queueMenu();
            } else {
                showNav();
                queueMenu();
            }
        }
    }

    // shows the navigation’s popover
    function showNav() {
        menuQueued = true;
        jQuery('nav').removeClass('invisible').addClass('expanded');
        jQuery('#mobile-menu a').attr('tabindex', '').removeClass('invisible'); // links inside navigation should be TAB selectable
        jQuery('.menu-button').addClass('open-nav');
        jQuery('.menu-button span').addClass('popover-invert');
        jQuery('.menu-background').css('display', 'block');
        jQuery('body').addClass('fixed');
    }

    // hides the navigation’s popover
    function hideNav() {
        menuQueued = true;
        jQuery('.menu-button span').removeClass('popover-invert');
        jQuery('#navigation a').attr('tabindex', '-1'); // links inside hidden navigation should not be TAB selectable
        jQuery('.icon').blur(); // deselect icon when navigation is hidden
        jQuery('.menu-button').removeClass('open-nav');
		jQuery('.menu-background').css('display', 'none');
		jQuery('nav').removeClass('expanded');
        jQuery('body').removeClass('fixed');
    }

}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Mobile Social Menu
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jQuery( document ).ready(function() {
    var socialMenu = jQuery('#social-wrapper-mobile');
    jQuery(socialMenu).detach();
    jQuery(socialMenu).appendTo('#menu-navigation');
});


// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Smoothly scroll to anchors
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function scrollToId(o) {
    jQuery('a[href*=#' + o + ']').click(function() {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
}

jQuery( document ).ready(function() {
    jQuery('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Login
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function toggleLogin() {
	if (!jQuery('#loginform').hasClass('invisible')) {
		jQuery('#loginform').addClass('invisible');
	} else {
		jQuery('#loginform').removeClass('invisible');
	}
}

jQuery( document ).ready(function() {
//    var menu = new Navigation(false);
//    menu.fixed(false);
//    menu.watchScroll();

	jQuery('a.login-toggle').click(function(){
		if ( $('.menu-button').hasClass('open-nav') ) {
			menu.toggle();
		}
		toggleLogin();
    });
    jQuery('#login-bg').focus(function(){
		if ( $('.menu-button').hasClass('open-nav') ) {
			menu.toggle();
		}
		toggleLogin();
    });

	jQuery('a[href*=#login-toggle]').click(function(){
		if ( $('.menu-button').hasClass('open-nav') ) {
			menu.toggle();
		}
        if ( $('.flickity-slider .carousel-cell:first-child').hasClass('is-selected') ) {
             toggleLogin();
        }

    });
    jQuery('#login-bg').click(function(){
		if ( $('.menu-button').hasClass('open-nav') ) {
			menu.toggle();
		}
		toggleLogin();
    });
});

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Forms - Dropdown
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jQuery( document ).ready(function() {
    jQuery('.bs-dropdown').click(function(){
        var listContainer = jQuery(this).find('.bs-select-list');
        var list = jQuery(listContainer).find('ul');
        var itemHeight = jQuery(list).height();
        var children = jQuery(list).find('li').length;
        var selectedSpan = jQuery(this).find('span.bs-selected-item');
        var dummySelect =  jQuery(this).find( 'select' )
        var selectedValue = jQuery(dummySelect).val();
        var selectedText = jQuery(dummySelect).val();

        // Expand / Collapse Dropdown Menu
        if (!jQuery(this).hasClass('bs-active')) {
            if (!jQuery(this).hasClass('disabled')) {
                jQuery(this).addClass('bs-active');
                jQuery(this).css('height', (itemHeight * (children)) + (itemHeight * 0.3) );
                jQuery(list).css('marginTop', itemHeight * -1 );
            }
        } else {
            this.style.height = null;
            this.style.height = null;
            jQuery(this).removeClass('bs-active');
        }
        if (!jQuery(listContainer).hasClass('invisible')) {
            jQuery(listContainer).addClass('invisible');
        } else {
            jQuery(listContainer).removeClass('invisible');
        }
        // Listen for selection, and update values
        jQuery(this).find('li').click(function(){
            selectedValue = jQuery(this).attr('data-value');
            selectedText = jQuery(this).text();
            jQuery(dummySelect).val(selectedValue);
            jQuery(selectedSpan).text(selectedText);
            if (!jQuery(selectedSpan).hasClass('non-default')) {
                jQuery(selectedSpan).addClass('non-default');
            }
        });

    });
});

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Signup
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jQuery( document ).ready(function() {
    jQuery('#sameAddress').click(function(){
        if ( jQuery( this ).prop( "checked" ) ) {

            // Assign Vars
            var s4_address1 = jQuery('#signup_s3_address1').val();
            var s4_address2 = jQuery('#signup_s3_address2').val();
            var s4_city = jQuery('#signup_s3_city').val();
            var s4_state = jQuery('#signup_s3_state').val();
            var s4_state_txt =  jQuery("#bs-dropdown-billing").text();
            var s4_zip = jQuery('#signup_s3_zip').val();
            var s4_country = jQuery('#signup_s3_country').val();

            // Replace
            jQuery('#signup_s4_address1').val(s4_address1).prop('disabled', true);
            jQuery('#signup_s4_address2').val(s4_address2).prop('disabled', true);
            jQuery('#signup_s4_city').val(s4_city).prop('disabled', true);
            jQuery('#signup_s4_state').val(s4_state).prop('disabled', true);
            jQuery("#bs-dropdown-shipping").text(s4_state_txt).addClass('disabled');
            jQuery('#signup_s4_zip').val(s4_zip).prop('disabled', true);
            jQuery('#signup_s4_country').val(s4_country).prop('disabled', true);
        } else {
            jQuery('#signup_s4_address1').val('').prop('disabled', false);
            jQuery('#signup_s4_address2').val('').prop('disabled', false);
            jQuery('#signup_s4_city').val('').prop('disabled', false);
            jQuery('#signup_s4_state').val('').prop('disabled', false);
            jQuery("#bs-dropdown-shipping").text('Please select an option').removeClass('disabled');
            jQuery('#signup_s4_zip').val('').prop('disabled', false);
            jQuery('#signup_s4_country').val('').prop('disabled', false);
        }
    });
});

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Flickity Navigation Color Changing Based on Background Image
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
var $carousel;
var flkty ;

jQuery(document).ready( function() {

    function getVideoHeight() {
        jQuery('.carousel').find('video').each( function( i, video ) {
          var videoHeight = jQuery( video ).css('height');
          //console.log(videoHeight);
         // jQuery( 'video' ).css('height', videoHeight);
          //jQuery( 'video' ).parent().css('height', videoHeight);
        //      jQuery( video ).parent().css('height', videoHeight);
        });
    }

	var $carousel = jQuery('.carousel').flickity({ "wrapAround": true, "autoPlay": 4000, "prevNextButtons": false, "adaptiveHeight": true   });
	var flkty = $carousel.data('flickity');
    //getVideoHeight();
	$carousel.on( 'settle', function() {
        $carousel.find('video').each( function( i, video ) {
            video.play();
            jQuery( video ).on( 'loadeddata', onLoadeddata );
        });
	});

    function onLoadeddata( event ) {
        console.log('loaded');
        var cell = $carousel.flickity( 'getParentCell', event.target );
        $carousel.flickity( 'cellSizeChange', cell && cell.element );
     }

    $carousel.find('video').each( function( i, video ) {
        video.play();
        jQuery( video ).on( 'loadeddata', onLoadeddata );
    });
    //jQuery( video ).on( 'loadeddata', onLoadeddata );
});
jQuery(window).load( function() {
//    jQuery( video ).on( 'loadeddata', onLoadeddata );
});
jQuery(window).smartresize( function() {
    //jQuery( video ).on( 'loadeddata', onLoadeddata );
});

*/

jQuery(document).ready( function() {
	var $carousel = jQuery('.carousel').flickity({ "wrapAround": true, "autoPlay": 4000, "prevNextButtons": false   });
	var flkty = $carousel.data('flickity');

    var $staff = jQuery('ul.staff-modal').flickity({ "wrapAround": true, "autoPlay": false, "prevNextButtons": true, arrowShape: { x0: 10, x1: 55, y1: 45, x2: 60, y2: 40, x3: 20 }  });

    $('ul.staff').on( 'click', 'li', function() {
      var index = $(this).index();
      $staff.flickity( 'select', index, false, true );
        if ( jQuery('aside.staff-modal').hasClass('invisible') ) {
            jQuery('aside.staff-modal').removeClass('invisible');
        }
    });


});
jQuery(window).smartresize( function() {
});

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SelectFX - Form Animation
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * selectFx.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
;( function( window ) {

	'use strict';

	/**
	 * based on from https://github.com/inuyaksa/jquery.nicescroll/blob/master/jquery.nicescroll.js
	 */
	function hasParent( e, p ) {
		if (!e) return false;
		var el = e.target||e.srcElement||e||false;
		while (el && el != p) {
			el = el.parentNode||false;
		}
		return (el!==false);
	};

	/**
	 * extend obj function
	 */
	function extend( a, b ) {
		for( var key in b ) {
			if( b.hasOwnProperty( key ) ) {
				a[key] = b[key];
			}
		}
		return a;
	}

	/**
	 * SelectFx function
	 */
	function SelectFx( el, options ) {
		this.el = el;
		this.options = extend( {}, this.options );
		extend( this.options, options );
		this._init();
	}

	/**
	 * SelectFx options
	 */
	SelectFx.prototype.options = {
		// if true all the links will open in a new tab.
		// if we want to be redirected when we click an option, we need to define a data-link attr on the option of the native select element
		newTab : true,
		// when opening the select element, the default placeholder (if any) is shown
		stickyPlaceholder : true,
		// callback when changing the value
		onChange : function( val ) { return false; }
	}

	/**
	 * init function
	 * initialize and cache some vars
	 */
	SelectFx.prototype._init = function() {
		// check if we are using a placeholder for the native select box
		// we assume the placeholder is disabled and selected by default
		var selectedOpt = this.el.querySelector( 'option[selected]' );
		this.hasDefaultPlaceholder = selectedOpt && selectedOpt.disabled;

		// get selected option (either the first option with attr selected or just the first option)
		this.selectedOpt = selectedOpt || this.el.querySelector( 'option' );

		// create structure
		this._createSelectEl();

		// all options
		this.selOpts = [].slice.call( this.selEl.querySelectorAll( 'li[data-option]' ) );

		// total options
		this.selOptsCount = this.selOpts.length;

		// current index
		this.current = this.selOpts.indexOf( this.selEl.querySelector( 'li.cs-selected' ) ) || -1;

		// placeholder elem
		this.selPlaceholder = this.selEl.querySelector( 'span.cs-placeholder' );

		// init events
		this._initEvents();
	}

	/**
	 * creates the structure for the select element
	 */
	SelectFx.prototype._createSelectEl = function() {
		var self = this, options = '', createOptionHTML = function(el) {
			var optclass = '', classes = '', link = '';

			if( el.selectedOpt && !this.foundSelected && !this.hasDefaultPlaceholder ) {
				classes += 'cs-selected ';
				this.foundSelected = true;
			}
			// extra classes
			if( el.getAttribute( 'data-class' ) ) {
				classes += el.getAttribute( 'data-class' );
			}
			// link options
			if( el.getAttribute( 'data-link' ) ) {
				link = 'data-link=' + el.getAttribute( 'data-link' );
			}

			if( classes !== '' ) {
				optclass = 'class="' + classes + '" ';
			}

			return '<li ' + optclass + link + ' data-option data-value="' + el.value + '"><span>' + el.textContent + '</span></li>';
		};

		[].slice.call( this.el.children ).forEach( function(el) {
			if( el.disabled ) { return; }

			var tag = el.tagName.toLowerCase();

			if( tag === 'option' ) {
				options += createOptionHTML(el);
			}
			else if( tag === 'optgroup' ) {
				options += '<li class="cs-optgroup"><span>' + el.label + '</span><ul>';
				[].slice.call( el.children ).forEach( function(opt) {
					options += createOptionHTML(opt);
				} );
				options += '</ul></li>';
			}
		} );

		var opts_el = '<div class="cs-options"><ul>' + options + '</ul></div>';
		this.selEl = document.createElement( 'div' );
		this.selEl.className = this.el.className;
		this.selEl.tabIndex = this.el.tabIndex;
		this.selEl.innerHTML = '<span class="cs-placeholder">' + this.selectedOpt.textContent + '</span>' + opts_el;
		this.el.parentNode.appendChild( this.selEl );
		this.selEl.appendChild( this.el );
	}

	/**
	 * initialize the events
	 */
	SelectFx.prototype._initEvents = function() {
		var self = this;

		// open/close select
		this.selPlaceholder.addEventListener( 'click', function() {
			self._toggleSelect();
		} );

		// clicking the options
		this.selOpts.forEach( function(opt, idx) {
			opt.addEventListener( 'click', function() {
				self.current = idx;
				self._changeOption();
				// close select elem
				self._toggleSelect();
			} );
		} );

		// close the select element if the target it´s not the select element or one of its descendants..
		document.addEventListener( 'click', function(ev) {
			var target = ev.target;
			if( self._isOpen() && target !== self.selEl && !hasParent( target, self.selEl ) ) {
				self._toggleSelect();
			}
		} );

		// keyboard navigation events
		this.selEl.addEventListener( 'keydown', function( ev ) {
			var keyCode = ev.keyCode || ev.which;

			switch (keyCode) {
				// up key
				case 38:
					ev.preventDefault();
					self._navigateOpts('prev');
					break;
				// down key
				case 40:
					ev.preventDefault();
					self._navigateOpts('next');
					break;
				// space key
				case 32:
					ev.preventDefault();
					if( self._isOpen() && typeof self.preSelCurrent != 'undefined' && self.preSelCurrent !== -1 ) {
						self._changeOption();
					}
					self._toggleSelect();
					break;
				// enter key
				case 13:
					ev.preventDefault();
					if( self._isOpen() && typeof self.preSelCurrent != 'undefined' && self.preSelCurrent !== -1 ) {
						self._changeOption();
						self._toggleSelect();
					}
					break;
				// esc key
				case 27:
					ev.preventDefault();
					if( self._isOpen() ) {
						self._toggleSelect();
					}
					break;
			}
		} );
	}

	/**
	 * navigate with up/dpwn keys
	 */
	SelectFx.prototype._navigateOpts = function(dir) {
		if( !this._isOpen() ) {
			this._toggleSelect();
		}

		var tmpcurrent = typeof this.preSelCurrent != 'undefined' && this.preSelCurrent !== -1 ? this.preSelCurrent : this.current;

		if( dir === 'prev' && tmpcurrent > 0 || dir === 'next' && tmpcurrent < this.selOptsCount - 1 ) {
			// save pre selected current - if we click on option, or press enter, or press space this is going to be the index of the current option
			this.preSelCurrent = dir === 'next' ? tmpcurrent + 1 : tmpcurrent - 1;
			// remove focus class if any..
			this._removeFocus();
			// add class focus - track which option we are navigating
			classie.add( this.selOpts[this.preSelCurrent], 'cs-focus' );
		}
	}

	/**
	 * open/close select
	 * when opened show the default placeholder if any
	 */
	SelectFx.prototype._toggleSelect = function() {
		// remove focus class if any..
		this._removeFocus();

		if( this._isOpen() ) {
			if( this.current !== -1 ) {
				// update placeholder text
				this.selPlaceholder.textContent = this.selOpts[ this.current ].textContent;
			}
			classie.remove( this.selEl, 'cs-active' );
		}
		else {
			if( this.hasDefaultPlaceholder && this.options.stickyPlaceholder ) {
				// everytime we open we wanna see the default placeholder text
				this.selPlaceholder.textContent = this.selectedOpt.textContent;
			}
			classie.add( this.selEl, 'cs-active' );
		}
	}

	/**
	 * change option - the new value is set
	 */
	SelectFx.prototype._changeOption = function() {
		// if pre selected current (if we navigate with the keyboard)...
		if( typeof this.preSelCurrent != 'undefined' && this.preSelCurrent !== -1 ) {
			this.current = this.preSelCurrent;
			this.preSelCurrent = -1;
		}

		// current option
		var opt = this.selOpts[ this.current ];

		// update current selected value
		this.selPlaceholder.textContent = opt.textContent;

		// change native select element´s value
		this.el.value = opt.getAttribute( 'data-value' );

		// remove class cs-selected from old selected option and add it to current selected option
		var oldOpt = this.selEl.querySelector( 'li.cs-selected' );
		if( oldOpt ) {
			classie.remove( oldOpt, 'cs-selected' );
		}
		classie.add( opt, 'cs-selected' );

		// if there´s a link defined
		if( opt.getAttribute( 'data-link' ) ) {
			// open in new tab?
			if( this.options.newTab ) {
				window.open( opt.getAttribute( 'data-link' ), '_blank' );
			}
			else {
				window.location = opt.getAttribute( 'data-link' );
			}
		}

		// callback
		this.options.onChange( this.el.value );
	}

	/**
	 * returns true if select element is opened
	 */
	SelectFx.prototype._isOpen = function(opt) {
		return classie.has( this.selEl, 'cs-active' );
	}

	/**
	 * removes the focus class from the option
	 */
	SelectFx.prototype._removeFocus = function(opt) {
		var focusEl = this.selEl.querySelector( 'li.cs-focus' )
		if( focusEl ) {
			classie.remove( focusEl, 'cs-focus' );
		}
	}

	/**
	 * add to global namespace
	 */
	window.SelectFx = SelectFx;

} )( window );

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Skip-link-focus-fix
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * skip-link-focus-fix.js
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
( function() {
	var is_webkit = navigator.userAgent.toLowerCase().indexOf( 'webkit' ) > -1,
	    is_opera  = navigator.userAgent.toLowerCase().indexOf( 'opera' )  > -1,
	    is_ie     = navigator.userAgent.toLowerCase().indexOf( 'msie' )   > -1;

	if ( ( is_webkit || is_opera || is_ie ) && document.getElementById && window.addEventListener ) {
		window.addEventListener( 'hashchange', function() {
			var id = location.hash.substring( 1 ),
				element;

			if ( ! ( /^[A-z0-9_-]+$/.test( id ) ) ) {
				return;
			}

			element = document.getElementById( id );

			if ( element ) {
				if ( ! ( /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) ) {
					element.tabIndex = -1;
				}

				element.focus();
			}
		}, false );
	}
})();

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Modal Popups
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	jQuery(window).on("load", function(){
        if ( jQuery('.cookie-modal.pop').hasClass('invisible') ) {
        //    if ( jQuery('.modal-popup').hasClass('subscribe-form') === false && jQuery('.modal-popup').hasClass('loginform') === false ) {
                jQuery('.cookie-modal.pop').delay(500).removeClass('invisible');
        //    }
        }
	});


// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Mailchimp Integration
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jQuery( document ).ready(function() {

	jQuery('.mc-embedded-subscribe').on('click', function(event) {
		event.preventDefault();
		if(validate_input($(this).parents('.mc-embedded-subscribe-form'))){
			register($(this).parents('.mc-embedded-subscribe-form'));
		}
	});

	function validate_input($form) {
		if ($form.find('input').val() == "") return false;
		else return true;
	}

	function register($form) {
		jQuery.ajax({
			type: $form.attr('method'),
			url: $form.attr('action'),
			data: $form.serialize(),
			cache: false,
			dataType: 'jsonp',
			jsonp: 'c',
			contentType: "application/json; charset=utf-8",
			error: function(err) {
			    alert("Could not connect to the server. Please try again later.");
                jQuery('.mc-response aside').html('<h4>Error</h4><p>Could not connect to the server. Please try again later.</p>');
                console.log('Mailchimp: Failure - Could not connect to the server.');
                if (jQuery('.mc-response').hasClass('invisible')) {
                    jQuery('.mc-response').removeClass('invisible');
                }
                jQuery('*').on('click', function() {
                    if (!jQuery('.mc-response').hasClass('invisible')) {
                        jQuery('.mc-response').addClass('invisible');
                    }
    			});
			},
			success: function(data) {
                jQuery('.mc-response aside').html('<h4>Thanks!</h4><p>Please check your inbox to complete the signup process.</p>');
                if (jQuery('.mc-response').hasClass('invisible')) {
                    jQuery('.mc-response').removeClass('invisible');
                }
                jQuery('*').on('click', function() {
                    if (!jQuery('.mc-response').hasClass('invisible')) {
                        jQuery('.mc-response').addClass('invisible');
                    }
                    if (!jQuery('.modal-popup').hasClass('invisible')) {
                        jQuery('.modal-popup').addClass('invisible');
                    }

    			});
			}
		});
	}
    jQuery('.modal-popup .remove').on('click', function(event) {
		event.preventDefault();
        if (!jQuery('.modal-popup').hasClass('invisible')) {
            jQuery('.modal-popup').addClass('invisible');
        }
	});
    jQuery('.modal-bg').on('click', function(event) {
		event.preventDefault();
        if (!jQuery('.modal-popup').hasClass('invisible')) {
            jQuery('.modal-popup').addClass('invisible');
        }
	});
});


// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Comments Show/Hide
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function setCalculatedHeight(element, toggleClass, isChild) {

    if (isChild === true) {
        var parent = jQuery(element).parent();
        if (jQuery(parent).hasClass(toggleClass)) {
            jQuery(parent).removeClass(toggleClass);
            jQuery(element).css('height', 'auto' );
            var h = jQuery(element).height();
            jQuery(element).css('height', h );
            jQuery(parent).addClass(toggleClass);
        } else {
            var h = jQuery(element).height();
            jQuery(element).css('height', h );
        }
    } else {
        jQuery(element).removeClass(toggleClass);
        jQuery(element).css('height', 'auto' );
        var h = jQuery(element).height();
        jQuery(element).css('height', h );
        jQuery(element).addClass(toggleClass);
    }
    //console.log('setCalculatedHeight - Element: ' + element + ' Height: ' + h + 'px ' + 'isChild: ' + isChild);
    return h;
}

function initComments( element, toggleClass, children, init ) {
    var status;
    if (init === true) {
        // First time
        if (toggleClass !== false) {
            jQuery(element).addClass(toggleClass);
        }
        if (jQuery(element).hasClass('init')) {
            jQuery(element).removeClass('init');
        }
        if (!jQuery(element).hasClass('no-comments')) {
            status = 'closed';
        } else {
            status = 'open';
        }
    } else {
        // Update on resize - Check if the comments were open or closed before resize
        if ( jQuery(element).hasClass(toggleClass) === true ) {
            status = 'closed';
        } else {
            status = 'open';
        }
    }
    if (children === true) {
        for (i = 0; i < children.length; i++) {
            setCalculatedHeight( children[i], toggleClass );
            //console.log('initComments - Element: ' + children[i] + ' Height: ' + setCalculatedHeight(children[i], toggleClass, true ));
        }
    } else {
        setCalculatedHeight( element, toggleClass, false );
        //console.log('initComments - Element: ' + element + ' Height: ' + setCalculatedHeight(element, toggleClass, false));
    }

    // Return to previous state
    if ( status === 'closed' ) {
        if (!jQuery(element).hasClass('minimized')) {
            jQuery(element).addClass('minimized');
        }
    } else {
        if (jQuery(element).hasClass('minimized')) {
            jQuery(element).removeClass('minimized');
        }
    }

}

function toggleComments() {
    jQuery('#comment-toggle').on('click touchstart', function(event) {
        console.log('fired');
        event.preventDefault();
        if (jQuery('#comments').hasClass('minimized')) {
            jQuery('#comments').removeClass('minimized').show(0);
        } else {
            jQuery('#comments').addClass('minimized').delay(1000).hide(0);
        }
        if (jQuery('.hide-comments').hasClass('hidden')) {
            jQuery('.hide-comments').removeClass('hidden');
            jQuery('.hide-comment-count').addClass('hidden');
        } else {
            jQuery('.hide-comments').addClass('hidden');
            jQuery('.hide-comment-count').removeClass('hidden');
        }
    });

}

jQuery( document ).ready(function() {
    initComments('#comments', 'minimized', [".comment-list", ".comment-respond"], true);
    toggleComments();
});
jQuery(window).smartresize( function() {
    initComments('#comments', 'minimized', [".comment-list", ".comment-respond"], false);
});

/*
jQuery( document ).ready(function() {
    jQuery('#comments').fadeOut('100');
    jQuery('#comments').addClass("is-minimized");
});
jQuery('.comments-link a').on("click touchstart", function(){
    if ( jQuery('#comments').hasClass("is-minimized") ) {
        jQuery('#comments').fadeIn("slow", function() {
            jQuery('#comments').removeClass("is-minimized");
            console.log('fade-in');
        });
    } else {
        jQuery('#comments').fadeOut("slow", function() {
            jQuery('#comments').addClass("is-minimized");
            scrollToId('comments');
            console.log('fade-out');
        });
    }
});
*/
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Subscribe
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jQuery('.subscribe').on("click touchstart", function(){
    if (jQuery('.modal-popup.subscribe-form').hasClass('invisible')) {
        jQuery('.modal-popup.subscribe-form').delay(500).removeClass('invisible');
    }
});

jQuery('.modal-popup .remove').on('click touchstart', function(event) {
	event.preventDefault();
    if (!jQuery(this).parents('.modal-popup').hasClass('invisible')) {
        jQuery(this).parents('.modal-popup').addClass('invisible');
    }
});
jQuery('.modal-bg').on('click touchstart', function(event) {
	event.preventDefault();
    if (jQuery('.modal-popup').hasClass('invisible')) {
        jQuery('.modal-popup').addClass('invisible');
    }
});

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Search Button
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jQuery( document ).ready(function() {
    jQuery('.fa-search').on("click touchend", function(){
        if ( !jQuery('nav').hasClass('mobile') && jQuery('nav').hasClass('expanded') ) {
            //menu.toggle();
        }
        if (jQuery('.search-entry').hasClass('invisible')) {
            jQuery('.search-entry').removeClass('invisible');
        }
    });

    $('input.search-field').val('').attr('placeholder','Enter your search here...');

    jQuery('.search-bg').on('click touchend', function(event) {
    	//event.preventDefault();
        if (!jQuery('.search-popup').hasClass('invisible')) {
            jQuery('.search-entry').addClass('invisible');
        }
    });

});

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Menu Helper Functions
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var menu = new Navigation(false);
menu.fixed(false);
menu.watchScroll();

function resizeMenuLinks() {
    if ( jQuery('nav').hasClass('mobile') ) {
        var h = jQuery(window).height();
        var count = jQuery('#menu-navigation').children();
        var count = count.length;
        var newFontSize = (h / 4) / count;
        var newLineHeight = newFontSize * 2.8 + 'px';
        var el = jQuery('#menu-navigation li a');
            el = el[0];
        var computedFontSize = getComputedStyle(el).getPropertyValue("font-size");
        var computedLineHeight = getComputedStyle(el).getPropertyValue("line-height");
        jQuery('#menu-navigation li a').css('fontSize', newFontSize)
        jQuery('#menu-navigation li a').css('lineHeight', newLineHeight)
    } else {
        jQuery('#menu-navigation li a').css('fontSize', '')
        jQuery('#menu-navigation li a').css('lineHeight', '')
    }
}


jQuery(window).smartresize( function() {

    // Close mobile menu if the window is resized past the minimum breakpoint
    if ( !jQuery('nav').hasClass('mobile') && jQuery('nav').hasClass('expanded') ) {
        menu.toggle();
    }
//    setTimeout(resizeMenuLinks, 1000);
    resizeMenuLinks();
});

jQuery( document ).ready(function() {
    resizeMenuLinks();
});


// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Video Slides
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var tag = document.createElement('script');

tag.src = "//www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('ytembed', {
        events: {
            'onReady': onPlayerReady
        }
    });
}

function onPlayerReady() {
    player.playVideo();
    // Mute!
    player.mute();
}

var monitor = setInterval(function(){
    var elem = document.activeElement;
    if(elem && elem.tagName == 'IFRAME'){
        $(elem).parent().removeClass('overlay-active');
        $(elem).blur();
        //clearInterval(monitor);
    }
}, 500);


// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Content Aware Spacing
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
function resizeContentAwareItems(el) {
    if ( jQuery(el) {
        var containerWidth = jQuery('.ca').width();
        var children = jQuery('.ca').children();
            childCount = children.length;
            console.log(childCount);
            jQuery(children).css('width','auto');

        var computedFontSize = getComputedStyle(el).getPropertyValue("font-size");
        var computedLineHeight = getComputedStyle(el).getPropertyValue("line-height");
        jQuery('#menu-navigation li a').css('fontSize', newFontSize)
        jQuery('#menu-navigation li a').css('lineHeight', newLineHeight)
    }
}

jQuery( document ).ready(function() {
    resizeContentAwareItems('.ca');
});

jQuery(window).smartresize( function() {
    resizeContentAwareItems('.ca');
});
*/

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Attend Page
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
jQuery('.day button').on('click touchstart', function(event) {
    var expanded = jQuery(this).parent();
    var show = jQuery(expanded).parent();
    jQuery(show).find('.day').addClass('minimized');
    jQuery(expanded).addClass('expanded');
    if (jQuery(expanded).hasClass('minimized')) {
        jQuery(expanded).removeClass('minimized');
    }
    jQuery(show).addClass('panel-expanded');
    //console.log(show);
});
*/

function displayShowInfo() {
    var selectedShow = $(this).parent();
    var showId = $(this).data("id");
    $('.day-list .is-selected').fadeOut('fast', function() {
        $('.shows-container .is-selected').removeClass('is-selected');
        $(selectedShow).addClass('is-selected');
        $('.day-list .is-selected').removeClass('is-selected');
        $( '#show-' + showId ).addClass('is-selected').fadeIn('fast');
    });
}


jQuery(window).smartresize( function() {
    if ($('.shows-container .shows').length < 1 ) {
        $('.shows-container .shows').first().addClass('is-selected');
    }});

// Equalize height
var maxHeight = 0;
jQuery( document ).ready(function() {
    $("article.description").css('height','auto');
    $("article.description").each(function(){
       if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    });
    $("article.description").height(maxHeight);
});

jQuery(window).smartresize( function() {
    $("article.description").css('height','');
    $("article.description").each(function(){
       if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    });
    $("article.description").height(maxHeight);
});

    $( document ).ready(function() {
    	$('.day button').on('click touchstart', function(e) {
    		e.preventDefault();
    		$(this).fadeOut('fast', function() {
    			$(this).parent().find('section.blocks').fadeIn('fast');
    		});
    	});
    });

    $( document ).ready(function() {
        // Change shows on click when not on a mobile device
        $('#attend .shows-container a').on('click touchstart', function(e) {
            e.preventDefault();
            if ( jQuery(window).width() > 600 ) {
                var selectedShow = $(this).parent();
                var showId = $(this).data("id");
                $('.day-list .is-selected').fadeOut('fast', function() {
                    $('.shows-container .is-selected').removeClass('is-selected');
                    $(selectedShow).addClass('is-selected');
                    $('.day-list .is-selected').removeClass('is-selected');
                    $( '#show-' + showId ).addClass('is-selected').fadeIn('fast');
                });
            }
        });
        // Use a carousel to change shows on click when on a mobile device
        var $attendCarousel = jQuery('#attend .shows-container').flickity({ "pageDots": false, "wrapAround": true, "autoPlay": false, "watchCSS": true });
        $attendCarousel.on( 'select.flickity', function() {
            var selectedShow = $('.shows-container .show.is-selected');
            var showId = $(selectedShow).data("id");
            $('.day-list .is-selected').fadeOut('fast', function() {
                $('.shows-container .is-selected').removeClass('is-selected');
                $(selectedShow).addClass('is-selected');
                $('.day-list .is-selected').removeClass('is-selected');
                $( '#show-' + showId ).addClass('is-selected').fadeIn('fast');
            });
        })
    });

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Attend Newsletter Buttons
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    jQuery('.day-list .newsletter').on("click touchstart", function(){
        if (jQuery('.modal-popup.cookie-modal').hasClass('invisible')) {
            jQuery('.modal-popup.cookie-modal').delay(250).removeClass('invisible');
        }
    });
