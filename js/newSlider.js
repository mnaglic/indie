jQuery(document).ready(function($) {
	$('.new-slider').slick({
	    dots: true,
	    infinite: true,
	    speed: 300,
		fade: false,
		arrows: false,
		cssEase: 'linear',
		appendDots: $('.slick-wrapper'),
		adaptiveHeight: true,
  	});
});