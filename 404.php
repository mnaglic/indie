<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package iBE_2016
 */

get_header(); ?>
	<div id="primary" class="content-area">
			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Sorry, the page you are looking for can&rsquo;t be found', 'ibe_' ); ?></h1>
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'ibe_' ); ?></p>
				</header><!-- .page-header -->


					<?php
						get_search_form();
						$view = rwmb_meta( 'ibe_default_landing', '', '133');
						$loop = new WP_Query( 'post_type=landing&p='.$view );
						while ( $loop->have_posts() ) : $loop->the_post();
							get_template_part( 'templates/partials/section-recent-posts', 'none' );
						endwhile;
					?>
			</section><!-- .error-404 -->

	</div><!-- #primary -->

<?php get_footer();
