<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package iBE_2016
 */
?>

<?php

get_header();

$loop = new WP_Query( 'post_type=landing&p='.$post->ID );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<div id="home" class="page landing-<?php echo $view; ?>">
    <div class="container">
        <?php get_template_part( 'templates/partials/header-carousel', 'none' ); ?>
        <?php get_template_part( 'templates/partials/section-featured-content', 'none' ); ?>
    </div>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
