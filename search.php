<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package iBE_2016
 */

get_header(); ?>

<div id="search-results" class="page">
    <div class="container">

		<?php
        //query_posts('cat=-3');
		if ( have_posts() ) : ?>
			<?php $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1; ?>
			<header class="page-header">
				<h2><?php printf( esc_html__( 'Search Results for: %s', 'ibe_' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
			</header><!-- .page-header -->
			<section class="search-items">
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

			get_template_part( 'templates/partials/content', 'search' );

			endwhile; ?>

			<nav id="pagination">
				<?php //numeric_posts_nav(); ?>
				<?php the_posts_navigation(); ?>
			</nav>

		<?php else : ?>

            <header class="page-header">
                <h2><?php printf( esc_html__( 'Search Results for: %s', 'ibe_' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
            </header><!-- .page-header -->
            <main style="text-align:center;">
                <p>Sorry, unfortunately, nothing matched your search terms. Please try again with some different keywords.</p>
                <?php get_search_form();  ?>
            </main>
		<?php endif; ?>
		</section>
	</div>
</div>

<?php get_footer();
