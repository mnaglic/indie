<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package IBE_2016
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-social">
			<a class="footer-logo" href="<?php echo site_url();?>"></a>
			<p>For the latest on all things indie, join our newsletter and be the first in the know.</p>
			<div class="newsletter">
			    <form action="//Indiebeautyexpo.us9.list-manage.com/subscribe/post-json?u=f4a3d85a1388923b3fe3a6cb9&amp;id=4ed1cffcc8" method="post" name="mc-embedded-subscribe-form" class="mc-embedded-subscribe-form validate" novalidate>
			        <input type="email" placeholder="Enter email" name="EMAIL" class="required mc-embedded-email">
			        <input type="submit" value="Go" name="subscribe" class="mc-embedded-subscribe">
				</form>
			</div>
			<ul class="social-wrapper">
				<?php
					if ( get_option('ibe_social_facebook') !== '' && get_option('ibe_social_facebook') !== false ) {
						echo '<li class="fa fa-facebook-square"><a href="'.get_option('ibe_social_facebook').'"><span>Facebook</span></a></li>';
					}
					if ( get_option('ibe_social_instagram') !== '' && get_option('ibe_social_instagram') !== false ) {
						echo '<li class="fa fa-instagram"><a href="'.get_option('ibe_social_instagram').'"><span>Instagram</span></a></li>';
					}
					if ( get_option('ibe_social_twitter') !== '' && get_option('ibe_social_twitter') !== false ) {
						echo '<li class="fa fa-twitter"><a href="'.get_option('ibe_social_twitter').'"><span>Twitter</span></a></li>';
					}
					if ( get_option('ibe_social_youtube') !== '' && get_option('ibe_social_youtube') !== false ) {
						echo '<li class="fa fa-youtube-square"><a href="'.get_option('ibe_social_youtube').'"><span>Youtube</span></a></li>';
					}
					if ( get_option('ibe_social_pintrest') !== '' && get_option('ibe_social_pintrest') !== false ) {
						echo '<li class="fa fa-pintrest"><a href="'.get_option('ibe_social_pintrest').'"><span>Pintrest</span></a></li>';
					}
				?>
			</ul>
			<p class="tagline"><?php echo strtolower(get_option('blogdescription', 'Tagline')); ?></p>
		</div>
		<div class="bottom-line">
			<?php
				$ibe_telephone 	= get_option('ibe_telephone', '1-222-333-4444');
				$ibe_tel_split = explode( '-' , $ibe_telephone );
				$ibe_tel_prefix = $ibe_tel_split[1];
				$ibe_tel_rest 	= $ibe_tel_split[2] . '-' . $ibe_tel_split[3];
				echo '<address class="copyright">';
				echo '	<span>&copy; ' . date("Y") . ' ' . get_option('ibe_company_name', 'Company Name') . '&nbsp;</span>';
				echo '	<span>' . get_option('ibe_address_1', 'Address Line 1') . '</span>';
				echo '	<span>' . get_option('ibe_address_2', 'Address Line 2') . '</span>';
				echo '	<span>' . get_option('ibe_address_3', 'Address Line 3') . '</span>';
				echo '	<a href="tel:'.$ibe_telephone.'">('.$ibe_tel_prefix.') '.$ibe_tel_rest.'</a>';
				echo '</address>';

				$args = array(
					'theme_location' => 'footer_menu',
				//	'depth'      => 2,
				//	'container'  => false,
				//	'menu_class'     => 'nav navbar-nav navbar-right',
				//	'walker'     => new Bootstrap_Walker_Nav_Menu()
				);
				wp_nav_menu($args);
			?>
		</div>
		<?php get_template_part( 'templates/partials/section-attend-sidebar','none' );  ?>
	</footer><!-- #colophon -->
</div><!-- #page -->
<div class="invisible response mc-response">
	<aside></aside>
	<div class="response-bg"></div>
</div>
<?php
	echo '
		<div class="modal-popup invisible loginform" id="loginform">
			<div class="modal-bg"></div>
			<div class="modal-popup-content">
				<span class="remove">&times;</span>
				<div class="inner">
					<form name="loginform" action="'.get_site_url().'/wp-login.php" method="post">
						<h3>Please Sign In</h3>
						<div>
							<input type="text" name="log" id="log" value="" size="20" tabindex="1" placeholder="Email" />
						</div>

						<div>
							<input type="password" name="pwd" id="pwd" value="" size="20" tabindex="2" placeholder="Password" />
						</div>

						<input name="wp-submit" id="wp-submit" value="Log In" tabindex="100" type="submit" tabindex="3">

						<input name="redirect_to" value="'.$_SERVER['REQUEST_URI'].'" type="hidden">
						<input name="testcookie" value="1" type="hidden">
						<a href="'.get_site_url().'/wp-login.php?action=lostpassword" class="forgot-pass">Forgot your password?</a>
					</form>
				</div>
			</div>
		</div>
		';
 ?>
<?php get_template_part( 'templates/partials/section-modal', 'none' ); ?>
<?php get_template_part( 'templates/partials/section-modal-search', 'none' ); ?>
<?php get_template_part( 'templates/partials/section-subscribe', 'none' ); ?>
<script>var weburl = '<?= get_template_directory_uri();?>';</script>
<script src="<?= get_template_directory_uri();?>/js/ea.js?v=" . time()></script>
<script src="<?= get_template_directory_uri();?>/js/joinButton.js"></script>
<?php wp_footer(); ?>

</body>
</html>
