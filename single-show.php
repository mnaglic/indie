<?php
/**
 *
 *
 * @file           single-show.php
 * @package        iBE_2016
 * @author         Luke Ragno
 * @copyright      2016 Luke Ragno Design & Art Direction
 * @license        license.txt
 * @version        Release: 1.0
 */

 ?>
<?php
    get_header();
    $loop = new WP_Query( 'post_type=show&p='.$post->ID );
    while ( $loop->have_posts() ) : $loop->the_post();
    $show = $loop->posts;
    $show = $show[0];
    $attend_page = get_page_by_title( 'Attend' );
    $past_shows_enabled = rwmb_meta( 'ibe_pastshows_enabled','', $attend_page->ID );
    if ($past_shows_enabled) :
?>

 <div id="show" class="page">
     <div class="container">
 		<?php if ( have_posts() ) : ?>
            <?php include( locate_template( 'templates/partials/header-show.php', false, false ) );  ?>
            <main class="attend-main">
                <?php include( locate_template( 'templates/partials/section-past-show-days.php', false, false ) );  ?>
                <?php include( locate_template( 'templates/partials/section-show-brands.php', false, false ) );  ?>
                <?php include( locate_template( 'templates/partials/section-show-photobanner.php', false, false ) );  ?>
                <?php include( locate_template( 'templates/partials/section-past-show-videos.php', false, false ) );  ?>


            </main>
         <?php endif; ?>
     </div> <!-- /container -->
 </div><!-- /ww -->
<?php endif; ?>
 <?php endwhile; ?>
 <?php get_footer(); ?>
