<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package iBE_2016
 */

get_header();


$loop = new WP_Query( 'post_type=post&p='.$post->ID );
while ( $loop->have_posts() ) : $loop->the_post();
$post_customclass = rwmb_meta( 'ibe_post_customclass' );
$post_customcss = rwmb_meta( 'ibe_post_customcss' );
?>
<style>
<?php echo $post_customcss; ?>
</style>
<div id="news" class="page">
	<div class="container single-post <?php echo $post_customclass; ?>">
		<?php // get_template_part( 'templates/partials/header-carousel', 'none' ); ?>
		<?php get_template_part( 'templates/partials/content-news', 'none' ); ?>
		<?php get_sidebar(); ?>
	</div>
</div>

<?php
function custom_nav(){
	$navigation = '';
	$previous   = get_previous_post_link( '<div class="nav-previous">%link</div>', '%title', true );
	$next       = get_next_post_link( '<div class="nav-next">%link</div>', '%title', true );

	// Only add markup if there's somewhere to navigate to.
	if ( $previous || $next ) {
		$navigation = _navigation_markup( $previous . $next, 'post-navigation' );
	}

	echo $navigation;
}

//the_post_navigation();


endwhile;
get_template_part( 'templates/partials/section-recent-posts', 'none' );
get_footer();
?>
