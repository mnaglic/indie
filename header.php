<?php
/**
 * The header for the IBE_2016 theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package IBE_2016
 */

define( 'WP_DEBUG', true );
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!isset($_COOKIE["modal_popup"])) {
    setcookie('modal_popup', 'true', time()+259200, '/');  /* expire in 1 day +86400 */
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php endif; ?>
<?php $favicons = get_stylesheet_directory_uri() . '/images/ibe-favicons'; ?>
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $favicons; ?>/apple-icon-57x57.png?v=4987234">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $favicons; ?>/apple-icon-60x60.png?v=4987234">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $favicons; ?>/apple-icon-72x72.png?v=4987234">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $favicons; ?>/apple-icon-76x76.png?v=4987234">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $favicons; ?>/apple-icon-114x114.png?v=4987234">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $favicons; ?>/apple-icon-120x120.png?v=4987234">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $favicons; ?>/apple-icon-144x144.png?v=4987234">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $favicons; ?>/apple-icon-152x152.png?v=4987234">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $favicons; ?>/apple-icon-180x180.png?v=4987234">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $favicons; ?>/android-icon-192x192.png?v=4987234">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $favicons; ?>/favicon-32x32.png?v=4987234">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $favicons; ?>/favicon-96x96.png?v=4987234">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $favicons; ?>/favicon-16x16.png?v=4987234">
<link rel="manifest" href="<?php echo $favicons; ?>/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo $favicons; ?>/ms-icon-144x144.png?v=4987234">
<meta name="theme-color" content="#ffffff">
<meta name="google-site-verification" content="dYknFScVyP-tFqA6wZuhN-KCPXPuswlGsQ42GeHOQOc" />
<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63910122-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '112449466083870'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=112449466083870&ev=PageView&noscript=1"
/></noscript>

<!--[if IE 9]>
<style type="text/css">
.menu-navigation-container
{
	display:table;
	width:100%;
	border-collapse:collapse;
	border:none;
	position: relative;
	z-index: 10;
}

.menu-navigation-container ul
{
	float:none;
	display:table-row;
	text-align:center;
	list-style:none;
}

.menu-navigation-container ul a
{
	display:block;
}

.menu-navigation-container ul li
{
	float:none;
	display:table-cell;
	margin:0;
}

.menu-navigation-container ul ul
{
	display:none;
	border:none;
	position:absolute;
	top:100%;
	left:0;
	text-align:left;
	padding:0;
	width:auto;
}

.menu-navigation-container ul ul li
{
	position:relative;
	float:none;
	display:block;
}

.menu-navigation-container ul ul a
{
	line-height:120%;
	padding:10px 15px;
}

.menu-navigation-container ul ul ul
{
	top:0;
	left:100%;
}

.menu-navigation-container ul li:hover > ul
{
	display:block;
}
</style>
<![endif]-->

<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#f3f3f3",
      "text": "#666666"
    },
    "button": {
      "background": "#778abe",
      "text": "#ffffff"
    }
  },
  "theme": "edgeless",
  "content": {
    "message": "This website uses cookies to ensure that you get the best experience. By continuing to browse the site you are agreeing to our use of cookies. ",
    "link": "Learn More",
    "href": "https://indiebeautyexpo.com/policies/privacy/#cookies"
  }
})});
</script>
<style>
.cc-banner.cc-theme-edgeless .cc-message {
    margin-left: 1em;
    font-family: "Avenir Next",sans-serif;
    font-style: normal;
    font-size: 0.875rem;
    line-height: 1.4;
    padding: 1rem;
}
</style>
<?php wp_head(); ?>
</head>

<body <?php body_class(array('no-js')); ?>>
	<nav role="banner"  aria-controls="primary-menu" aria-expanded="false" class="mobile">
		<div class="menu-bar">
			<div class="masthead">
                <div class="accounts">
                    <ul id="social-wrapper">
                        <?php
        					if ( get_option('ibe_social_facebook') !== '' && get_option('ibe_social_facebook') !== false ) {
        						echo '<li class="fa fa-facebook-square"><a href="'.get_option('ibe_social_facebook').'"><span>Facebook</span></a></li>';
        					}
        					if ( get_option('ibe_social_instagram') !== '' && get_option('ibe_social_instagram') !== false ) {
        						echo '<li class="fa fa-instagram"><a href="'.get_option('ibe_social_instagram').'"><span>Instagram</span></a></li>';
        					}
        					if ( get_option('ibe_social_twitter') !== '' && get_option('ibe_social_twitter') !== false ) {
        						echo '<li class="fa fa-twitter"><a href="'.get_option('ibe_social_twitter').'"><span>Twitter</span></a></li>';
        					}
        					if ( get_option('ibe_social_youtube') !== '' && get_option('ibe_social_youtube') !== false ) {
        						echo '<li class="fa fa-youtube-square"><a href="'.get_option('ibe_social_youtube').'"><span>Youtube</span></a></li>';
        					}
        					if ( get_option('ibe_social_pintrest') !== '' && get_option('ibe_social_pintrest') !== false ) {
        						echo '<li class="fa fa-pintrest"><a href="'.get_option('ibe_social_pintrest').'"><span>Pintrest</span></a></li>';
        					}
                            echo '<li class="fa fa-search"><a href="#"><span>Search</span></a></li>';
                            echo '<li class="fa fa-bi"><a href="https://www.beautyindependent.com"><span>Beauty Independent</span></a></li>';
                            echo '<li class="fa fa-bx"><a href="https://www.beautyxsummit.com"><span>Beauty X Summit</span></a></li>';
        				?>
        			</ul>
                </div>
                <a class="menu-logo" href="<?php echo site_url();?>"></a>
                    <?php
                    echo '<a class="button buy-now" href="/attend">BUY TICKETS</a>';

                    /*
                        $default_show = rwmb_meta( 'ibe_default_show', array() , 359 );
                        $shows = rwmb_meta( 'ibe_showlist_selection', array() , 359 );
                        foreach ($shows as &$show) {
                            $show_id = $show['ibe_show_item'];
                            $show_buylink = rwmb_meta( 'ibe_show_buyticketslink', array(), $show_id );
                            $show_type = rwmb_meta( 'ibe_show_header_type', array(), $show_id );
                            if ( $show_id === $default_show ) {
                                echo '<a class="button buy-now" href="'.$show_buylink.'">BUY TICKETS</a>';
                            }
                        } */
                    ?>
            </div>
			<div id="menu-wrapper">
				<?php

                $args = array(
        			'theme_location' => 'primary',
        		//	'depth'      => 2,
        		//	'container'  => false,
        		//	'menu_class'     => 'nav navbar-nav navbar-right',
        		//	'walker'     => new Bootstrap_Walker_Nav_Menu()
        		);

        		wp_nav_menu($args);

				?>
                <li id="social-wrapper-mobile" class="menu-item menu-item-type-post_type menu-item-object-page">
                    <ul id="mobile-social-list">
                        <?php
        					if ( get_option('ibe_social_facebook') !== '' && get_option('ibe_social_facebook') !== false ) {
        						echo '<li class="fa fa-facebook-square"><a href="'.get_option('ibe_social_facebook').'"><span>Facebook</span></a></li>';
        					}
        					if ( get_option('ibe_social_instagram') !== '' && get_option('ibe_social_instagram') !== false ) {
        						echo '<li class="fa fa-instagram"><a href="'.get_option('ibe_social_instagram').'"><span>Instagram</span></a></li>';
        					}
        					if ( get_option('ibe_social_twitter') !== '' && get_option('ibe_social_twitter') !== false ) {
        						echo '<li class="fa fa-twitter"><a href="'.get_option('ibe_social_twitter').'"><span>Twitter</span></a></li>';
        					}
                         echo '<li class="fa fa-bi"><a href="https://www.beautyindependent.com"><span>Beauty Independent</span></a></li>';
                         echo '<li class="fa fa-bx"><a href="https://www.beautyxsummit.com"><span>Beauty X Summit</span></a></li>';

        					if ( get_option('ibe_social_youtube') !== '' && get_option('ibe_social_youtube') !== false ) {
        						echo '<li class="fa fa-youtube-square"><a href="'.get_option('ibe_social_youtube').'"><span>Youtube</span></a></li>';
        					}
        					if ( get_option('ibe_social_pintrest') !== '' && get_option('ibe_social_pintrest') !== false ) {
        						echo '<li class="fa fa-pintrest"><a href="'.get_option('ibe_social_pintrest').'"><span>Pintrest</span></a></li>';
        					}
                            echo '<li class="fa fa-search"><a href="#"><span>Search</span></a></li>';
        				?>
        			</ul>
                </li>
			</div>
			<div class="menu-button">
				<span class="" title="Menu (Esc)" tabindex="0" data-icon=""></span><span></span><span></span>
			</div>
		</div>
	</nav><!-- #masthead -->
	<div class="menu-background"></div>
	<div id="page" class="site">
	<div id="content" class="site-content">
