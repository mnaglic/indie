<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package iBE_2016
 */

if ( ! function_exists( 'ibe__posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function ibe__posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', 'ibe_' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'ibe_' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<h4 class="posted-on">' . $time_string . '</h4>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'ibe_news_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function ibe_news_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'ibe_' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( '%1$s', 'ibe_' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		//comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'ibe_' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
		ibe_comments_popup_link( 'No comments yet', '1 comment', 'Comments: %', 'comments-link', 'Comments are diabled for this post');
		echo '</span>';
	}
	/*
	edit_post_link(
		sprintf(
			esc_html__( 'Edit %s', 'ibe_' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	); */
}
endif;

function ibe_news_categories() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'ibe_' ) );
		if ( $categories_list && ibe__categorized_blog() ) {
			printf( ' | <span class="cat-links">' . esc_html__( '%1$s', 'ibe_' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}
	}

}

function social_sharing_bar($post_id) {
	$post_data =  get_post( $post_id );
	$title = urlencode($post_data->post_title);
	$raw_title = $post_data->post_title;
	$url = urlencode($post_data->guid);
	$media = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'retina' );
	$media = urlencode($media[0]);
	$output = '
	<div class="social-sharing-bar">
		<ul>
			<li class="fa fa-facebook"><a href="http://www.facebook.com/sharer/sharer.php?u='.$url.'&title='.$title.'" alt="Share this on facebook"></a></li>
			<li class="fa fa-pinterest-p"><a href="http://pinterest.com/pin/create/bookmarklet/?media='.$media.'&url='.$url.'&is_video=false&description='.$title.'" alt="Share this on pintrest"></a></li>
			<li class="fa fa-envelope"><a href="mailto:?subject='.$raw_title.'&body=Check out this article from Indie Beauty Expo: '.$url.'" alt="Share this via email"></a></li>
		</ul>
	</div>
	';
/*	$output = '
	<div class="social-sharing-bar">
		<p>SHARE IT</p>
		<ul>
			<li class="fa fa-facebook"><a href="http://www.facebook.com/sharer/sharer.php?u='.$url.'&title='.$title.'" alt="Share this on facebook"></a></li>
			<li class="fa fa-pinterest-p"><a href="http://pinterest.com/pin/create/bookmarklet/?media='.$media.'&url='.$url.'&is_video=false&description='.$title.'" alt="Share this on pintrest"></a></li>
			<li class="fa fa-envelope"><a href="mailto:?subject='.$raw_title.'&body=Check out this article from Indie Beauty Expo: '.$url.'" alt="Share this via email"></a></li>
		</ul>
	</div>
	';	*/
	echo $output;
}

function ibe_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; //'.get_comment_ID().'
	$classes = get_comment_class();
	$classes_str = '';
	foreach ($classes as $class) {
		$classes_str .= $class . ' ';
	}
	echo '<li id="li-comment-'.get_comment_ID().'" classes="'.$classes_str.'">';
	echo '	<div id="comment-'.get_comment_ID().'">';
	echo '		<div class="comment-author">';
					//echo get_avatar($comment,$size='48',$default='<path_to_url>' );
					printf(__('<cite class="fn">%s</cite>'), get_comment_author_link());
	echo '		</div>';
				if ($comment->comment_approved == '0') {
					echo '<em class="approval-required">Your comment is awaiting moderation.</em>';
				}
	echo '		<div class="comment-meta">';
	echo '			<a href="'.htmlspecialchars( get_comment_link( $comment->comment_ID ) ).'" title="'.get_comment_date().' '.get_comment_time().'">';
						echo time_ago('comment');
	echo '			</a>';
	echo '		</div>';
	echo '		<div class="comment-reply">';
					comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'])));
	echo '		</div>';
	echo '		<div class="comment-content">';
					comment_text();
	echo '		</div>';

	echo '	</div>';
	echo '</li>';
}

function ibe_comments_popup_link( $zero = false, $one = false, $more = false, $css_class = '', $none = false ) {
    $id = get_the_ID();
    $title = get_the_title();
    $number = get_comments_number( $id );

    if ( false === $zero ) {
        /* translators: %s: post title */
        $zero = sprintf( __( 'No Comments<span class="screen-reader-text"> on %s</span>' ), $title );
    }

    if ( false === $one ) {
        /* translators: %s: post title */
        $one = sprintf( __( '1 Comment<span class="screen-reader-text"> on %s</span>' ), $title );
    }

    if ( false === $more ) {
        /* translators: 1: Number of comments 2: post title */
        $more = _n( '%1$s Comment<span class="screen-reader-text"> on %2$s</span>', '%1$s Comments<span class="screen-reader-text"> on %2$s</span>', $number );
        $more = sprintf( $more, number_format_i18n( $number ), $title );
    }

    if ( false === $none ) {
        /* translators: %s: post title */
        $none = sprintf( __( 'Comments Off<span class="screen-reader-text"> on %s</span>' ), $title );
    }

    if ( 0 == $number && !comments_open() && !pings_open() ) {
        echo '<span' . ((!empty($css_class)) ? ' class="' . esc_attr( $css_class ) . '"' : '') . '>' . $none . '</span>';
        return;
    }

    if ( post_password_required() ) {
        _e( 'Enter your password to view comments.' );
        return;
    }


    if ( 0 == $number ) {
		//echo '<a ';
		echo '<a id="comment-toggle"';
        //$respond_link = get_permalink() . '#comments';
        //echo apply_filters( 'respond_link', $respond_link, $id );
		//echo '"';
    } else {
        //comments_link();
		echo '<a id="comment-toggle"';
    }

    if ( !empty( $css_class ) ) {
        echo ' class="'.$css_class.'" ';
    }

    $attributes = '';
    /**
     * Filter the comments link attributes for display.
     *
     * @since 2.5.0
     *
     * @param string $attributes The comments link attributes. Default empty.
     */
    echo apply_filters( 'comments_popup_link_attributes', $attributes );

    echo '><span class="hide-comments hidden">Hide Comments</span><span class="hide-comment-count">';
    comments_number( $zero, $one, $more );
    echo '</span></a>';
}

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function ibe__categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'ibe__categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'ibe__categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so ibe__categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so ibe__categorized_blog should return false.
		return false;
	}
}
/*
function custom_nav(){
   $navigation = '';
   $previous   = get_previous_post_link( '<div class="nav-previous">%link</div>', '%title', true );
   $next       = get_next_post_link( '<div class="nav-next">%link</div>', '%title', true );

   // Only add markup if there's somewhere to navigate to.
   if ( $previous || $next ) {
	   $navigation = _navigation_markup( $previous . $next, 'post-navigation' );
   }

   echo $navigation;
}
*/
//the_post_navigation();

/**
 * Flush out the transients used in ibe__categorized_blog.
 */
function ibe__category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'ibe__categories' );
}
add_action( 'edit_category', 'ibe__category_transient_flusher' );
add_action( 'save_post',     'ibe__category_transient_flusher' );
