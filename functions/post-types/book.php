<?php

// Register Custom Post Type
function book_post_type() {

	$labels = array(
		'name'                => _x( 'Books', 'Post Type General Name', 'ibe' ),
		'singular_name'       => _x( 'Book', 'Post Type Singular Name', 'ibe' ),
		'menu_name'           => __( 'Books', 'ibe' ),
		'parent_item_colon'   => __( 'Parent Book:', 'ibe' ),
		'all_items'           => __( 'All Books', 'ibe' ),
		'view_item'           => __( 'View Book', 'ibe' ),
		'add_new_item'        => __( 'Add New Book', 'ibe' ),
		'add_new'             => __( 'Add New', 'ibe' ),
		'edit_item'           => __( 'Edit Book', 'ibe' ),
		'update_item'         => __( 'Update Book', 'ibe' ),
		'search_items'        => __( 'Search Book', 'ibe' ),
		'not_found'           => __( 'Not found', 'ibe' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'ibe' ),
	);
	$rewrite = array(
		'slug'                => 'book',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'book', 'ibe' ),
		'description'         => __( 'Post Type Description', 'ibe' ),
		'labels'              => $labels,
		'supports'            => array( ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-book',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'book', $args );

}

// Hook into the 'init' action
add_action( 'init', 'book_post_type', 0 );


?>
