<?php
use \DrewM\MailChimp\MailChimp;

function mc_addSubscriber($email) {
	$MailChimp = new MailChimp('baad74561eff507c9d219eae8185d00c-us9');
	$list_id = '4ed1cffcc8';

	$result = $MailChimp->post("lists/$list_id/members", [
					'email_address' => $email,
					'status'        => 'subscribed',
				]);

	//$result = $MailChimp->get('lists');
//	print_r($result);
}

function loginForm() {
	echo '
	<form name="loginform" action="'.get_site_url().'/wp-login.php" method="post">
		<div>
			<input type="text" name="log" id="log" value="" size="20" tabindex="1" placeholder="Email" />
		</div>

		<div>
			<input type="password" name="pwd" id="pwd" value="" size="20" tabindex="2" placeholder="Password" />
		</div>

		<input name="wp-submit" id="wp-submit" value="Log In" tabindex="100" type="submit" tabindex="3">

		<input name="redirect_to" value="'.$_SERVER['REQUEST_URI'].'" type="hidden">
		<input name="testcookie" value="1" type="hidden">
		<a href="'.get_site_url().'/wp-login.php?action=lostpassword" class="forgot-pass">Forgot your password?</a>
	</form>
	';
}

function subscribe_form( $username, $password, $email, $first_name, $last_name, $newsletter, $terms ) {
	echo '
	    <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
			<h3>Create An Account</h3>
		    <div>
		    	<input type="text" name="username" value="' . ( isset( $_POST['username'] ) ? $username : null ) . '" placeholder="Username (required)">
		    </div>

		    <div>
		    	<input type="password" name="password" value="' . ( isset( $_POST['password'] ) ? $password : null ) . '" placeholder="Password (required)">
		    </div>

		    <div>
		    	<input type="text" name="email" value="' . ( isset( $_POST['email'] ) ? $email : null ) . '" placeholder="Email (required)">
		    </div>

		    <div>
		    	<input type="text" name="fname" value="' . ( isset( $_POST['fname']) ? $first_name : null ) . '" placeholder="First Name">
		    </div>

		    <div>
		    	<input type="text" name="lname" value="' . ( isset( $_POST['lname']) ? $last_name : null ) . '" placeholder="Last Name">
		    </div>

		    <div>
				<input name="newsletter" id="newsletter-signup" type="checkbox" checked="' . ( isset( $_POST['newsletter']) ? $newsletter : null ) . '">
				<label for="newsletter">I would like to sign up for the Newsletter</label>
		    </div>

			<div>
				<input name="terms" id="terms" type="checkbox">
				<label for="terms">I agree to the <a style="text-decoration: underline;" href="'.get_site_url().'/policies/terms-of-use/">Terms of Use</a></label>
		    </div>
			<input type="hidden" name="persist" value="true" />
		    <input type="submit" name="submit" value="Register" />
	    </form>
	';
}

function subscription_validation( $username, $password, $email, $first_name, $last_name, $newsletter, $terms )  {
	global $reg_errors;
	$reg_errors = new WP_Error;
	if ( empty( $username ) || empty( $password ) || empty( $email ) ) {
		$req_list = '';
		$req_list_count =0;
		if ( empty( $username ) ) {
			$req_list .= 'Username ';
			$req_list_count++;
		} elseif ( empty( $password ) ) {
			$req_list .= 'Password ';
			$req_list_count++;
		} elseif ( empty( $email ) ) {
			$req_list .= 'Email ';
			$req_list_count++;
		}
		if ($req_list_count > 1) {
			$plural = 's';
		} else {
			$plural = '';
		}
	    $reg_errors->add('field', 'Missing required form field'.$plural.': ' . $req_list );
	}
	if ( 4 > strlen( $username ) ) {
	    $reg_errors->add( 'username_length', 'Username too short. At least 4 characters is required' );
	}
	if ( username_exists( $username ) ) {
		$reg_errors->add('user_name', 'Sorry, that username already exists!');
	}
	if ( ! validate_username( $username ) ) {
	    $reg_errors->add( 'username_invalid', 'Sorry, the username you entered is not valid' );
	}
	if ( !is_email( $email ) ) {
	    $reg_errors->add( 'email_invalid', 'Email is not valid' );
	}
	if ( email_exists( $email ) ) {
	    $reg_errors->add( 'email', 'Email Already in use' );
	}
	if( !isset($_POST['terms']) || $_POST['terms'] === '' ) {
	    $reg_errors->add( 'terms', 'You must agree to the Terms of Use' );

	}
	if ( is_wp_error( $reg_errors ) ) {
		$success = false;
	    foreach ( $reg_errors->get_error_messages() as $error ) {

	        echo '<div>';
	        echo '<strong>ERROR</strong>:';
	        echo $error . '<br/>';
	        echo '</div>';

	    }

	} else {
		$success = true;
	}
}

function complete_registration() {
    global $reg_errors, $username, $password, $email, $first_name, $last_name, $newsletter, $terms;
    if ( 1 > count( $reg_errors->get_error_messages() ) ) {
        $userdata = array(
        'user_login'    =>   $username,
        'user_email'    =>   $email,
        'user_pass'     =>   $password,
        'first_name'    =>   $first_name,
        'last_name'     =>   $last_name,
        'newsletter'    =>   $newsletter,
        'terms'  		=>   $terms,
        );
        $user = wp_insert_user( $userdata );
        echo '<h3 class="sub-success">Registration complete!</h3> <p class="sub-success">Please log in</p></a>.';
		// Register user for newsletter if they consented
		if( isset($newsletter) && $newsletter !== '' ) {
		    mc_addSubscriber($email);
		}
    }
}

function register_subscriber() {
	global $reg_errors;
	$success = false;
    if ( isset($_POST['submit'] ) ) {
        subscription_validation(
        $_POST['username'],
        $_POST['password'],
        $_POST['email'],
        $_POST['fname'],
        $_POST['lname'],
        isset($_POST['newsletter']),
        isset($_POST['terms'])
        );

        // sanitize user form input
        global $username, $password, $email, $first_name, $last_name, $newsletter, $terms;
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );
        $first_name =   sanitize_text_field( $_POST['fname'] );
        $last_name  =   sanitize_text_field( $_POST['lname'] );
        $newsletter   = sanitize_text_field( isset($_POST['newsletter']) );
        $terms        = sanitize_text_field( isset($_POST['terms']) );
        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
        $username,
        $password,
        $email,
        $first_name,
        $last_name,
        $newsletter,
        $terms
        );
		if ( 1 > count( $reg_errors->get_error_messages() ) ) {
			$success = true;
		}
    }
//	var_dump($reg_errors);
	if ($success === false) {
		subscribe_form(
	        ( isset( $username ) ? $username : null ),
			( isset( $password ) ? $password : null ),
			( isset( $email ) ? $email : null ),
			( isset( $first_name ) ? $first_name : null ),
			( isset( $last_name ) ? $last_name : null ),
			( isset( $newsletter ) ? $newsletter : null ),
			( isset( $terms ) ? $terms : null )
	        );
	} else {
		loginform();
	}

}

// Register a new shortcode: [ibe_subscriber_registration]
add_shortcode( 'ibe_subscriber_registration', 'ibe_subscriber_registration_shortcode' );

// The callback function that will replace [book]
function ibe_subscriber_registration_shortcode() {
    ob_start();
    register_subscriber();
    return ob_get_clean();
}

?>
