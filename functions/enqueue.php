<?php

/**
 * Enqueue scripts and styles.
 */

 // Remove jQuery Migrate Script from header and Load jQuery from Google API
 function crunchify_remove_jquery_migrate() {
 	if (!is_admin()) {
 		wp_deregister_script('jquery');
 		//wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', false, null);
		wp_register_script('jquery', get_template_directory_uri() . '/js/jquery.min.js', false, null);
 		wp_enqueue_script('jquery');
 	}
 }
 add_action('init', 'crunchify_remove_jquery_migrate');

 function ibe_scripts() {
    //wp_enqueue_script( 'ibe-app', get_template_directory_uri() . '/js/passive.min.js', array(), false , true );
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.js', array(), false, true );
	wp_enqueue_script( 'slick-data', get_template_directory_uri() . '/js/newSlider.js', array(), false, true );
	wp_enqueue_script( 'ibe-app', get_template_directory_uri() . '/js/app.min.js', array(), filemtime( get_stylesheet_directory().'/js/app.min.js' ), true );
    wp_enqueue_script( 'lib-modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array(), false, true );
	wp_enqueue_script( 'lib-flickity-js', get_template_directory_uri() . '/js/flickity.pkgd.min.js', array(), false, true );
    wp_enqueue_script( 'lib-html5shiv-js', get_template_directory_uri() . '/js/html5shiv.min.js', array(), false, true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
 }

function ibe_styles() {
	wp_enqueue_style( 'ibe-style', get_template_directory_uri() . '/css/style.css', array(), filemtime( get_stylesheet_directory().'/css/style.css' ) );
	wp_enqueue_style( 'lib-fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), true );
}

function admin_assets() {
	wp_enqueue_style( 'admin-css', get_template_directory_uri() . '/css/admin.min.css', array(), true );
    //wp_enqueue_style( 'admin-fonts', 'https://fonts.googleapis.com/css?family=Karla', array(), true );
}

add_action( 'wp_enqueue_scripts', 'ibe_scripts' );
add_action( 'wp_enqueue_scripts', 'ibe_styles' );
add_action('admin_head', 'admin_assets');

?>
