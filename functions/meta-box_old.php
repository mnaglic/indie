<?php
add_filter( 'rwmb_meta_boxes', 'ibe_register_meta_boxes' );
function ibe_register_meta_boxes( $meta_boxes ) {

    $prefix = 'ibe_';
    $post_types = get_post_types();

    // Home -  Instructions
    $meta_boxes[] = array(
    	'title'  => __( 'Please Note', 'ibe' ),
    	'post_types' => array('page'),
    	'include' => array(
    		'template' => array( 'templates/home.php' ),
    	),
    	'fields' => array(
    		array(
				'desc' => esc_html__( 'The content shown on the home page is controlled by the selected Landing Page. To change the home page content, please modify an existing landing page or create a new one by selecting Landing Pages in the side menu.  In te box below, you may select which of the landing pages will be active.', 'ibe' ),
                'id' => $prefix . 'notice_home',
    			'type' => 'post',
                'field_type'  => 'heading',
    		),
    	),
    );
    // Home - Default Landing Select
    $meta_boxes[] = array(
    	'title'  => __( 'Default Landing Page', 'ibe' ),
    	'post_types' => array('page'),
    	'include' => array(
    		'template' => array( 'templates/home.php' ),
    	),
    	'fields' => array(
    		array(
                'name'  => __( 'Landing Page', 'ibe' ),
                'tooltip' => __('The page that will be used as the default home page, and displayed to users who are not following a custom landing page link.','ibe'),
    			'id' => $prefix . 'default_landing',
    			'type' => 'post',
    			'post_type'  => 'landing',
                'field_type'  => 'select',
    		),
    	),
    );

    // News - Custom CSS
    $meta_boxes[] = array(
    	'title'  => __( 'Custom Styling', 'ibe' ),
    	'post_types' => array('post'),
    	'fields' => array(
            array(
                'name' => __('Custom Class','ibe'),
                'tooltip' => __('Assign a custom class','ibe'),
                'id' => $prefix . 'post_customclass',
                'type' => 'text',
            ),
            array(
                'name' => __('CSS','ibe'),
                'tooltip' => __('Use this box to inject custom CSS code into this post','ibe'),
                'id' => $prefix . 'post_customcss',
                'type' => 'textarea',
            ),
    	),
    );

    // Home - Modal Popup
    $meta_boxes[] = array(
    	'title'  => __( 'Modal Popup', 'ibe' ),
    	'post_types' => array('page'),
    	'include' => array(
    		'template' => array( 'templates/home.php' ),
    	),
    	'fields' => array(
    		array(
                'name'  => __( 'Active Popup', 'ibe' ),
                'tooltip' => __('Select which popup should be active on the site','ibe'),
    			'id' => $prefix . 'default_popup',
    			'type' => 'post',
    			'post_type'  => 'popup',
                'field_type'  => 'select',
    		),
    	),
    );

    // Landing Page - Unique Code
    $meta_boxes[] = array(
    	'title'  => __( 'Custom Direct Link', 'ibe' ),
    	'post_types' => array('landing'),
    	'fields' => array(
            array(
    			'name' => __('Link URL','ibe'),
    			'tooltip' => __('Copy this URL to link to this custom landing page.','ibe'),
    			'id' => $prefix . 'custom_landing_url',
    			'type' => 'text',
                'size' => '60',
    			'std'  => '',
                'class' => 'custom_landing_url',
                'attributes' => array(
                    'readonly'  => true,
                ),
    		),
    	),
    );

    // Landing Page - Header
    $meta_boxes[] = array(
    	'title'  => __( 'Header', 'ibe' ),
    	'post_types' => array('landing'),
    	'fields' => array(
            array(
                'id'	=> $prefix . 'home_header_type',
                'name'	=> 'Header Type',
                'tooltip' => __('Select what type of header will be used','ibe'),
                'type'	=> 'select',
                'options' => array(
                    'none'      => 'None',
                    'carousel' 	=> 'Carousel',
                    'youtube'   => 'Youtube'
                )
            ),
    		array(
    			'id'     => $prefix . 'home_carousel_images',
    			'title'  => __( 'Carousel Image', 'ibe' ),
    			'type'   => 'group',
    			'clone'  => true,
    			'sort_clone' => true,
                'visible' => array( $prefix . 'home_header_type', 'in', array('carousel')),
    			'fields' => array(
                    array(
        				'id'	=> $prefix . 'home_slide_type',
        				'name'	=> 'Slide Type',
                        'tooltip' => __('Select what kind of slide this will be.','ibe'),
                        'type'	=> 'select',
        				'options' => array(
        					'images'      => 'Images',
        					'wysiwyg' 	    => 'Background + WYSIWYG',
        				),
                        'visible' => array( $prefix . 'home_header_type', 'in', array('carousel'))
        			),
    				array(
    					'id'          => $prefix . 'home_carousel_img_desktop',
    					'name'        => __( 'Background Image (Desktop)', 'ibe' ),
                        'tooltip' => __('Image to be used as the background of the slide on desktop computers. Minimum dimensions: 1400 x 566 px.','ibe'),
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                        'visible' => array( $prefix . 'home_slide_type', 'in', array('images', 'wysiwyg'))
    				),
                    array(
    					'id'          => $prefix . 'home_carousel_img_mobile',
    					'name'        => __( 'Background Image (Mobile)', 'ibe' ),
                        'tooltip' => __('Image to be used as the background of the slide on mobile devices. Minimum dimensions: 800 x 1200 px.','ibe'),
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                        'visible' => array( $prefix . 'home_slide_type', 'in', array('images', 'wysiwyg'))
    				),
                    array(
        				'id'	=> $prefix . 'home_carousel_bgpos',
        				'name'	=> 'Image Alignment',
                        'tooltip' => __('Select how the image should be vertically centered. This is to better accommodate images with text, that may otherwise have the text cut off. If no option is selected it will be centered.','ibe'),
                        'type'	=> 'select',
                        'placeholder' => __('Please select an option','ibe'),
        				'options' => array(
        					'center'      => 'Center (default)',
        					'bottom' 	    => 'Bottom',
        					'top'      => 'Top',
        				),
                        'visible' => array( $prefix . 'home_slide_type', 'in', array('images', 'wysiwyg'))
        			),
                    array(
                        'name' => __('Text/HTML Content','ibe'),
                        'tooltip' => __('The summary text for this custom featured item.','ibe'),
                        'id' => $prefix . 'home_carousel_content',
                        'type' => 'wysiwyg',
                        'options' => array(
                            'media_buttons'      => false,
                            'textarea_rows' 	    => 6
                        ),
                        'visible' => array( $prefix . 'home_slide_type', 'in', array('wysiwyg'))
                    ),
                    array(
        				'id'	=> $prefix . 'home_carousel_slide_link',
        				'name'	=> 'Link Type',
                        'tooltip' => __('Select what kind of page this slide should link to (optional).','ibe'),
                        'type'	=> 'select',
        				'options' => array(
        					'none'      => 'None',
        					'page' 	    => 'Page',
        					'news'      => 'News',
                            'article' 	=> 'Article',
                            'deck' 	    => 'Deck',
                            'url' 	    => 'URL'
        				),
                        'visible' => array( $prefix . 'home_header_type', 'in', array('carousel'))
        			),
                    array(
                        'name' => __('Page Link','ibe'),
            			'tooltip' => __('Select the page that this image should link to (optional).','ibe'),
            			'id' => $prefix . 'home_carousel_link_page',
            			'type' => 'post',
            			'post_type' => array('page'),
                        'field_type'  => 'select',
                        'hidden' => array( 'home_carousel_slide_link', '!=', 'page' )
            		),
                    array(
                        'name' => __('News Link','ibe'),
            			'tooltip' => __('Select the news post that this image should link to (optional).','ibe'),
            			'id' => $prefix . 'home_carousel_link_news',
            			'type' => 'post',
            			'post_type' => array('post'),
                        'field_type'  => 'select',
                        'hidden' => array( 'home_carousel_slide_link', '!=', 'news' )
            		),
                    array(
                        'name' => __('Article Link','ibe'),
            			'tooltip' => __('Select the article that this image should link to (optional).','ibe'),
            			'id' => $prefix . 'home_carousel_link_article',
            			'type' => 'post',
            			'post_type' => array('article'),
                        'field_type'  => 'select',
                        'hidden' => array( 'home_carousel_slide_link', '!=', 'article' )
            		),
                    array(
                        'name' => __('Deck Link','ibe'),
            			'tooltip' => __('Select the deck that this image should link to (optional).','ibe'),
            			'id' => $prefix . 'home_carousel_link_deck',
            			'type' => 'post',
            			'post_type' => array('deck'),
                        'field_type'  => 'select',
                        'hidden' => array( 'home_carousel_slide_link', '!=', 'deck' )
            		),
    				array(
    					'name' => __('External Link','ibe'),
    					'tooltip' => __('Input the full link that this image should link to (optional) Ignore this if you have selected an internal link above.','ibe'),
    					'id' => $prefix . 'home_carousel_link_url',
    					'type' => 'text',
    					'size'  => '60',
                        'hidden' => array( 'home_carousel_slide_link', '!=', 'url' )
    				),
    			),
    		),
            array(
                'id'     => $prefix . 'home_carousel_youtube',
                'title'  => __( 'Video', 'ibe' ),
                'type'   => 'group',
                'clone'  => false,
                'visible' => array( $prefix . 'home_header_type', 'in', array('youtube')),
                'fields' => array(

                    array(
                        'id'          => $prefix . 'home_carousel_youtube_oembed',
                        'name'        => __( 'Youtube URL', 'ibe' ),
                        'tooltip' => __('Input the share code for videos from Youtube. Example: https://youtu.be/SLR8e1i_3eQ','ibe'),
                        'type' => 'oembed',
                        'visible' => array( $prefix . 'home_header_type', 'in', array('youtube'))
                    ),
                    array(
                        'name' => __('Youtube Playlist','ibe'),
                        'tooltip' => __('In order to loop infinitely and not display other videos, the video must be in a playlist by itself and the playlist code must be pasted below. Do not paste the "https://www.youtube.com/playlist?list=" part of the URL','ibe'),
                        'id' => $prefix . 'home_carousel_youtube_playlist',
                        'type' => 'text',
                        'size'  => '60',
                        'visible' => array( $prefix . 'home_header_type', 'in', array('youtube'))
                    ),
                    array(
                        'id'	=> $prefix . 'home_carousel_youtube_link',
                        'name'	=> 'Link Type',
                        'tooltip' => __('Select what kind of page this slide should link to (optional).','ibe'),
                        'type'	=> 'select',
                        'options' => array(
                            'none'      => 'None',
                            'page' 	    => 'Page',
                            'news'      => 'News',
                            'article' 	=> 'Article',
                            'deck' 	    => 'Deck',
                            'url' 	    => 'URL'
                        ),
                    ),
                    array(
                        'id'          => $prefix . 'home_carousel_youtube_mobile',
                        'name'        => __( 'Mobile Fallback Image ', 'ibe' ),
                        'tooltip' => __('Image to be used as the background of the slide on mobile devices. Minimum dimensions: 800 x 1200 px.','ibe'),
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                    ),
                    array(
                        'name' => __('Page Link','ibe'),
                        'tooltip' => __('Select the page that this image should link to (optional).','ibe'),
                        'id' => $prefix . 'home_carousel_link_page',
                        'type' => 'post',
                        'post_type' => array('page'),
                        'field_type'  => 'select',
                        'hidden' => array( 'home_carousel_youtube_link', '!=', 'page' )
                    ),
                    array(
                        'name' => __('News Link','ibe'),
                        'tooltip' => __('Select the news post that this image should link to (optional).','ibe'),
                        'id' => $prefix . 'home_carousel_link_news',
                        'type' => 'post',
                        'post_type' => array('post'),
                        'field_type'  => 'select',
                        'hidden' => array( 'home_carousel_youtube_link', '!=', 'news' )
                    ),
                    array(
                        'name' => __('Article Link','ibe'),
                        'tooltip' => __('Select the article that this image should link to (optional).','ibe'),
                        'id' => $prefix . 'home_carousel_link_article',
                        'type' => 'post',
                        'post_type' => array('article'),
                        'field_type'  => 'select',
                        'hidden' => array( 'home_carousel_youtube_link', '!=', 'article' )
                    ),
                    array(
                        'name' => __('Deck Link','ibe'),
                        'tooltip' => __('Select the deck that this image should link to (optional).','ibe'),
                        'id' => $prefix . 'home_carousel_link_deck',
                        'type' => 'post',
                        'post_type' => array('deck'),
                        'field_type'  => 'select',
                        'hidden' => array( 'home_carousel_youtube_link', '!=', 'deck' )
                    ),
                    array(
                        'name' => __('External Link','ibe'),
                        'tooltip' => __('Input the full link that this image should link to (optional) Ignore this if you have selected an internal link above.','ibe'),
                        'id' => $prefix . 'home_carousel_link_url',
                        'type' => 'text',
                        'size'  => '60',
                        'hidden' => array( 'home_carousel_youtube_link', '!=', 'url' )
                    ),
                ),
            ),

    	),
    );

    // Landing Page - Featured Content
    $meta_boxes[] = array(
    	'title'  => __( 'Featured Conent ', 'ibe' ),
    	'post_types' => array('landing'),
    	'fields' => array(
    		array(
    			'id'     => $prefix . 'home_featured_items',
    			'title'  => __( 'Featured Items', 'ibe' ),
    			'type'   => 'group',
    			'clone'  => true,
    			'sort_clone' => true,
    			'fields' => array(
                    array(
        				'id'	=> $prefix . 'home_featured_item_type',
        				'name'	=> 'Type',
                        'tooltip' => __('What kind of page/post is being featured?','ibe'),
                        'type'	=> 'select',
                        'placeholder'	=> 'Please select a type',
        				'options' => array(
        					'page' 	    => 'Page',
        					'news'      => 'News',
                            'article' 	=> 'Article',
                            'custom' 	    => 'Custom'
        				)
        			),
                    array(
        				'id'	=> $prefix . 'home_featured_item_style',
        				'name'	=> 'Display Size',
                        'tooltip' => __('The size this featurd article should occupy. NOTE: If spotlight is selected, the article MUST be positioned as an odd number in the order of featured items to avoid an issue with the display of the articles.','ibe'),
                        'type'	=> 'select',
                        'placeholder'	=> 'Please select an option',
        				'options' => array(
        					'featured' 	    => 'Featured (1/2 Page Width)',
        					'spotlight'      => 'Spotlight (Full Page Width)'
        				)
        			),
                    array(
                        'name' => __('Page to Feature','ibe'),
            			'tooltip' => __('Select the page that this image should link to (optional).','ibe'),
            			'id' => $prefix . 'home_featured_item_page',
            			'type' => 'post',
            			'post_type' => array('page'),
                        'field_type'  => 'select',
                        'hidden' => array( 'home_featured_item_type', '!=', 'page' )
            		),
                    array(
                        'name' => __('News to Feature','ibe'),
            			'tooltip' => __('Select the news post that this image should link to (optional).','ibe'),
            			'id' => $prefix . 'home_featured_item_news',
            			'type' => 'post',
            			'post_type' => array('post'),
                        'field_type'  => 'select',
                        'hidden' => array( 'home_featured_item_type', '!=', 'news' )
            		),
                    array(
                        'name' => __('Article to Feature','ibe'),
            			'tooltip' => __('Select the article that this image should link to (optional).','ibe'),
            			'id' => $prefix . 'home_featured_item_article',
            			'type' => 'post',
            			'post_type' => array('article'),
                        'field_type'  => 'select',
                        'hidden' => array( 'home_featured_item_type', '!=', 'article' )
            		),
                    array(
    					'name' => __('Custom Image','ibe'),
    					'tooltip' => __('select an image for this custom featured item.','ibe'),
    					'id' => $prefix . 'home_featured_item_custom_image',
    					'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                        'hidden' => array( 'home_featured_item_type', '!=', 'custom' )
    				),
    				array(
    					'name' => __('Custom Title','ibe'),
    					'tooltip' => __('The title for this custom featured item.','ibe'),
    					'id' => $prefix . 'home_featured_item_custom_title',
    					'type' => 'text',
    					'size'  => '60',
                        'hidden' => array( 'home_featured_item_type', '!=', 'custom' )
    				),
                    array(
    					'name' => __('Custom Summary','ibe'),
    					'tooltip' => __('The summary text for this custom featured item.','ibe'),
    					'id' => $prefix . 'home_featured_item_custom_summary',
    					'type' => 'wysiwyg',
    					'size'  => '60',
                        'hidden' => array( 'home_featured_item_type', '!=', 'custom' )
    				),
                    array(
    					'name' => __('Custom Link','ibe'),
    					'tooltip' => __('The url this custom featured item should link to.','ibe'),
    					'id' => $prefix . 'home_featured_item_custom_url',
    					'type' => 'text',
    					'size'  => '60',
                        'std' => 'http://',
                        'hidden' => array( 'home_featured_item_type', '!=', 'custom' )
    				),
                    array(
        				'id'	=> $prefix . 'home_featured_item_bgpos',
        				'name'	=> 'Custom Image Alignment',
                        'tooltip' => __('Select how the image should be vertically centered. This is to better accommodate images with text, that may otherwise have the text cut off. If no option is selected it will be centered.','ibe'),
                        'type'	=> 'select',
                        'placeholder' => __('(Optional)','ibe'),
        				'options' => array(
        					'center'      => 'Center (default)',
        					'bottom' 	    => 'Bottom',
        					'top'      => 'Top',
        				)
        			),
    			),
    		),
    	),
    );

    // Modal Popup
    $meta_boxes[] = array(
    	'title'  => __( 'Popup Content', 'ibe' ),
        'post_types' => array('popup'),
    	'fields' => array(
    		array(
    			'id'     => $prefix . 'popup',
    			'title'  => __( 'Popup Content', 'ibe' ),
    			'type'   => 'group',
    			'clone'  => false,
    			'fields' => array(
                    array(
    					'name' => __('Text/HTML Content','ibe'),
    					'tooltip' => __('The summary text for this custom featured item.','ibe'),
    					'id' => $prefix . 'popup_content',
    					'type' => 'wysiwyg',
    					'size'  => '80',
    				),
                    array(
    					'name' => __('Background Image','ibe'),
    					'tooltip' => __('Select an image to use for the background of this popup. For best results, use a square image.','ibe'),
    					'id' => $prefix . 'popup_image',
    					'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
    				),
                    array(
    					'name' => __('Popup Link (optional)','ibe'),
    					'tooltip' => __('An optional url that will turn the entire popup into a link. Leave this blank if you have links within the popup. Must begin with http://','ibe'),
    					'id' => $prefix . 'popup_url',
    					'type' => 'text',
    					'size'  => '60',
                        'placeholder' => 'http://',
    				),
    			),
    		),
    	),
    );

    // About - Staff
    $meta_boxes[] = array(
    	'title'  => __( 'Meet the Founders', 'ibe' ),
        'post_types' => array('page'),
    	'include' => array(
    		'template' => array( 'templates/about.php' ),
    	),
    	'fields' => array(
    		array(
    			'id'     => $prefix . 'staff_member',
    			'title'  => __( 'Staff Member', 'ibe' ),
    			'type'   => 'group',
    			'clone'  => true,
                'sort_clone' => true,
    			'fields' => array(
                    array(
    					'name' => __('Name','ibe'),
    					'tooltip' => __('The name of this staff member.','ibe'),
    					'id' => $prefix . 'staff_member_name',
    					'type' => 'text',
    					'size'  => '60',
    				),
                    array(
    					'name' => __('Bio','ibe'),
    					'tooltip' => __('The biographic info for this staff member.','ibe'),
    					'id' => $prefix . 'staff_member_bio',
                        'type' => 'wysiwyg',
                        'options' => array(
                            'media_buttons' => false,
                            'textarea_rows' => 6
                        )
                    ),
                    array(
    					'name' => __('Photo','ibe'),
    					'tooltip' => __('The photo of this staff member.','ibe'),
    					'id' => $prefix . 'staff_member_photo',
    					'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
    				),
    			),
    		),
    	),
    );
    // Contact - Headline
    $meta_boxes[] = array(
    	'title'  => __( 'Headline', 'ibe' ),
        'post_types' => array('page'),
    	'include' => array(
    		'template' => array(
                'templates/contact.php',
            ),
    	),
    	'fields' => array(
            array(
                'name' => __('Headline','ibe'),
                'tooltip' => __('The headline for this page','ibe'),
                'id' => $prefix . 'page_headline',
                'type' => 'text',
                'size'  => '60',
            ),
    	),
    );
    // Contact - Details
    $meta_boxes[] = array(
    	'title'  => __( 'Contacts', 'ibe' ),
        'post_types' => array('page'),
    	'include' => array(
    		'template' => array( 'templates/contact.php' ),
    	),
    	'fields' => array(
    		array(
    			'id'     => $prefix . 'contact',
    			'title'  => __( 'Contact', 'ibe' ),
    			'type'   => 'group',
    			'clone'  => true,
                'sort_clone' => true,
    			'fields' => array(
                    array(
    					'name' => __('Title','ibe'),
    					'tooltip' => __('The title of this contact','ibe'),
    					'id' => $prefix . 'contact_title',
    					'type' => 'text',
    					'size'  => '60',
    				),
                    array(
    					'name' => __('Subhead','ibe'),
    					'tooltip' => __('The subheadline of this contact','ibe'),
    					'id' => $prefix . 'contact_subhead',
    					'type' => 'text',
    					'size'  => '60',
    				),
                    array(
    					'name' => __('Email','ibe'),
    					'tooltip' => __('The email address of this contact','ibe'),
    					'id' => $prefix . 'contact_email',
    					'type' => 'text',
    					'size'  => '60',
    				),
    			),
    		),
    	),
    );

    // Decks - Select File
    $meta_boxes[] = array(
    	'title'  => __( 'Select a File', 'ibe' ),
    	'post_types' => array('deck'),
    	'fields' => array(
    		array(
                'name'  => __( 'File', 'ibe' ),
    			'tooltip' => __('Select a PDF deck (or any type of file) that will be downloadable from this link.','ibe'),
    			'id' => $prefix . 'deck_url',
    			'type' => 'file_input',
    		),
    	),
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Attend Template
    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Attend - Header
    $meta_boxes[] = array(
    	'title'  => __( 'Header', 'ibe' ),
        'post_types' => array('page'),
    	'include' => array(
    		'template' => array(
                'templates/attend.php',
            ),
    	),
    	'fields' => array(
            array(
                'name' => __('Headline','ibe'),
                'tooltip' => __('The headline for this page','ibe'),
                'id' => $prefix . 'showlist_headline',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'id'          => $prefix . 'showlist_background',
                'name'        => __( 'Background Image', 'ibe' ),
                'tooltip' => __('Image to be used as the background of the header section. Minimum dimensions: 1400 x 566 px.','ibe'),
                'type' => 'image_advanced',
                'max_file_uploads' => 1,
                'max_status' => false,
            ),
    	),
    );
    // Attend - Shows
    $meta_boxes[] = array(
        'title'  => __( 'Shows', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array( 'templates/attend.php' ),
        ),
        'fields' => array(
            array(
                'name' => __('Default Show','ibe'),
                'tooltip' => __('Select the show that should be selected by default.','ibe'),
                'id' => $prefix . 'default_show',
                'type' => 'post',
                'post_type' => array('show'),
                'field_type'  => 'select',
            ),
            array(
                'id' => $prefix . 'default_divider',
                'type' => 'divider',
            ),
            array(
                'id'     => $prefix . 'showlist_selection',
                'title'  => __( 'Shows', 'ibe' ),
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => __('Show','ibe'),
                        'tooltip' => __('Select the show to display.','ibe'),
                        'id' => $prefix . 'show_item',
                        'type' => 'post',
                        'post_type' => array('show'),
                        'field_type'  => 'select',
                    ),
                ),
            ),
        ),
    );


    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Shows
    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Details
    $meta_boxes[] = array(
    	'title'  => __( 'Details', 'ibe' ),
        'post_types' => array('show'),
    	'fields' => array(
            array(
                'name' => __('City','ibe'),
                'tooltip' => __('The city this show is located in','ibe'),
                'id' => $prefix . 'show_details_city',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'name' => __('Dates','ibe'),
                'tooltip' => __('The dates this show spans','ibe'),
                'id' => $prefix . 'show_details_dates',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'name' => __('Enabled','ibe'),
                'tooltip' => __('If this box is unchecked, the information below will not be displayed publicly for this event','ibe'),
                'id' => $prefix . 'show_details_enabled',
                'type' => 'checkbox',
            ),
            array(
                'id'          => $prefix . 'footer_youtube_oembed',
                'name'        => __( 'Video URL', 'ibe' ),
                'tooltip' => __('Input the share code for videos from Youtube. Example: https://youtu.be/SLR8e1i_3eQ','ibe'),
                'type' => 'oembed',
            ),
            array(
                'name' => __('Video Tagline','ibe'),
                'tooltip' => __('The tagline for this video','ibe'),
                'id' => $prefix . 'footer_video_tagline',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'name' => __('Video Text','ibe'),
                'tooltip' => __('This is the description that will be displayed below the tagline','ibe'),
                'id' => $prefix . 'footer_video_description',
                'type' => 'textarea',
            ),
            array(
                'name' => __('Anthology Cover Image','ibe'),
                'id' => $prefix . 'footer_anthology_image',
                'type' => 'image_advanced',
                'max_file_uploads' => 1,
                'max_status' => false,
            ),
            array(
                'id'          => $prefix . 'footer_anthology_link',
                'name'        => __( 'Anthology Link', 'ibe' ),
                'tooltip' => __('Link to the anthology.','ibe'),
                'type' => 'text',
                'size'  => '60',
                'placeholder' => __('http://','ibe'),
            ),
            array(
                'name' => __('Anthology Tagline','ibe'),
                'tooltip' => __('The tagline for this video','ibe'),
                'id' => $prefix . 'footer_anthology_tagline',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'name' => __('Anthology Text','ibe'),
                'tooltip' => __('This is the description that will be displayed below the tagline','ibe'),
                'id' => $prefix . 'footer_anthology_description',
                'type' => 'textarea',
            ),
    	),
    );
    $meta_boxes[] = array(
        'title'  => __( 'Day 1', 'ibe' ),
        'post_types' => array('show'),
        'fields' => array(
            array(
                'id'     => $prefix . 'show_day_2',
                'type'   => 'group',
                'clone'  => false,
                'sort_clone' => false,
                'fields' => array(
                    array(
                        'name' => __('Title','ibe'),
                        'tooltip' => __('The title of this day','ibe'),
                        'id' => $prefix . 'day_2_title',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Date','ibe'),
                        'tooltip' => __('The date this show day','ibe'),
                        'id' => $prefix . 'day_2_date',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Times','ibe'),
                        'tooltip' => __('The times this show day spans','ibe'),
                        'id' => $prefix . 'day_2_times',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Location Name','ibe'),
                        'tooltip' => __('The name of the location for this show day','ibe'),
                        'id' => $prefix . 'day_2_location',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Address Line 1','ibe'),
                        'tooltip' => __('The address line 1 this show day','ibe'),
                        'id' => $prefix . 'day_2_address1',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Address Line 2','ibe'),
                        'tooltip' => __('The address line 2 this show day','ibe'),
                        'id' => $prefix . 'day_2_address2',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Address Line 3','ibe'),
                        'tooltip' => __('The address line 3 this show day','ibe'),
                        'id' => $prefix . 'day_2_address3',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Map','ibe'),
                        'tooltip' => __('The google maps link to this location','ibe'),
                        'id' => $prefix . 'day_2_map',
                        'type' => 'url',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Audience','ibe'),
                        'tooltip' => __('Who this day is targeting','ibe'),
                        'id' => $prefix . 'day_2_audience',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Description','ibe'),
                        'tooltip' => __('This is the description of each day that will be displayed for each day before clicking the learn more button','ibe'),
                        'id' => $prefix . 'day_2_description',
                        'type' => 'wysiwyg',
                        'options' => array(
                            'media_buttons'      => false,
                            'textarea_rows' 	 => 4
                        ),
                    ),
                    array(
                        'name' => __('Header Image','ibe'),
                        'id' => $prefix . 'day_2_image',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                    ),
                    array(
                        'name' => __('Ticket Button Text','ibe'),
                        'id' => $prefix . 'day_2_tickets_text',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Ticket Button URL','ibe'),
                        'id' => $prefix . 'day_2_tickets_url',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Schedule Button Text','ibe'),
                        'id' => $prefix . 'day_2_schedule_text',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Schedule Button URL','ibe'),
                        'id' => $prefix . 'day_2_schedule_url',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Details Enabled','ibe'),
                        'tooltip' => __('If this box is unchecked, the learn more button for this day will not be displayed.','ibe'),
                        'id' => $prefix . 'day_2_enabled',
                        'type' => 'checkbox',
                    ),
                    // Modular Content Blocks
                    array(
                        'type' => 'heading',
                        'name' => esc_html__( 'Day Details', 'ibe' ),
                    ),
                    array(
                        'id'     => $prefix . 'day_2_blocks',
                        'title'  => __( 'Blocks', 'ibe' ),
                        'type'   => 'group',
                        'clone'  => true,
                        'sort_clone' => true,
                        'fields' => array(
                            array(
                                'id'	=> $prefix . 'day_2_section_type',
                                'name'	=> 'Type',
                                'tooltip' => __('What type of section is this?','vx'),
                                'type'	=> 'select',
                                'placeholder'	=> 'Please select a type',
                                'options' => array(
                                    'section_type_paragraph'    => 'Paragraph',
                                    'section_type_ticket'       => 'Ticket',
                                    'section_type_button'       => 'Button',
                                    'section_type_callout'      => 'Callout',
                                    'section_type_divider'      => 'Divider'
                                )
                            ),
                            array(
                                'id' => $prefix . 'day_2_section_heading',
                                'name' => __('Heading','ibe'),
                                'tooltip' => __('A Heading for this block.','ibe'),
                                'type' => 'text',
                                'visible' => array( $prefix . 'day_2_section_type', 'in', array('section_type_paragraph','section_type_list','section_type_ticket','section_type_callout','section_type_button'))
                            ),
                            array(
                                'id' => $prefix . 'day_2_section_ticketprice',
                                'name' => __('Pricing','ibe'),
                                'tooltip' => __('Pricing information for this ticket','ibe'),
                                'type' => 'text',
                                'visible' => array( $prefix . 'day_2_section_type', 'in', array('section_type_ticket'))

                            ),
                            array(
                                'id' => $prefix . 'day_2_section_copy',
                                'name' => __('Copy','ibe'),
                                'tooltip' => __('The copy for this block.','ibe'),
                                'type' => 'wysiwyg',
                                'options' => array(
                                    'media_buttons'      => false,
                                    'textarea_rows' 	 => 4
                                ),
                                'visible' => array( $prefix . 'day_2_section_type', 'in', array('section_type_paragraph','section_type_list','section_type_ticket','section_type_callout'))
                            ),
                            //Button
                            array(
                                'id' => $prefix . 'day_2_section_url',
                                'name' => __('URL','ibe'),
                                'tooltip' => __('The URL for this button to link to.','ibe'),
                                'type' => 'url',
                                'size'  => '80',
                                'visible' => array( $prefix . 'day_2_section_type', 'in', array('section_type_button'))
                            ),
                        ),
                    ),
                ),
            ),
        ),
    );

    $meta_boxes[] = array(
        'title'  => __( 'Day 2', 'ibe' ),
        'post_types' => array('show'),
        'fields' => array(
            array(
                'id'     => $prefix . 'show_day_3',
                'type'   => 'group',
                'clone'  => false,
                'sort_clone' => false,
                'fields' => array(
                    array(
                        'name' => __('Title','ibe'),
                        'tooltip' => __('The title of this day','ibe'),
                        'id' => $prefix . 'day_3_title',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Date','ibe'),
                        'tooltip' => __('The date this show day','ibe'),
                        'id' => $prefix . 'day_3_date',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Times','ibe'),
                        'tooltip' => __('The times this show day spans','ibe'),
                        'id' => $prefix . 'day_3_times',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Location Name','ibe'),
                        'tooltip' => __('The name of the location for this show day','ibe'),
                        'id' => $prefix . 'day_3_location',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Address Line 1','ibe'),
                        'tooltip' => __('The address line 1 this show day','ibe'),
                        'id' => $prefix . 'day_3_address1',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Address Line 2','ibe'),
                        'tooltip' => __('The address line 2 this show day','ibe'),
                        'id' => $prefix . 'day_3_address2',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Address Line 3','ibe'),
                        'tooltip' => __('The address line 3 this show day','ibe'),
                        'id' => $prefix . 'day_3_address3',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Map','ibe'),
                        'tooltip' => __('The google maps link to this location','ibe'),
                        'id' => $prefix . 'day_3_map',
                        'type' => 'url',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Audience','ibe'),
                        'tooltip' => __('Who this day is targeting','ibe'),
                        'id' => $prefix . 'day_3_audience',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Description','ibe'),
                        'tooltip' => __('This is the description of each day that will be displayed for each day before clicking the learn more button','ibe'),
                        'id' => $prefix . 'day_3_description',
                        'type' => 'wysiwyg',
                        'options' => array(
                            'media_buttons'      => false,
                            'textarea_rows' 	 => 4
                        ),
                    ),
                    array(
                        'name' => __('Header Image','ibe'),
                        'id' => $prefix . 'day_3_image',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                    ),
                    array(
                        'name' => __('Ticket Button Text','ibe'),
                        'id' => $prefix . 'day_3_tickets_text',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Ticket Button URL','ibe'),
                        'id' => $prefix . 'day_3_tickets_url',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Schedule Button Text','ibe'),
                        'id' => $prefix . 'day_3_schedule_text',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Schedule Button URL','ibe'),
                        'id' => $prefix . 'day_3_schedule_url',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Details Enabled','ibe'),
                        'tooltip' => __('If this box is unchecked, the learn more button for this day will not be displayed.','ibe'),
                        'id' => $prefix . 'day_3_enabled',
                        'type' => 'checkbox',
                    ),
                    // Modular Content Blocks
                    array(
                        'type' => 'heading',
                        'name' => esc_html__( 'Day Details', 'ibe' ),
                    ),
                    array(
                        'id'     => $prefix . 'day_3_blocks',
                        'title'  => __( 'Blocks', 'ibe' ),
                        'type'   => 'group',
                        'clone'  => true,
                        'sort_clone' => true,
                        'fields' => array(
                            array(
                                'id'	=> $prefix . 'day_3_section_type',
                                'name'	=> 'Type',
                                'tooltip' => __('What type of section is this?','vx'),
                                'type'	=> 'select',
                                'placeholder'	=> 'Please select a type',
                                'options' => array(
                                    'section_type_paragraph'    => 'Paragraph',
                                    'section_type_ticket'       => 'Ticket',
                                    'section_type_button'       => 'Button',
                                    'section_type_callout'      => 'Callout',
                                    'section_type_divider'      => 'Divider'
                                )
                            ),
                            array(
                                'id' => $prefix . 'day_3_section_heading',
                                'name' => __('Heading','ibe'),
                                'tooltip' => __('A Heading for this block.','ibe'),
                                'type' => 'text',
                                'visible' => array( $prefix . 'day_3_section_type', 'in', array('section_type_paragraph','section_type_list','section_type_ticket','section_type_callout','section_type_button'))
                            ),
                            array(
                                'id' => $prefix . 'day_3_section_ticketprice',
                                'name' => __('Pricing','ibe'),
                                'tooltip' => __('Pricing information for this ticket','ibe'),
                                'type' => 'text',
                                'visible' => array( $prefix . 'day_3_section_type', 'in', array('section_type_ticket'))

                            ),
                            array(
                                'id' => $prefix . 'day_3_section_copy',
                                'name' => __('Copy','ibe'),
                                'tooltip' => __('The copy for this block.','ibe'),
                                'type' => 'wysiwyg',
                                'options' => array(
                                    'media_buttons'      => false,
                                    'textarea_rows' 	 => 4
                                ),
                                'visible' => array( $prefix . 'day_3_section_type', 'in', array('section_type_paragraph','section_type_list','section_type_ticket','section_type_callout'))
                            ),
                            //Button
                            array(
                                'id' => $prefix . 'day_3_section_url',
                                'name' => __('URL','ibe'),
                                'tooltip' => __('The URL for this button to link to.','ibe'),
                                'type' => 'url',
                                'size'  => '80',
                                'visible' => array( $prefix . 'day_3_section_type', 'in', array('section_type_button'))
                            ),
                        ),
                    ),
                ),
            ),
        ),
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Exhibit Template
    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Exhibit - Header
    $meta_boxes[] = array(
        'title'  => __( 'Header', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array(
                'templates/exhibit.php',
            ),
        ),
        'fields' => array(
            array(
                'name' => __('Headline','ibe'),
                'tooltip' => __('The headline for this page','ibe'),
                'id' => $prefix . 'exhibit_headline',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'id'          => $prefix . 'exhibit_background',
                'name'        => __( 'Background Image', 'ibe' ),
                'tooltip' => __('Image to be used as the background of the header section. Minimum dimensions: 1400 x 566 px.','ibe'),
                'type' => 'image_advanced',
                'max_file_uploads' => 1,
                'max_status' => false,
            ),
            array(
                'name' => __('Tagline','ibe'),
                'tooltip' => __('The headline for this page','ibe'),
                'id' => $prefix . 'exhibit_tagline',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'name' => __('Button Text','ibe'),
                'tooltip' => __('The text that will appear in the button below the header','ibe'),
                'id' => $prefix . 'exhibit_button_text',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'name' => __('Button URL','ibe'),
                'tooltip' => __('The url for the button below the header','ibe'),
                'id' => $prefix . 'exhibit_button_url',
                'type' => 'text',
                'size'  => '60',
            ),

        ),
    );
    // Exhibit - Shows
    $meta_boxes[] = array(
        'title'  => __( 'Shows', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array( 'templates/exhibit.php' ),
        ),
        'fields' => array(
            array(
                'id'     => $prefix . 'showlist_selection',
                'title'  => __( 'Shows', 'ibe' ),
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => __('Show','ibe'),
                        'tooltip' => __('Select the show to display.','ibe'),
                        'id' => $prefix . 'show_item',
                        'type' => 'post',
                        'post_type' => array('show'),
                        'field_type'  => 'select',
                    ),
                ),
            ),
        ),
    );
    // Exhibit - Categories
    $meta_boxes[] = array(
        'title'  => __( 'Exhibit Categories', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array( 'templates/exhibit.php' ),
        ),
        'fields' => array(
            array(
                'id'     => $prefix . 'exhibit_categories',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => __('Title','ibe'),
                        'tooltip' => __('The title of this day','ibe'),
                        'id' => $prefix . 'exhibit_category_title',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Description','ibe'),
                        'tooltip' => __('This is the description of each day that will be displayed for each day before clicking the learn more button','ibe'),
                        'id' => $prefix . 'exhibit_category_description',
                        'type' => 'wysiwyg',
                        'options' => array(
                            'media_buttons'      => false,
                            'textarea_rows' 	 => 4
                        ),
                    ),
                    array(
                        'name' => __('Background','ibe'),
                        'tooltip' => __('The background for this tile.','ibe'),
                        'id' => $prefix . 'exhibit_category_background',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                    ),
                ),
            ),
        ),
    );


        // Exhibit - Testimonials
        $meta_boxes[] = array(
            'title'  => __( 'Testimonials', 'ibe' ),
            'post_types' => array('page'),
            'include' => array(
                'template' => array( 'templates/exhibit.php' ),
            ),
            'fields' => array(
                array(
                    'name' => __('Section Title','ibe'),
                    'tooltip' => __('The title for this testimonial section','ibe'),
                    'id' => $prefix . 'testimonials_title',
                    'type' => 'text',
                    'size'  => '60',
                ),
                array(
                    'id'	=> $prefix . 'testimonials_layout',
                    'name'	=> 'Layout Columns',
                    'tooltip' => __('How many columns should the testimonials layout have at desktop view?','ibe'),
                    'type'	=> 'select',
                    'options' => array(
                        'two-col'      => '2',
                        'three-col'    => '3'
                    )
                ),
                array(
                    'id' => $prefix . 'default_divider',
                    'type' => 'divider',
                ),
                array(
                    'id'     => $prefix . 'testimonials',
                    'type'   => 'group',
                    'clone'  => true,
                    'sort_clone' => true,
                    'fields' => array(
                        array(
                            'id'          => $prefix . 'testimonial_youtube_oembed',
                            'name'        => __( 'Youtube URL', 'ibe' ),
                            'tooltip' => __('Input the share code for videos from Youtube. Example: https://youtu.be/SLR8e1i_3eQ','ibe'),
                            'type' => 'oembed',
                        ),
                        array(
                            'name' => __('Tagline','ibe'),
                            'tooltip' => __('The tagline for this file','ibe'),
                            'id' => $prefix . 'testimonial_tagline',
                            'type' => 'text',
                            'size'  => '60',
                        ),
                        array(
                            'name' => __('Description','ibe'),
                            'tooltip' => __('This is the description of each day that will be displayed below the tagline for this file','ibe'),
                            'id' => $prefix . 'testimonial_description',
                            'type' => 'textarea',
                        ),

                    ),
                ),
            ),
        );

    // Exhibit - Media
    $meta_boxes[] = array(
        'title'  => __( 'Media', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array( 'templates/exhibit.php' ),
        ),
        'fields' => array(
            array(
                'name' => __('Section Title','ibe'),
                'tooltip' => __('The title for this media section','ibe'),
                'id' => $prefix . 'media_assets_title',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
               'id'	=> $prefix . 'media_layout',
               'name'	=> 'Layout Columns',
               'tooltip' => __('How many columns should the media section layout have on a desktop view?','ibe'),
               'type'	=> 'select',
               'options' => array(
                   'two-col'      => '2',
                   'three-col'    => '3'
               )
           ),
            array(
                'id' => $prefix . 'default_divider',
                'type' => 'divider',
            ),
            array(
                'id'     => $prefix . 'media_assets',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => __('Media File','ibe'),
                        'tooltip' => __('Select the deck that this should link to','ibe'),
                        'id' => $prefix . 'media_asset_link_deck',
                        'type' => 'post',
                        'post_type' => array('deck'),
                        'field_type'  => 'select',
                    ),
                    array(
                        'name' => __('Asset Title','ibe'),
                        'tooltip' => __('The title for this file','ibe'),
                        'id' => $prefix . 'media_asset_title',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Tagline','ibe'),
                        'tooltip' => __('The tagline for this file','ibe'),
                        'id' => $prefix . 'media_asset_tagline',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Description','ibe'),
                        'tooltip' => __('This is the description of each day that will be displayed below the tagline for this file','ibe'),
                        'id' => $prefix . 'media_asset_description',
                        'type' => 'textarea',
                    ),
                    array(
                        'name' => __('Background','ibe'),
                        'tooltip' => __('The background for this tile.','ibe'),
                        'id' => $prefix . 'media_asset_background',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                    ),
                ),
            ),
        ),
    );

    // Section - Gallery Upper
    $meta_boxes[] = array(
        'title'  => __( 'Gallery - Upper', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array( 'templates/about.php' ),
        ),
        'fields' => array(
            array(
                'id'     => $prefix . 'gallery_top',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => __('Background','ibe'),
                        'tooltip' => __('Select an image to add to the gallery.','ibe'),
                        'id' => $prefix . 'gallery_image',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                    ),
                ),
            ),
        ),
    );

    // Section - Gallery Lower
    $meta_boxes[] = array(
        'title'  => __( 'Gallery - Lower', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array( 'templates/about.php' ),
        ),
        'fields' => array(
            array(
                'id'     => $prefix . 'gallery_bottom',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => __('Background','ibe'),
                        'tooltip' => __('Select an image to add to the gallery.','ibe'),
                        'id' => $prefix . 'gallery_image',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                    ),
                ),
            ),
        ),
    );

    // Section - Story
    $meta_boxes[] = array(
        'title'  => __( 'Story', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array( 'templates/about.php' ),
        ),
        'fields' => array(
            array(
                'name' => __('Section Title','ibe'),
                'tooltip' => __('The title for this section','ibe'),
                'id' => $prefix . 'story_title',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'id' => $prefix . 'default_divider',
                'type' => 'divider',
            ),
            array(
                'id'     => $prefix . 'story_sections',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => __('Title','ibe'),
                        'tooltip' => __('The title for this file','ibe'),
                        'id' => $prefix . 'section_title',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Description','ibe'),
                        'tooltip' => __('This is the description of each day that will be displayed below the tagline for this file','ibe'),
                        'id' => $prefix . 'section_description',
                        'type' => 'textarea',
                    ),
                ),
            ),
        ),
    );

    // Section - Sponsor Days
    $meta_boxes[] = array(
        'title'  => __( 'Sponsor Days', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array( 'templates/sponsors.php' ),
        ),
        'fields' => array(
            array(
                'name' => __('Section Headline','ibe'),
                'tooltip' => __('The title for this section','ibe'),
                'id' => $prefix . 'sponsor_title',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'id' => $prefix . 'default_divider',
                'type' => 'divider',
            ),
            array(
                'id'     => $prefix . 'sponsor_sections',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => __('Day Title','ibe'),
                        'tooltip' => __('The title for this file','ibe'),
                        'id' => $prefix . 'section_title',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Who Attends','ibe'),
                        'id' => $prefix . 'section_attends',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Who Exhibits','ibe'),
                        'id' => $prefix . 'section_exhibits',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Video Link','ibe'),
                        'id' => $prefix . 'section_video_link',
                        'type' => 'oembed',
                    ),
                    array(
                        'name' => __('Featured Image','ibe'),
                        'tooltip' => __('Select an image for this section.','ibe'),
                        'id' => $prefix . 'section_image',
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                    ),
                ),
            ),
        ),
    );
/*
    // Section - Sponsor Form
    $meta_boxes[] = array(
        'title'  => __( 'Sponsor Form', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array( 'templates/sponsors.php' ),
        ),
        'fields' => array(
            array(
                'name' => __('Form Headline','ibe'),
                'tooltip' => __('The title for this section','ibe'),
                'id' => $prefix . 'sponsor_form_title',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'name' => __('Form Text','ibe'),
                'tooltip' => __('The text for this section','ibe'),
                'id' => $prefix . 'sponsor_form_paragraph',
                'type' => 'text',
                'size'  => '60',
            ),
        ),
    );
*/
    //  Section - Youtube Video + Info (for mobile)
    $meta_boxes[] = array(
    	'title'  => __( 'Video Section', 'ibe' ),
        'post_types' => array('page'),
    	'include' => array(
    		'template' => array(
            //    'templates/exhibit.php',
            ),
    	),
    	'fields' => array(
            array(
                'id'          => $prefix . 'page_youtube_oembed',
                'name'        => __( 'Youtube Shortcode', 'ibe' ),
                'tooltip' => __('Input the share code for videos from Youtube. Example: https://youtu.be/SLR8e1i_3eQ','ibe'),
                'type' => 'oembed'
            ),
            array(
                'id'	=> $prefix . 'page_youtube_autoplay',
                'name'	=> 'Autoplay',
                'tooltip' => __('Should the video play as soon as the page is loaded, or require the user to click play?.','ibe'),
                'type'	=> 'select',
                'options' => array(
                    'off'      => 'Off',
                    'on' 	 => 'On'
                )
            ),
            array(
                'id'          => $prefix . 'page_youtube_link',
                'name'        => __( 'Video Link', 'ibe' ),
                'tooltip' => __('Link assiciated with the Youtube Video (Optional).','ibe'),
                'type' => 'text',
                'size'  => '60',
                'placeholder' => __('http://','ibe'),
                'visible' => array( $prefix . 'page_youtube_autoplay', 'in', array('on'))
            ),
            array(
                'id'          => $prefix . 'page_youtube_title',
                'name'        => __( 'Title', 'ibe' ),
                'tooltip' => __('The title of this video.','ibe'),
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'id'          => $prefix . 'page_youtube_tagline',
                'name'        => __( 'Tagline', 'ibe' ),
                'tooltip' => __('The tagline of this video.','ibe'),
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'id'          => $prefix . 'page_youtube_description',
                'name'        => __( 'Description', 'ibe' ),
                'tooltip' => __('The description of this video.','ibe'),
                'type' => 'textarea',
                'size'  => '60',
            ),
    	),
    );

    // Section - Footer Videos
    $meta_boxes[] = array(
        'title'  => __( 'Videos', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
    		'template' => array(
                'templates/about.php',
                'templates/exhibit.php',
                //'templates/attend.php',
            ),
    	),
        'fields' => array(
            array(
                'id'     => $prefix . 'videos',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'id'          => $prefix . 'video_youtube_oembed',
                        'name'        => __( 'Youtube URL', 'ibe' ),
                        'tooltip' => __('Input the share code for videos from Youtube. Example: https://youtu.be/SLR8e1i_3eQ','ibe'),
                        'type' => 'oembed',
                    ),
                    array(
                        'name' => __('Tagline','ibe'),
                        'tooltip' => __('The tagline for this video','ibe'),
                        'id' => $prefix . 'video_tagline',
                        'type' => 'text',
                        'size'  => '60',
                    ),
                    array(
                        'name' => __('Description','ibe'),
                        'tooltip' => __('This is the description that will be displayed below the tagline','ibe'),
                        'id' => $prefix . 'video_description',
                        'type' => 'textarea',
                    ),
                ),
            ),
        ),
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // General Modules
    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Header - Title
    $meta_boxes[] = array(
        'title'  => __( 'Header', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array(
                'templates/about.php',
            ),
        ),
        'fields' => array(
            array(
                'name' => __('Headline','ibe'),
                'tooltip' => __('The headline for this page','ibe'),
                'id' => $prefix . 'header_headline',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'id'          => $prefix . 'header_background',
                'name'        => __( 'Background Image', 'ibe' ),
                'tooltip' => __('Image to be used as the background of the header section. Minimum dimensions: 1400 x 566 px.','ibe'),
                'type' => 'image_advanced',
                'max_file_uploads' => 1,
                'max_status' => false,
            ),
        ),
    );

    // Header - Large Background
    $meta_boxes[] = array(
        'title'  => __( 'Header', 'ibe' ),
        'post_types' => array('page'),
        'include' => array(
            'template' => array(
                'templates/sponsors.php',
            ),
        ),
        'fields' => array(
            array(
                'name' => __('Headline','ibe'),
                'tooltip' => __('The headline for this page','ibe'),
                'id' => $prefix . 'header_headline',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'name' => __('Paragraph','ibe'),
                'tooltip' => __('The intro paragraph for this page','ibe'),
                'id' => $prefix . 'header_paragraph',
                'type' => 'textarea',
                'size'  => '60',
            ),
            array(
                'id'          => $prefix . 'header_background',
                'name'        => __( 'Background Image', 'ibe' ),
                'tooltip' => __('Image to be used as the background of the header section. Minimum dimensions: 1400 x 566 px.','ibe'),
                'type' => 'image_advanced',
                'max_file_uploads' => 1,
                'max_status' => false,
            ),
        ),
    );


    // Section - Info
    $meta_boxes[] = array(
    	'title'  => __( 'Information Section', 'ibe' ),
        'post_types' => array('page'),
    	'include' => array(
    		'template' => array(
                'templates/about.php',
            ),
    	),
    	'fields' => array(
            array(
                'name' => __('Headline','ibe'),
                'tooltip' => __('The headline for this page','ibe'),
                'id' => $prefix . 'page_headline',
                'type' => 'text',
                'size'  => '60',
            ),
            array(
                'name' => __('Main Paragraph','ibe'),
                'tooltip' => __('The informational paragraph for this page','ibe'),
                'id' => $prefix . 'page_paragraph',
                'type' => 'wysiwyg',
                'size'  => '60',
            ),
            array(
                'name' => __('Mission Statement','ibe'),
                'tooltip' => __('The mission statement that is shown under the main paragraph. This will not be displayed on mobile','ibe'),
                'id' => $prefix . 'page_mission',
                'type' => 'textarea',
                'size'  => '60',
            ),
    	),
    );

    //  Section - Youtube Video
    $meta_boxes[] = array(
    	'title'  => __( 'Video Section', 'ibe' ),
        'post_types' => array('page'),
    	'include' => array(
    		'template' => array(
                //'templates/attend.php',
            ),
    	),
    	'fields' => array(
            array(
                'id'          => $prefix . 'page_youtube_oembed',
                'name'        => __( 'Youtube Shortcode', 'ibe' ),
                'tooltip' => __('Input the share code for videos from Youtube. Example: https://youtu.be/SLR8e1i_3eQ','ibe'),
                'type' => 'oembed'
            ),
            array(
                'id'	=> $prefix . 'page_youtube_autoplay',
                'name'	=> 'Autoplay',
                'tooltip' => __('Should the video play as soon as the page is loaded, or require the user to click play?.','ibe'),
                'type'	=> 'select',
                'options' => array(
                    'off'      => 'Off',
                    'on' 	 => 'On'
                )
            ),
            array(
                'id'          => $prefix . 'page_youtube_link',
                'name'        => __( 'Video Link', 'ibe' ),
                'tooltip' => __('Link assiciated with the Youtube Video (Optional).','ibe'),
                'type' => 'text',
                'size'  => '60',
                'placeholder' => __('http://','ibe'),
                'visible' => array( $prefix . 'page_youtube_autoplay', 'in', array('on'))
            ),
    	),
    );

    // Section - Carousel
    $meta_boxes[] = array(
        'title'  => __( 'Carousel', 'ibe' ),
        'post_types' => array('page'),
    	'include' => array(
    		'template' => array(
                'templates/attend.php',
                'templates/exhibit.php',
                'templates/sponsors.php',
            ),
    	),
        'fields' => array(
            array(
                'id'	=> $prefix . 'page_transition_type',
                'name'	=> 'Transition Type',
                'tooltip' => __('How should the slides transition?','ibe'),
                'type'	=> 'select',
                'options' => array(
                    'slide'      => 'Slide',
                    'fade' 	  => 'Fade',
                ),
            ),
            array(
				'type' => 'heading',
				'name' => esc_html__( 'Slides', 'ibe' ),
			),
            array(
                'id'     => $prefix . 'page_carousel_images',
                'title'  => __( 'Carousel Image', 'ibe' ),
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'id'	=> $prefix . 'page_slide_type',
                        'name'	=> 'Slide Type',
                        'tooltip' => __('Select what kind of slide this will be.','ibe'),
                        'type'	=> 'select',
                        'options' => array(
                            'images'      => 'Image',
                            'wysiwyg' 	  => 'Image + WYSIWYG',
                            'quote' 	  => 'Quote'
                        ),
                    ),
                    array(
                        'id'          => $prefix . 'page_carousel_img_desktop',
                        'name'        => __( 'Background Image (Desktop)', 'ibe' ),
                        'tooltip' => __('Image to be used as the background of the slide on desktop computers. Minimum dimensions: 1400 x 566 px.','ibe'),
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                        'visible' => array( $prefix . 'page_slide_type', 'in', array('images', 'wysiwyg', 'quote'))
                    ),
                    array(
                        'id'          => $prefix . 'page_carousel_img_mobile',
                        'name'        => __( 'Background Image (Mobile)', 'ibe' ),
                        'tooltip' => __('Image to be used as the background of the slide on mobile devices. Minimum dimensions: 800 x 1200 px.','ibe'),
                        'type' => 'image_advanced',
                        'max_file_uploads' => 1,
                        'max_status' => false,
                        'visible' => array( $prefix . 'page_slide_type', 'in', array('images', 'wysiwyg', 'quote'))
                    ),
                    array(
                        'id'	=> $prefix . 'page_carousel_bgpos',
                        'name'	=> 'Image Alignment',
                        'tooltip' => __('Select how the image should be aligned. This is to better accommodate images with text, that may otherwise have the text cut off. If no option is selected it will be centered. ','ibe'),
                        'type'	=> 'select',
                        'placeholder' => __('Please select an option','ibe'),
                        'options' => array(
                            'center-center'     => 'Center - Center',
                            'top-center'        => 'Top - Center',
                            'bottom-center' 	=> 'Bottom - Center',
                            'center-left'       => 'Center - Left',
                            'top-left'          => 'Top - Left',
                            'bottom-left' 	    => 'Bottom - Left',
                            'center-right'      => 'Center - Right',
                            'top-right'         => 'Top - Right',
                            'bottom-right' 	    => 'Bottom - Right',
                        ),
                        'visible' => array( $prefix . 'page_slide_type', 'in', array('images', 'wysiwyg', 'quote'))
                    ),
                    array(
                        'name' => __('Text/HTML Content','ibe'),
                        'tooltip' => __('The summary text for this custom featured item.','ibe'),
                        'id' => $prefix . 'page_carousel_content',
                        'type' => 'wysiwyg',
                        'options' => array(
                            'media_buttons'      => false,
                            'textarea_rows' 	    => 6
                        ),
                        'visible' => array( $prefix . 'page_slide_type', 'in', array('wysiwyg'))
                    ),
                    array(
                        'name' => __('Quote','ibe'),
                        'tooltip' => __('The text for the quote to be featured on this slide.','ibe'),
                        'id' => $prefix . 'page_carousel_quote_text',
                        'type' => 'textarea',
                        'size'  => '60',
                        'visible' => array( $prefix . 'page_slide_type', 'in', array('quote'))
                    ),
                    array(
                        'id'	=> $prefix . 'page_carousel_quote_attribution',
                        'name'	=> 'Attribution',
                        'tooltip' => __('Should this quote feature a tag beneath it to indicate the source?.','ibe'),
                        'type'	=> 'select',
                        'placeholder' => __('Please select an option','ibe'),
                        'options' => array(
                            'no'      => 'No',
                            'yes'      => 'Yes',
                        ),
                        'visible' => array( $prefix . 'page_slide_type', 'in', array('quote'))
                    ),
                    array(
                        'name' => __('Source','ibe'),
                        'tooltip' => __('The name of the source of this quote.','ibe'),
                        'id' => $prefix . 'page_carousel_quote_source',
                        'type' => 'text',
                        'size'  => '60',
                        'visible' => array( $prefix . 'page_carousel_quote_attribution', 'in', array('yes'))
                    ),
                    array(
                        'id'	=> $prefix . 'page_quote_textcolor',
                        'name'	=> 'Text Color',
                        'tooltip' => __('Select the text color for the quote. Select a color from the list that will provide suficient contrast with the background image (if any)','ibe'),
                        'type'	=> 'select',
                        'placeholder' => __('Please select an option','ibe'),
                        'options' => array(
                            'color__white'       => 'White',
                            'color__grey' 	     => 'Grey',
                            'color__black'       => 'Black',
                            'color__pink' 	     => 'Pink',
                            'color__purple'  	 => 'Purple',
                            'color__blue'        => 'Blue',
                            'color__lightpink'   => 'Light Pink',
                            'color__lightpurple' => 'Light Purple',
                            'color__lightblue' 	 => 'Light Blue',
                            'color__lightgrey' 	 => 'Light Grey',
                        ),
                        'visible' => array( $prefix . 'page_slide_type', 'in', array('quote'))
                    ),
                    array(
                        'id'	=> $prefix . 'page_quote_alignment',
                        'name'	=> 'Quote Alignment',
                        'tooltip' => __('Select how the quote text should be aligned.','ibe'),
                        'type'	=> 'select',
                        'placeholder' => __('Please select an option','ibe'),
                        'options' => array(
                            'left'     => 'Left',
                            'center'        => 'Center',
                            'right' 	=> 'Right',
                        ),
                        'visible' => array( $prefix . 'page_slide_type', 'in', array('quote'))
                    ),
                    array(
                        'id'	=> $prefix . 'page_source_alignment',
                        'name'	=> 'Source Alignment',
                        'tooltip' => __('Select how the source should be aligned.','ibe'),
                        'type'	=> 'select',
                        'placeholder' => __('Please select an option','ibe'),
                        'options' => array(
                            'left'     => 'Left',
                            'center'        => 'Center',
                            'right' 	=> 'Right',
                        ),
                        'visible' => array( $prefix . 'page_carousel_quote_attribution', 'in', array('yes'))
                    ),
                    array(
                        'id'	=> $prefix . 'page_carousel_slide_link',
                        'name'	=> 'Link Type',
                        'tooltip' => __('Select what kind of page this slide should link to (optional).','ibe'),
                        'type'	=> 'select',
                        'options' => array(
                            'none'      => 'None',
                            'page' 	    => 'Page',
                            'news'      => 'News',
                            'article' 	=> 'Article',
                            'deck' 	    => 'Deck',
                            'url' 	    => 'URL'
                        ),
                    ),
                    array(
                        'name' => __('Page Link','ibe'),
                        'tooltip' => __('Select the page that this image should link to (optional).','ibe'),
                        'id' => $prefix . 'page_carousel_link_page',
                        'type' => 'post',
                        'post_type' => array('page'),
                        'field_type'  => 'select',
                        'hidden' => array( 'page_carousel_slide_link', '!=', 'page' )
                    ),
                    array(
                        'name' => __('News Link','ibe'),
                        'tooltip' => __('Select the news post that this image should link to (optional).','ibe'),
                        'id' => $prefix . 'page_carousel_link_news',
                        'type' => 'post',
                        'post_type' => array('post'),
                        'field_type'  => 'select',
                        'hidden' => array( 'page_carousel_slide_link', '!=', 'news' )
                    ),
                    array(
                        'name' => __('Article Link','ibe'),
                        'tooltip' => __('Select the article that this image should link to (optional).','ibe'),
                        'id' => $prefix . 'page_carousel_link_article',
                        'type' => 'post',
                        'post_type' => array('article'),
                        'field_type'  => 'select',
                        'hidden' => array( 'page_carousel_slide_link', '!=', 'article' )
                    ),
                    array(
                        'name' => __('Deck Link','ibe'),
                        'tooltip' => __('Select the deck that this image should link to (optional).','ibe'),
                        'id' => $prefix . 'page_carousel_link_deck',
                        'type' => 'post',
                        'post_type' => array('deck'),
                        'field_type'  => 'select',
                        'hidden' => array( 'page_carousel_slide_link', '!=', 'deck' )
                    ),
                    array(
                        'name' => __('External Link','ibe'),
                        'tooltip' => __('Input the full link that this image should link to (optional) Ignore this if you have selected an internal link above.','ibe'),
                        'id' => $prefix . 'page_carousel_link_url',
                        'type' => 'text',
                        'size'  => '60',
                        'hidden' => array( 'page_carousel_slide_link', '!=', 'url' )
                    ),
                ),
            ),
        ),
    );


    return $meta_boxes;
}
