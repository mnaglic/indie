<?php
/**
 * iBE_2016 Theme Customizer.
 *
 * @package iBE_2016
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function ibe__customize_register( $wp_customize ) {

	$wp_customize->add_section(
        'company_info',
        array(
            'title'       => 'Company Info',
            'priority'    => 30,
        )
    );
    /* Now you can add your settings ... */
	$wp_customize->add_setting(
        'ibe_company_name',
        array(
            'default'    => '',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        )
    );
    $wp_customize->add_setting(
        'ibe_address_1',
        array(
            'default'    => '123 Example Ave.',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        )
    );
	$wp_customize->add_setting(
        'ibe_address_2',
        array(
            'default'    => 'Unit (optional)',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        )
    );
	$wp_customize->add_setting(
        'ibe_address_3',
        array(
            'default'    => 'Example, EX 11111',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        )
    );
	$wp_customize->add_setting(
        'ibe_telephone',
        array(
            'default'    => '1-222-333-4444',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        )
    );
    /* ... and link controls to these settings. */
	$wp_customize->add_control(
        'ibe_company_name',
        array(
            'label'      => 'Company Name',
            'section'    => 'company_info',
            'settings'   => 'ibe_company_name',
        )
    );
    $wp_customize->add_control(
        'ibe_address_1',
        array(
            'label'      => 'Address 1',
            'section'    => 'company_info',
            'settings'   => 'ibe_address_1',
        )
    );
	$wp_customize->add_control(
        'ibe_address_2',
        array(
            'label'      => 'Address 2',
            'section'    => 'company_info',
            'settings'   => 'ibe_address_2',
        )
    );
	$wp_customize->add_control(
        'ibe_address_3',
        array(
            'label'      => 'City, State, Zip',
            'section'    => 'company_info',
            'settings'   => 'ibe_address_3',
        )
    );
	$wp_customize->add_control(
        'ibe_telephone',
        array(
            'label'      => 'Telephone',
            'section'    => 'company_info',
            'settings'   => 'ibe_telephone',
        )
    );


	$wp_customize->add_section(
        'ibe_social_links',
        array(
            'title'       => 'Social Links',
            'priority'    => 35,
        )
    );
	$wp_customize->add_setting(
        'ibe_social_facebook',
        array(
            'default'    => '',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        )
    );
	$wp_customize->add_control(
        'ibe_social_facebook',
        array(
            'label'      => 'Facebook',
            'section'    => 'ibe_social_links',
            'settings'   => 'ibe_social_facebook',
        )
    );

	$wp_customize->add_setting(
        'ibe_social_instagram',
        array(
            'default'    => '',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        )
    );
	$wp_customize->add_control(
        'ibe_social_instagram',
        array(
            'label'      => 'Instagram',
            'section'    => 'ibe_social_links',
            'settings'   => 'ibe_social_instagram',
        )
    );

	$wp_customize->add_setting(
        'ibe_social_twitter',
        array(
            'default'    => '',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        )
    );
	$wp_customize->add_control(
        'ibe_social_twitter',
        array(
            'label'      => 'Twitter',
            'section'    => 'ibe_social_links',
            'settings'   => 'ibe_social_twitter',
        )
    );

	$wp_customize->add_setting(
        'ibe_social_youtube',
        array(
            'default'    => '',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        )
    );
	$wp_customize->add_control(
        'ibe_social_youtube',
        array(
            'label'      => 'Youtube',
            'section'    => 'ibe_social_links',
            'settings'   => 'ibe_social_youtube',
        )
    );

	$wp_customize->add_setting(
        'ibe_social_pintrest',
        array(
            'default'    => '',
            'type'       => 'option',
            'capability' => 'edit_theme_options',
        )
    );
	$wp_customize->add_control(
        'ibe_social_pintrest',
        array(
            'label'      => 'Pintrest',
            'section'    => 'ibe_social_links',
            'settings'   => 'ibe_social_pintrest',
        )
    );

	$wp_customize->get_setting( 'ibe_address_1' )->transport = 'postMessage';
	$wp_customize->get_setting( 'ibe_address_2' )->transport = 'postMessage';
	$wp_customize->get_setting( 'ibe_address_3' )->transport = 'postMessage';
	$wp_customize->get_setting( 'ibe_telephone' )->transport = 'postMessage';
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';

	$wp_customize->get_setting( 'ibe_social_facebook' )->transport = 'postMessage';
	$wp_customize->get_setting( 'ibe_social_twitter' )->transport = 'postMessage';
	$wp_customize->get_setting( 'ibe_social_twitter' )->transport = 'postMessage';
	$wp_customize->get_setting( 'ibe_social_youtube' )->transport = 'postMessage';

}


add_action( 'customize_register', 'ibe__customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function ibe__customize_preview_js() {
	wp_enqueue_script( 'ibe__customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'ibe__customize_preview_js' );


function ibe_customize_register( $wp_customize ) {
    /* Just use the $wp_customize object and create a section or use a built-in
       section. */


}
add_action( 'customize_register' , 'ibe_customize_register' );
