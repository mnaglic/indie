<?php

show_admin_bar(false);
add_filter( 'auth_cookie_expiration', 'keep_me_logged_in_for_1_year' );

function keep_me_logged_in_for_1_year( $expirein ) {
    return 31556926; // 1 year in seconds
}

if ( is_user_logged_in() ) {
    add_filter('body_class','add_role_to_body');
    add_filter('admin_body_class','add_role_to_body');
}
function add_role_to_body($classes) {
    $current_user = new WP_User(get_current_user_id());
    $user_role = array_shift($current_user->roles);
    if (is_admin()) {
        $classes .= 'role-'. $user_role;
    } else {
        $classes[] = 'role-'. $user_role;
    }
    return $classes;
}


add_filter( 'logout_url', 'custom_logout_url' );
function custom_logout_url( $default )
{
    // set your URL here
    return is_admin() ? home_url() : $default;
}

function my_login_logo() {
//wp_enqueue_style( 'ibe-style', get_template_directory_uri() . '/css/style.css', array(), true );
    ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/menu-logo.png);
		    width: 100%;
		    background-size: contain;
        }
        .login .message {
            border-left: 4px solid #e2c0df !important;
        }
        .wp-core-ui .button-primary {
            background: #e2c0df !important;
            border-color: inherit !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            color: #fff !important;
            text-decoration: none !important;
            text-shadow: none !important;
            width: 100% !important;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Your Site Name and Info';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

function load_custom_wp_admin_style() {
    wp_enqueue_script( 'my_custom_script', get_template_directory_uri() . '/js/admin.js' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );



/**
 * Change the greeting
 *
 */
add_filter('gettext', 'change_howdy', 10, 3);

function change_howdy($translated, $text, $domain) {

    if (!is_admin() || 'default' != $domain)
        return $translated;

    if (false !== strpos($translated, 'Howdy'))
        return str_replace('Howdy', 'Welcome', $translated);

    return $translated;
}


/**
 * Change footer text
 *
 */
function change_footer_admin () {

    echo 'Custom theme developed for Indie Beauty Expo by <a href="http://www.lukeragno.com.com">Luke Ragno Design &amp; Art Direction</a>.';

}

add_filter('admin_footer_text', 'change_footer_admin');



/**
 * Add theme info box into WordPress Dashboard
 *
 */
 /*
add_action('wp_dashboard_setup', 'ibe_add_dashboard_widgets' );
function ibe_add_dashboard_widgets() {

  wp_add_dashboard_widget('wp_dashboard_widget', 'Theme Instructions', 'ibe_theme_info');

}
*/
/*
function ibe_theme_info() {

  echo "
  <ul>
    <li><strong>Instructions available here:</strong> <a href='http://indiebeautyexpo.com/admin-help/'>Admin Guide (PDF)</a></li>
  </ul>";

}

*/






/**
 * Hide editor on specific pages.
 *
 */
 add_action( 'admin_head', 'hide_editor' );

 function hide_editor() {
     global $pagenow;
     if( !( 'post.php' == $pagenow ) ) return;

     global $post;
     // Get the Post ID.
     $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
     if( !isset( $post_id ) ) return;

  // Hide the editor on the page titled 'Homepage'
  //$homepgname = get_the_title($post_id);
  //if($homepgname == 'Homepage'){
  //    remove_post_type_support('page', 'editor');
  //}

  // Hide the editor on a page with a specific post type
  //$post_type = get_post_type( $post_id );
  //if($post_type == 'contact.php'){ // the filename of the page templat
  //    remove_post_type_support($post_type, 'editor');
  //}

    $template_file = substr( get_page_template(), strrpos( get_page_template(), '/' ) + 1 );
    if(
        $template_file == 'home.php'    ||
        $template_file == 'contact.php' ||
        $template_file == 'news.php'    ||
        $template_file == 'about.php'   ||
        $template_file == 'attend.php'  ||
        $template_file == 'exhibit.php' ||
        $template_file == 'apply.php'

    ){
        remove_post_type_support('page', 'editor');
    }
}

?>
